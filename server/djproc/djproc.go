// Background processing daemon for djrandom.

package main

import (
	"flag"
	"log"

	"git.autistici.org/ale/djrandom/server/actions"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/services/index"
	"git.autistici.org/ale/djrandom/services/storage"
	"git.autistici.org/ale/djrandom/services/tasks"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/daemon"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
	addr       = flag.String("addr", ":3006", "TCP address to listen on")
	nWorkers   = flag.Int("nworkers", 3, "Number of workers")
)

func main() {
	flag.Parse()
	config.MustParse(*configFile)

	daemon.Setup()

	// Create client stubs for all our services.
	db := db_client.NewDatabaseImplFromConfig()
	storageClient := storage.NewDistributedStorageClientFromConfig()
	indexClient := index.NewIndexClientFromConfig()
	taskClient := tasks.NewTaskClientFromConfig(db)

	// Start the state machine.
	sm := actions.NewSongStateMachine(db, storageClient, indexClient, taskClient)
	go sm.Run(*nWorkers)

	// Start an HTTP server (just for the debug pages, really).
	log.Printf("Starting processing server on %s", *addr)
	err := util.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
