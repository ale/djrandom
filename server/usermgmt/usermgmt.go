package main

import (
	"flag"
	"fmt"
	"log"
	"strconv"
	"time"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/util/config"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
)

func createNewUser(db services.Database, username string, pw string) {
	user := &api.User{
		Email:       username,
		CreatedAt:   time.Now().Unix(),
		InvitedBy:   "admin",
		InvitesLeft: 3,
		State:       "active",
	}
	user.SetPassword(pw)
	err := db.PutUser(nil, user)
	if err != nil {
		log.Fatalf("Error creating user: %s", err)
	}
}

func setPassword(db services.Database, username string, pw string) {
	s, err := db.NewSession()
	if err != nil {
		log.Fatalf("Error connecting to db: %s", err)
	}
	defer s.Close()

	user, ok := db.GetUser(s, username)
	if !ok {
		log.Fatalf("User does not exist")
	}
	user.SetPassword(pw)
	err = db.PutUser(s, user)
	if err != nil {
		log.Fatalf("Error updating user: %s", err)
	}
}

func setInvites(db services.Database, username string, numInvites int) {
	s, err := db.NewSession()
	if err != nil {
		log.Fatalf("Error connecting to db: %s", err)
	}
	defer s.Close()

	user, ok := db.GetUser(s, username)
	if !ok {
		log.Fatalf("User does not exist")
	}
	user.InvitesLeft = numInvites
	err = db.PutUser(s, user)
	if err != nil {
		log.Fatalf("Error updating user: %s", err)
	}
}

func createNewAuthKey(db services.Database, username string) {
	s, err := db.NewSession()
	if err != nil {
		log.Fatalf("Error connecting to db: %s", err)
	}
	defer s.Close()

	user, ok := db.GetUser(s, username)
	if !ok {
		log.Fatalf("User does not exist")
	}
	authKey := user.NewAuthKey()
	err = db.PutAuthKey(s, authKey)
	if err != nil {
		log.Fatalf("Error saving auth key: %s", err)
	}
	fmt.Printf("auth_key=%s\nauth_secret=%s\n", authKey.KeyId, authKey.Secret)
}

func main() {
	flag.Parse()
	config.Parse(*configFile)

	db := db_client.NewDatabaseImplFromConfig()

	cmd := flag.Arg(0)
	switch cmd {
	case "add-user":
		createNewUser(db, flag.Arg(1), flag.Arg(2))
	case "gen-auth-key":
		createNewAuthKey(db, flag.Arg(1))
	case "set-password":
		setPassword(db, flag.Arg(1), flag.Arg(2))
	case "set-invites":
		n, err := strconv.Atoi(flag.Arg(2))
		if err != nil {
			log.Fatalf("Could not parse number: %v", err)
		}
		setInvites(db, flag.Arg(1), n)
	default:
		log.Fatalf("Unknown command '%s'", cmd)
	}
}
