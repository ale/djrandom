// Computes acoustic correlations between songs passed on the standard
// input, comparing each one of them with every other song in the
// acoustic features archive.
//
// This is a huge memory hog since it needs to load all acoustic data
// in memory at once.
//
package main

import (
	"bufio"
	"flag"
	"log"
	"os"
	"runtime"
	"sort"
	"strings"
	"sync"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/imms/imms-go"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")

	// Ignore correlations below this threshold.
	CorrelationThreshold = 30

	// Keep only the top N correlation scores.
	TopScores = 20
)

type Correlator struct {
	ac map[api.SongID]imms.Features
	db services.Database
}

func NewCorrelator(db services.Database) *Correlator {
	return &Correlator{
		ac: make(map[api.SongID]imms.Features),
		db: db,
	}
}

func (c *Correlator) LoadAcousticData() error {
	sess, err := c.db.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer sess.Close()

	iter, err := sess.Scan(db_client.AcousticDataBucket, "", "\xff", -1)
	if err != nil {
		return err
	}
	for {
		var data api.AcousticData
		key, err := iter.Next(&data)
		if err != nil {
			break
		}
		id, _ := api.ParseSongID(key)
		c.ac[id] = *imms.NewFeaturesFromData(data.Features)
	}
	log.Printf("loaded acoustic data for %d songs", len(c.ac))
	return nil
}

type correlationScore struct {
	songId api.SongID
	corr   int
}

type correlationSorter struct {
	scores []correlationScore
}

func (cs correlationSorter) Len() int {
	return len(cs.scores)
}

func (cs correlationSorter) Swap(i, j int) {
	cs.scores[i], cs.scores[j] = cs.scores[j], cs.scores[i]
}

func (cs correlationSorter) Less(i, j int) bool {
	return cs.scores[i].corr > cs.scores[j].corr
}

func (c *Correlator) computeCorrelations(sess services.Session, songId api.SongID) {
	ref, ok := c.ac[songId]
	if !ok {
		log.Printf("no acoustic data for %s", songId)
		return
	}

	sm := imms.NewSimilarityModel()
	defer sm.Close()

	scores := make([]correlationScore, 0)
	for id, f := range c.ac {
		corr := (int)(100 * sm.Eval(&ref, &f))
		if corr > CorrelationThreshold {
			scores = append(scores, correlationScore{id, corr})
		}
	}

	sort.Sort(correlationSorter{scores})

	count := 0
	for _, cs := range scores[:TopScores] {
		c.db.PutSongAcousticCorrelation(sess, songId, cs.songId, float32(cs.corr))
		count++
	}
	log.Printf("song %s: %d correlations", songId, count)
}

func (c *Correlator) CompareAll(in chan api.SongID) {
	sess, err := c.db.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer sess.Close()

	for songId := range in {
		c.computeCorrelations(sess, songId)
	}
}

// Read song ids from standard input.
func readSongIdsFromStdin(out chan api.SongID) {
	in := bufio.NewReader(os.Stdin)
	for {
		line, err := in.ReadString('\n')
		if err != nil {
			break
		}

		id, err := api.ParseSongID(strings.TrimSpace(line))
		if err == nil {
			out <- id
		}
	}
	close(out)
}

func main() {
	flag.Parse()
	config.Parse(*configFile)

	db := db_client.NewDatabaseImplFromConfig()

	c := NewCorrelator(db)
	if err := c.LoadAcousticData(); err != nil {
		log.Fatal(err)
	}

	songCh := make(chan api.SongID)
	var wg sync.WaitGroup
	for i := 0; i < runtime.NumCPU(); i++ {
		wg.Add(1)
		go func() {
			c.CompareAll(songCh)
			wg.Done()
		}()
	}

	go readSongIdsFromStdin(songCh)

	wg.Wait()

	log.Printf("done.")
}
