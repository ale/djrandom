// Background processing on songs is structured in terms of a
// workflow, where each song goes through a series of consecutive
// states or 'actions'. These actions are usually triggered upon
// import of a new song.
//
// This has some advantages with respect to running everything
// sequentially in a background goroutine, most importantly that
// the song state machine can be easily restarted, making the
// processing components stateless.
//
package actions

import (
	"errors"
	"io"
	"log"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/api/task_api"
	"git.autistici.org/ale/djrandom/server/actions/plans"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/services/tasks"
)

var (
	songActions = make(map[string]func(*SongActionBase) SongAction)
)

func RegisterSongAction(name string, fn func(*SongActionBase) SongAction) {
	songActions[name] = fn
}

// Internal SongAction / Action glue.
type songActionWrapper struct {
	action SongAction
}

// Handle wraps a SongAction's HandleSong method.
func (a *songActionWrapper) Handle(songId string, l io.Writer) error {
	id, err := api.ParseSongID(songId)
	if err != nil {
		return err
	}

	// Run GetSong() and HandleSong() within the same database session.
	db := a.action.GetDb()
	session, err := db.NewSession()
	if err != nil {
		return err
	}
	defer session.Close()

	song, ok := db.GetSong(session, id)
	if !ok {
		log.Printf("Error: non-existing song %s", songId)
		return errors.New("Song not found")
	}
	return a.action.HandleSong(session, song, l)
}

func NewSongStateMachine(db services.Database, storage services.Storage, idx services.Index, tc services.TaskClient) *tasks.StateMachine {
	base := &SongActionBase{
		Db:      db,
		Storage: storage,
		Index:   idx,
	}

	actionsMap := make(map[string]task_api.Action)
	for name, actionFn := range songActions {
		actionsMap[name] = &songActionWrapper{actionFn(base)}
	}

	return tasks.NewStateMachine(tc, plans.KnownPlans, actionsMap)
}
