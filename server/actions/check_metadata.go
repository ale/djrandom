package actions

import (
	"errors"
	"flag"
	"fmt"
	"io"
	"time"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	fp "git.autistici.org/ale/djrandom/util/fingerprinting"
)

var (
	// API key.
	echonestApiKey = flag.String("echonest_api_key", "", "Echonest API key")

	// Do not retry a check before 6 months.
	maxCheckMetadataFingerprintAge int64 = 15552000
)

// CheckMetadataAction checks whether metadata for a song is sufficient,
// and triggers a check on last.fm if it isn't.
type CheckMetadataAction struct {
	*SongActionBase
}

func (a *CheckMetadataAction) HandleSong(session services.Session, song *api.Song, log io.Writer) error {
	if song.Meta.Title != "" && song.Meta.Artist != "" {
		// Metadata is ok, activate.
		return nil
	}

	// Bad metadata. Let's see if we can do something to improve it.
	exitStatus := errors.New("Metadata check failed")

	// Let's first check if we already have fingerprinted the song recently.
	now := time.Now().Unix()
	fpObj, err := a.Db.GetSongFingerprint(session, song.Id)
	if err != nil || (now-fpObj.Stamp) < maxCheckMetadataFingerprintAge {

		code, version, err := fp.GetSongFingerprint(song, a.Db, a.Storage)
		found := false
		if err != nil {
			fmt.Fprintf(log, "GetSongFingerprint(%s) failed: %s", song.Id, err)
		} else {
			echonest := fp.NewEchonest(*echonestApiKey)
			newMeta, err := echonest.Identify(code, version)
			if err != nil {
				fmt.Fprintf(log, "echonest.Identify(%s) failed: %s", song.Id, err)
			} else if newMeta == nil {
				fmt.Fprintf(log, "echonest.Identify(%s) returned no data", song.Id)
			} else {
				// Wow, we actually found something!
				fmt.Fprintf(log, "CheckMetadataAction(%s): identified as %s / %s\n", song.Id, newMeta.Artist, newMeta.Title)
				song.Meta.Artist = newMeta.Artist
				song.Meta.Title = newMeta.Title
				a.Db.PutSong(session, song)
				found = true
				exitStatus = nil

			}
		}

		// Save the fingerprint record anyway (it's important to
		// record negative results too!)
		newFp := api.Fingerprint{
			Code:    code,
			Version: version,
			Ok:      found,
			Stamp:   now,
		}
		a.Db.PutSongFingerprint(session, song.Id, &newFp)
	}

	return exitStatus
}

func init() {
	RegisterSongAction(
		"check_metadata",
		func(base *SongActionBase) SongAction {
			return &CheckMetadataAction{base}
		})
}
