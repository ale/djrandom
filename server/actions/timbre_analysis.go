package actions

import (
	"errors"
	"fmt"
	"io"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/ffmpeg"
	// "git.autistici.org/ale/imms/imms-go"
)

var (
	immsAnalyzer = config.String("imms_analyzer", "/usr/local/bin/imms-analyzer", "Location of the imms-analyzer binary")
)

// TimbreAnalysisAction computes the timbre fingerprint of a song.
type TimbreAnalysisAction struct {
	*SongActionBase
}

func (a *TimbreAnalysisAction) HandleSong(session services.Session, song *api.Song, log io.Writer) error {
	// First, check to see if the timbre analysis has already been
	// performed.
	if _, err := a.Db.GetSongAcousticData(session, song); err == nil {
		// In this case, no further processing is done.
		return nil
	}

	// Run imms-analyzer to extract the feature vector.
	f, err := analyzeSong(song, a.Storage, log)
	if err != nil {
		return err
	}

	return a.Db.PutSongAcousticData(session, song, &api.AcousticData{f})
}

func init() {
	RegisterSongAction(
		"timbre_analysis",
		func(base *SongActionBase) SongAction {
			return &TimbreAnalysisAction{base}
		})
}

func analyzeSong(song *api.Song, storage services.Storage, log io.Writer) ([]byte, error) {
	audioFileId := song.Files[0].MD5

	input, err := storage.Open(audioFileId)
	if err != nil {
		return nil, err
	}
	defer input.Close()

	stdout, stderr, err := ffmpeg.RunPipe(
		input,
		[]string{"-i", "-", "-f", "u16le", "-ac", "1", "-ar", "22050", "-"},
		[]string{*immsAnalyzer})
	if err != nil {
		fmt.Fprintf(log, "error running imms-analyzer: %s\n\n%s\n", err, stderr)
		return nil, err
	}
	if stdout == "" {
		fmt.Fprintf(log, "empty output from imms-analyzer\n")
		return nil, errors.New("empty output")
	}
	return []byte(stdout), nil
}
