package actions

import (
	"errors"
	"io"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
)

// ActivateSongAction makes a song 'active' in the db.
type ActivateSongAction struct {
	*SongActionBase
}

func (a *ActivateSongAction) HandleSong(session services.Session, song *api.Song, log io.Writer) error {
	// Set song state to active and add the song to the index.
	song.Info.State = "active"
	a.Db.PutSong(session, song)
	a.Index.AddDoc(song.Id, song.ToDoc())
	return nil
}

// DeactivateSongAction makes a song 'inactive' in the db.
type DeactivateSongAction struct {
	*SongActionBase
}

func (a *DeactivateSongAction) HandleSong(session services.Session, song *api.Song, log io.Writer) error {
	// Set state to inactive and remove the song from the index.
	song.Info.State = "inactive"
	a.Db.PutSong(session, song)
	a.Index.DeleteDoc(song.Id)

	// Deactivate returns an error since it's meant to be the
	// last state of a plan, as the result of a previous error.
	return errors.New("Song disabled")
}

func init() {
	RegisterSongAction(
		"activate",
		func(base *SongActionBase) SongAction {
			return &ActivateSongAction{base}
		})
	RegisterSongAction(
		"deactivate",
		func(base *SongActionBase) SongAction {
			return &DeactivateSongAction{base}
		})
}
