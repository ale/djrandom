package plans

import (
	"git.autistici.org/ale/djrandom/api/task_api"
)

// IncomingPlan is executed whenever we receive a new song.
var IncomingPlan = task_api.NewPlan(
	"incoming",
	"timbre_analysis",
	map[string][]string{
		"timbre_analysis": []string{"check_metadata", "check_metadata"},
		"check_metadata":  []string{"activate", "deactivate"},
		"encode_mp3":      []string{"", ""},
		"activate":        []string{"encode_mp3", ""},
		"deactivate":      []string{"", ""},
	})

// ReencodePlan is used to trigger encoding, in case we missed something.
var ReencodePlan = task_api.NewPlan(
	"reencode",
	"encode_mp3",
	map[string][]string{
		"encode_mp3": []string{"", ""},
	})

// ReanalyzePlan is used to extract acoustic data from all songs that
// miss it.
var ReanalyzePlan = task_api.NewPlan(
	"reanalyze",
	"timbre_analysis",
	map[string][]string{
		"timbre_analysis": []string{"", ""},
	})

// KnownPlans keeps a registry of all available plans.
var KnownPlans = map[string]*task_api.Plan{}

func RegisterPlan(plan *task_api.Plan) {
	KnownPlans[plan.Name] = plan
}

func init() {
	RegisterPlan(IncomingPlan)
	RegisterPlan(ReencodePlan)
	RegisterPlan(ReanalyzePlan)
}
