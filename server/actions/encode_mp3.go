package actions

import (
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util/ffmpeg"
	"git.autistici.org/ale/djrandom/util/metadata_extraction"
)

type EncodeMp3Action struct {
	*SongActionBase
}

func init() {
	RegisterSongAction(
		"encode_mp3",
		func(base *SongActionBase) SongAction {
			return &EncodeMp3Action{base}
		})
}

func isHighQuality(af *api.AudioFile) bool {
	return af.MimeType == api.MIMETYPE_FLAC
}

func (a *EncodeMp3Action) HandleSong(session services.Session, song *api.Song, log io.Writer) error {
	// See if we already have an mp3 file, otherwise pick a high
	// quality source to encode.
	var audioFileId string
	for _, af := range song.Files {
		if af.MimeType == api.MIMETYPE_MP3 {
			fmt.Fprintf(log, "an mp3 file is already present\n")
			return nil
		}
		if isHighQuality(af) {
			audioFileId = af.MD5
		}
	}
	if audioFileId == "" {
		fmt.Fprintf(log, "could not find a high-quality source to encode\n")
		return errors.New("No high-quality source")
	}

	// Generate a temporary file to save the encoded mp3 locally.
	tmpf, err := ioutil.TempFile("", "djrandom-mp3-")
	if err != nil {
		return err
	}
	tmpf.Close()
	defer os.Remove(tmpf.Name())

	// Download file and pipe it directly into ffmpeg.
	file, err := a.Storage.Open(audioFileId)
	if err != nil {
		fmt.Fprintf(log, "error downloading file: %s\n", err)
		return err
	}
	defer file.Close()

	// Invoke ffmpeg.
	ffmpegArgs := []string{
		"-i", "-",
		"-acodec", "libmp3lame",
		"-b:a", "320k",
		"-f", "mp3",
		tmpf.Name(),
	}
	fmt.Fprintf(log, "running ffmpeg: %v\n", ffmpegArgs)
	output, err := ffmpeg.Run(file, ffmpegArgs)
	// Add ffmpeg output to the log, it's useful when debugging.
	io.WriteString(log, output)
	if err != nil {
		fmt.Fprintf(log, "ffmpeg failed: %s", err)
		return err
	}

	// Update the db with the new file!
	af, _, err := metadata_extraction.GetMetadata(tmpf.Name(), api.MIMETYPE_MP3)
	if err != nil {
		fmt.Fprintf(log, "error reading meta: %s\n", err)
		return err
	}

	// Add the file to the song, and upload it to remote storage.
	outfile, err := os.Open(tmpf.Name())
	if err != nil {
		fmt.Fprintf(log, "unexpected error re-opening file %s: %s\n", tmpf.Name(), err)
		return err
	}
	defer outfile.Close()

	if err = a.Storage.Write(af.MD5, outfile); err != nil {
		fmt.Fprintf(log, "error copying file to persistent storage: %s\n", err)
		return err
	}

	fmt.Fprintf(log, "encoded new mp3 (%s)\n", af.MD5)
	song.Files = append(song.Files, af)
	a.Db.PutSong(session, song)

	return nil
}
