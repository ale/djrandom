package actions

import (
	"io"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
)

var (
	NoOpAction = []string{}
)

// SongActionBase provides context to the song state machine actions.
type SongActionBase struct {
	Db      services.Database
	Index   services.Index
	Storage services.Storage
}

func (a *SongActionBase) GetDb() services.Database {
	return a.Db
}

// SongAction is the common interface for song actions.
type SongAction interface {
	GetDb() services.Database
	HandleSong(session services.Session, song *api.Song, w io.Writer) error
}
