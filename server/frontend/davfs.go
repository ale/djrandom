// Expose a WebDAV interface to the song database.

package frontend

import (
	"net/http"
	"strings"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/api/fsapi"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util/webdav"
)

// DavFs implements a filesystem in terms of the webdavi.Fs
// interface. It exports a "/artist/album/song_title"
// filesystem hierarchy.
type DavFs struct {
	fs      fsapi.Filesystem
	storage services.Storage
}

func NewDavFs(fs fsapi.Filesystem, storage services.Storage) *DavFs {
	return &DavFs{fs, storage}
}

func (davfs *DavFs) Get(w http.ResponseWriter, r *http.Request) {
	path := strings.Trim(r.URL.Path, "/")
	attr, err := davfs.fs.Stat(path)
	if err != nil {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	// Directly call the storage reverse proxy handler. We need
	// to wipe clean the Authorization header in the original
	// request though, or the storage client authentication won't
	// work.
	r.Header.Del("Authorization")
	downloadProxyHandler(davfs.storage, attr.Audio.MD5, w, r)
}

func (davfs *DavFs) Stat(path string) (webdav.Attrs, error) {
	var wattrs webdav.Attrs

	attr, err := davfs.fs.Stat(path)
	if err != nil {
		return wattrs, err
	}

	// Note: A Stat() request does not return a name as it always
	// refers to the request URL.
	if attr.Audio != nil {
		wattrs.Size = attr.Audio.Size
		wattrs.Mtime = attr.Song.Info.CreatedAt
		wattrs.Ctime = attr.Song.Info.CreatedAt
	} else {
		wattrs.IsDir = true
	}

	return wattrs, nil
}

func (davfs *DavFs) Ls(path string) ([]webdav.Attrs, error) {
	attrs, err := davfs.fs.Ls(path)
	if err != nil {
		return nil, err
	}

	result := make([]webdav.Attrs, len(attrs))
	for i, attr := range attrs {
		var wattrs webdav.Attrs
		wattrs.Name = attr.Name
		wattrs.IsDir = attr.IsDir
		if attr.Audio != nil {
			// Add a suitable extension.
			wattrs.Name = wattrs.Name + api.GetExtensionForMimeType(attr.Audio.MimeType)
			wattrs.Size = attr.Audio.Size
			wattrs.Mtime = attr.Song.Info.CreatedAt
			wattrs.Ctime = attr.Song.Info.CreatedAt
		}
		result[i] = wattrs
	}
	return result, nil
}

