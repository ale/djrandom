// Expose a JSON-based filesystem-like API to the song database.

package frontend

import (
	"log"
	"net/http"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/api/fsapi"
)

type JsonFs struct {
	fs fsapi.Filesystem
}

func NewJsonFs(fs fsapi.Filesystem) *JsonFs {
	return &JsonFs{fs}
}

func fsattrToJson(attr fsapi.Attr) api.JsonFsAttr {
	out := api.JsonFsAttr{
		Name:  attr.Name,
		IsDir: attr.IsDir,
		Audio: attr.Audio,
	}
	if attr.Song != nil {
		out.Meta = &attr.Song.Meta
	}
	return out
}

func fsattrListToJson(attrs []fsapi.Attr) []*api.JsonFsAttr {
	out := make([]*api.JsonFsAttr, len(attrs))
	for i, attr := range attrs {
		jattr := fsattrToJson(attr)
		out[i] = &jattr
	}
	return out
}

func (j *JsonFs) ServeHTTP(w http.ResponseWriter, r *djRequest) {
	op := r.Request.URL.Query().Get("op")
	path := r.Request.URL.Query().Get("path")

	log.Printf("jsonfs: op=%s, path=%s", op, path)

	switch op {
	case "stat":
		result, err := j.fs.Stat(path)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		resp := fsattrToJson(result)
		sendJsonResponse(w, &resp)

	case "ls":
		result, err := j.fs.Ls(path)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		var resp api.JsonFsLsResponse
		resp.Results = fsattrListToJson(result)
		sendJsonResponse(w, &resp)

	default:
		http.Error(w, "Bad op", http.StatusBadRequest)
	}
}
