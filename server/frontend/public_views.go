package frontend

import (
	"net/http"
)

// Index shows the homepage.
func Index(w http.ResponseWriter, r *djRequest) {
	if r.Request.URL.Path != "/" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	r.Ctx.RenderTemplate(w, "index.html", newTemplateContext("index", r))
}

// About shows the about page.
func About(w http.ResponseWriter, r *djRequest) {
	r.Ctx.RenderTemplate(w, "about.html", newTemplateContext("about", r))
}
