package frontend

import (
	"git.autistici.org/ale/djrandom/api"
)

type SearchEngine interface {
	Search(query string) ([]string, error)
	Index(md5 string, meta *api.Meta) error
}
