package frontend

import (
	"log"
	"os"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/server/actions/plans"
	"git.autistici.org/ale/djrandom/util/metadata_extraction"
)

func createSong(path string) (*api.Song, error) {
	// Do not trust the MIME type that came with the HTTP request.
	af, m, err := metadata_extraction.GuessMetadata(path)
	if err != nil {
		return nil, err
	}

	// Assemble the Song object. The new ID will simply be the MD5
	// checksum of the audio data.
	return api.NewSong(af, m), nil
}

// AnalyzeAndStore implements the background processing pipeline for
// incoming files. At this stage, the file still exists on the local
// filesystem.
//
// Once the song is in the database, further processing and analysis
// is delegated to the background processing nodes via the queue
// service (i.e. we initialize the song state machine at the end of
// this function).
func AnalyzeAndStore(ac *AppContext, localPath string) {
	// Whatever happens, we'll need to remove the temporary file
	// when we're done.
	defer os.Remove(localPath)

	// Parse metadata and generate a new Song object.
	song, err := createSong(localPath)
	if err != nil {
		log.Printf("analyzeAndStore(): metadata scan failed: %s", err)
		return
	}

	// Copy the local file to remote storage.
	file, err := os.Open(localPath)
	if err != nil {
		log.Printf("analyzeAndStore(): Unexpected error re-opening file %s: %s", localPath, err)
		return
	}
	defer file.Close()

	if err = ac.Storage.Write(song.Id.String(), file); err != nil {
		log.Printf("analyzeAndStore(%s): Error copying file to persistent storage: %s", song.Id, err)
		return
	}

	// Save the newly created Song object in the database.
	if err = ac.Db.PutSong(nil, song); err != nil {
		log.Printf("analyzeAndStore(%s): Error saving Song object to the db: %s", song.Id, err)
		return
	}

	// Add it to the index.
	if err = ac.Index.AddDoc(song.Id, song.ToDoc()); err != nil {
		log.Printf("analyzeAndStore(%s): Error indexing song: %s", song.Id, err)
		// Don't bail out here, if indexing fails we still have the
		// opportunity to re-add the document later, while running
		// consistency checks.
	}

	// Create a new 'incoming' job for this song.
	if err = ac.TaskClient.CreateJob(plans.IncomingPlan, song.Id.String()); err != nil {
		log.Printf("analyzeAndStore(%s): Error creating incoming job: %s", song.Id, err)
	}

	// Just log something here, we have been successful!
	log.Printf("successfully stored new Song: %s", song.Id)
}
