package albumart

import (
	"bytes"
	"encoding/xml"
	"errors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"os/exec"
	"sort"
)

const (
	lastfmApiEndpoint = "http://ws.audioscrobbler.com/2.0/?"
)

type LastFmImage struct {
	Size string `xml:"size,attr"`
	Url  string `xml:",chardata"`
}

var lastFmSizeIndexMap = map[string]int{
	"medium":     1,
	"large":      2,
	"extralarge": 3,
	"mega":       4,
}

func (i *LastFmImage) GetSizeIndex() int {
	// This will return 0 if the size identifier is not known.
	return lastFmSizeIndexMap[i.Size]
}

type LastFmImages []LastFmImage

func (l LastFmImages) Len() int      { return len(l) }
func (l LastFmImages) Swap(i, j int) { l[i], l[j] = l[j], l[i] }

type SortedLastFmImages struct {
	LastFmImages
}

// Less actually implements a reverse sort.
func (l SortedLastFmImages) Less(i, j int) bool {
	return l.LastFmImages[i].GetSizeIndex() > l.LastFmImages[j].GetSizeIndex()
}

type LastFmMeta struct {
	XMLName xml.Name      `xml:"lfm"`
	Images  []LastFmImage `xml:"album>image"`
}

// Query Last.fm for album art.
func queryLastFm(apiKey, artist, album string) ([]byte, error) {

	// Perform the metadata query, trying to find out the URL for
	// an 'extralarge' or 'mega' version of the album art image.
	v := url.Values{}
	v.Set("method", "album.getInfo")
	v.Set("artist", artist)
	v.Set("album", album)
	v.Set("api_key", apiKey)
	resp, err := http.Get(lastfmApiEndpoint + v.Encode())
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	xmlData, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	// Let's find the URLs inside the returned XML...
	var meta LastFmMeta
	buf := bytes.NewBuffer(xmlData)
	if err = xml.NewDecoder(buf).Decode(&meta); err != nil {
		log.Printf("last.fm response:\n%s", xmlData)
		return nil, err
	}
	if len(meta.Images) == 0 {
		log.Printf("last.fm response:\n%s", xmlData)
		return nil, errors.New("No images found")
	}

	// Find the best available image and download it to a
	// temporary file.
	sort.Sort(SortedLastFmImages{meta.Images})
	bestImage := meta.Images[0]

	tmpf, err := ioutil.TempFile("", "albumart_")
	if err != nil {
		return nil, err
	}
	tmpName := tmpf.Name()
	defer os.Remove(tmpName)

	imgresp, err := http.Get(bestImage.Url)
	if err != nil {
		return nil, err
	}
	defer imgresp.Body.Close()
	if imgresp.StatusCode != 200 {
		return nil, errors.New("Bad status code")
	}

	io.Copy(tmpf, imgresp.Body)
	tmpf.Close()

	// Convert the downloaded image to JPEG.
	return convertToJpeg(tmpName)
}

func convertToJpeg(filename string) ([]byte, error) {
	return exec.Command("gm", "convert", filename, "-quality", "70", "jpeg:-").Output()
}
