package albumart

import (
	"bytes"
	"encoding/base64"
	"io"
	"log"
	"strings"
	"time"

	"git.autistici.org/ale/djrandom/services"
)

const albumArtBucket = "albumart"

var emptyJpeg = []byte{0}

// AlbumArtEntry is what we store in the database. It can either
// contain the actual image data, or be a negative-cache entry with a
// ttl (in which case, Img will be nil).
type AlbumArtEntry struct {
	Img       []byte
	Timestamp int64
}

func (e *AlbumArtEntry) HasExpired() bool {
	return (e.Img == nil && time.Now().Unix()-e.Timestamp > (86400*7))
}

// AlbumArtCache retrieves album art from last.fm and caches it in the
// global database.
type AlbumArtCache struct {
	apiKey string
	db     services.Database
}

func NewAlbumArtCache(db services.Database, apiKey string) *AlbumArtCache {
	return &AlbumArtCache{apiKey, db}
}

func (ac *AlbumArtCache) makeKey(artist, album string) string {
	var buf bytes.Buffer
	io.WriteString(&buf, strings.ToLower(artist))
	io.WriteString(&buf, "\n")
	io.WriteString(&buf, strings.ToLower(album))
	return base64.StdEncoding.EncodeToString(buf.Bytes())
}

// GetAlbumArt returns a JPEG-encoded image for the specified album. If
// no image could be found, an empty 1x1 blank JPEG is returned.
func (ac *AlbumArtCache) GetAlbumArt(artist, album string) []byte {
	s, err := ac.db.NewSession()
	if err != nil {
		return nil
	}

	// Attempt to retrieve the image from db.
	key := ac.makeKey(artist, album)
	var data AlbumArtEntry
	if ac.db.Get(s, albumArtBucket, key, &data) != nil || data.HasExpired() {

		// Attempt to query Last.fm and save results to the db.
		imageData, err := queryLastFm(ac.apiKey, artist, album)
		if err != nil {
			log.Printf("Error downloading album art for \"%s / %s\": %s", artist, album, err)
			data.Img = nil
		} else {
			log.Printf("Downloaded album art for \"%s / %s\" (%d bytes)", artist, album, len(imageData))
			data.Img = imageData
		}
		data.Timestamp = time.Now().Unix()
		ac.db.Put(s, albumArtBucket, key, &data)
	}

	if data.Img == nil {
		return emptyJpeg
	}
	return data.Img
}
