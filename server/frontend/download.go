package frontend

import (
	"log"
	"net/http"
	"net/http/httputil"

	"github.com/gorilla/mux"

	"git.autistici.org/ale/djrandom/services"
)

func downloadProxyHandler(storage services.Storage, key string, w http.ResponseWriter, r *http.Request) {
	storageDirector := storage.ReverseProxyDirector()
	director := func(r *http.Request) {
		log.Printf("Download(%s)", key)
		storageDirector(key, r)
	}

	// Note: if we set FlushInterval, we get nil pointer errors
	// when Go tries to flush a connection that doesn't exist
	// anymore. So, don't use it for now.
	reverse := httputil.ReverseProxy{
		Director: director,
		// FlushInterval: (2 * time.Second),
	}
	reverse.ServeHTTP(w, r)
}

// Download retrieves the song data from storage.
func Download(w http.ResponseWriter, r *djRequest) {
	// Glue together the storage reverseproxy director with our
	// routing map.
	key := mux.Vars(r.Request)["id"]
	downloadProxyHandler(r.Ctx.Storage, key, w, r.Request)
}

/**

// Download retrieves the song data from storage.
func DownloadOLD(w http.ResponseWriter, r *djRequest) {
	vars := mux.Vars(r.Request)
	audioId := vars["id"]
	if audioId == "" {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}
	log.Printf("Download(%s)", audioId)

	// Perform a meta-data lookup, to get information such as
	// MIME type and file size.
	af, ok := r.Ctx.Db.GetAudioFile(audioId)
	if !ok {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	// Open the backend file and stream data to the client.
	file, err := r.Ctx.Storage.Open(audioId)
	if err != nil {
		log.Printf("Download(%s) not found on backend: %s", audioId, err)
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	defer file.Close()

	w.Header().Set("Content-Length", fmt.Sprintf("%d", af.Size))
	w.Header().Set("Content-Type", flacContentType)
	io.Copy(w, file)
}

**/
