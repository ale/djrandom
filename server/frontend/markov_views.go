package frontend

import (
	"encoding/json"
	"log"
	"net/http"
	"sync"
	"time"

	"git.autistici.org/ale/djrandom/api"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
)

type markovMapCacheEntry struct {
	m     *api.MarkovMap
	stamp time.Time
}

type markovMapCache struct {
	ttl   time.Duration
	cache map[string]markovMapCacheEntry
	lock  sync.Mutex
}

func newMarkovMapCache() *markovMapCache {
	return &markovMapCache{
		ttl:   43200 * time.Second,
		cache: make(map[string]markovMapCacheEntry),
	}
}

func (c *markovMapCache) Get(user string, r *djRequest) *api.MarkovMap {
	c.lock.Lock()
	defer c.lock.Unlock()

	now := time.Now()
	entry, ok := c.cache[user]
	if !ok || now.Sub(entry.stamp) > c.ttl {
		var m *api.MarkovMap
		if mm, ok := r.Ctx.Db.GetMarkovMap(nil, user); ok {
			m = mm
		}
		entry = markovMapCacheEntry{m, now}
		c.cache[user] = entry
	}
	return entry.m
}

var mmCache *markovMapCache

func init() {
	mmCache = newMarkovMapCache()
}

type taggedMarkovMap struct {
	m   *api.MarkovMap
	tag string
}

func findNextSong(curSong api.SongID, r *djRequest) (api.SongID, string, bool) {
	// Retrieve the Markov map for this user and the global one
	// (keep them properly tagged so that we can mark the right
	// origin later).
	maps := make([]taggedMarkovMap, 0, 2)
	for _, mapinfo := range []struct {
		tag  string
		user string
	}{{"user", r.AuthUser}, {"global", api.MarkovGlobalKey}} {
		m := mmCache.Get(mapinfo.user, r)
		if m != nil {
			maps = append(maps, taggedMarkovMap{m, mapinfo.tag})
		}
	}

	// Search for a Markov-generated result first.
	if curSong != api.NilSongID {
		for _, mapinfo := range maps {
			if song, ok := mapinfo.m.GetSong(curSong); ok && song != api.NilSongID {
				return song, "markov/" + mapinfo.tag, true
			}
		}
	}

	// If nothing has been found so far, just pick a random song
	// from the global map (if present, otherwise fall back to the
	// personal one).
	if len(maps) > 0 {
		globalMap := maps[len(maps)-1]
		song := globalMap.m.GetRandomSong()
		if song != api.NilSongID {
			return song, "random/" + globalMap.tag, true
		}
	}

	return api.NilSongID, "", false
}

// Suggest builds a (short) playlist based on a seed, using a mix of
// the Markov models for the user and the global one. The function
// will try to generate a playlist containing the requested number of
// songs; if at any step the Markov models can't provide a suggestion,
// a random song will be inserted.
func Suggest(w http.ResponseWriter, r *djRequest) {
	var suggestReq api.SuggestRequest
	if err := json.NewDecoder(r.Request.Body).Decode(&suggestReq); err != nil {
		log.Printf("Suggest(): Bad request: %s", err)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	n := suggestReq.NumResults
	if n < 0 {
		n = 1
	}
	songIds := make([]api.SongID, 0, n)
	songOrigin := make([]string, 0, n)

	// Iterate 'n' times a call to findNextSong, building an
	// ordered playlist.
	var curSong api.SongID
	if len(suggestReq.CurrentSongs) > 0 {
		curSong = suggestReq.CurrentSongs[len(suggestReq.CurrentSongs)-1]
	}
	for i := 0; i < n; i++ {
		song, origin, ok := findNextSong(curSong, r)
		if !ok {
			// We've been unable to find a random song?
			log.Printf("Suggest(): findNextSong(%s) returned nil, aborting early", curSong)
			break
		}
		log.Printf("Suggest(): findNextSong(%s) -> %s - %s", curSong, song, origin)
		songIds = append(songIds, song)
		songOrigin = append(songOrigin, origin)
		curSong = song
	}

	// Retrieve all song info in parallel.
	songs, err := db_client.ParallelFetchSongs(r.Ctx.Db, songIds)
	if err != nil {
		log.Printf("Search(): %s", err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	resp := api.SuggestResponse{
		Results:    songs,
		SongOrigin: songOrigin,
	}
	sendJsonResponse(w, &resp)
}
