// Front-end HTTP server for djrandom.

package main

import (
	"flag"
	"log"
	"net/rpc"
	"strings"

	"git.autistici.org/ale/djrandom/server/frontend"
	"git.autistici.org/ale/djrandom/server/frontend/albumart"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/services/index"
	"git.autistici.org/ale/djrandom/services/storage"
	"git.autistici.org/ale/djrandom/services/tasks"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/daemon"
)

var (
	configFile   = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
	addr         = flag.String("addr", ":3001", "TCP address to listen on")
	xsrfSecret   = config.String("xsrf_secret", "", "XSRF secret")
	lastFmApiKey = config.String("last_fm_api_key", "", "Last.fm API key")
)

func main() {
	flag.Parse()
	config.MustParse(*configFile)

	daemon.EnableStatusPage = false
	daemon.Setup()

	// Create client stubs for all our services.
	db := db_client.NewDatabaseImplFromConfig()
	storageClient := storage.NewDistributedStorageClientFromConfig()
	indexClient := index.NewIndexClientFromConfig()
	taskClient := tasks.NewTaskClientFromConfig(db)

	lastFmApiKeyStr := strings.TrimSpace(*lastFmApiKey)
	aaCache := albumart.NewAlbumArtCache(db, lastFmApiKeyStr)

	// Bundle everything into an AppContext.
	ac := frontend.AppContext{
		Db:         db,
		Storage:    storageClient,
		TaskClient: taskClient,
		Index:      indexClient,
		AlbumArt:   aaCache,
		XsrfSecret: *xsrfSecret,
	}

	frontend.RegisterService(&ac)

	// Start the RPC HTTP handler for debug functions.
	rpc.HandleHTTP()

	log.Printf("Starting HTTP front-end server on %s", *addr)
	err := util.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
