package frontend

import (
	"bytes"
	"html/template"
	"log"
	"net/http"
	"path/filepath"

	"golang.org/x/net/xsrftoken"
	"github.com/gorilla/mux"
	"github.com/gorilla/sessions"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/api/fsapi"
	"git.autistici.org/ale/djrandom/server/frontend/albumart"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/http_auth"
	"git.autistici.org/ale/djrandom/util/webdav"
)

var (
	// HTML templates and static files location.
	templatesDir = config.String("templates-dir", "./ui/templates", "Location of templates")
	staticDir    = config.String("static-dir", "./ui/static", "Location of static files")
	staticPrefix = "/static/"

	// URL for client download.
	clientDownloadURL = config.String("client-download-url", "http://www.incal.net/ale/djrandom/", "URL to download the client from")

	// Session name.
	sessionName = "djsession"
)

type AppContext struct {
	// Services.
	Db         services.Database
	Storage    services.Storage
	TaskClient services.TaskClient
	Index      services.Index

	// HTTP server specific runtime globals.
	AlbumArt     *albumart.AlbumArtCache
	SessionStore sessions.Store
	Template     *template.Template
	XsrfSecret   string
}

type djRequest struct {
	Ctx      *AppContext
	Session  *sessions.Session
	Request  *http.Request
	AuthUser string
}

type djHandler interface {
	ServeHTTP(w http.ResponseWriter, r *djRequest)
}

type djHandlerFunc func(w http.ResponseWriter, r *djRequest)

func (f djHandlerFunc) ServeHTTP(w http.ResponseWriter, r *djRequest) {
	f(w, r)
}

// Authenticate attempts to authenticate the specified user.
func (ac *AppContext) Authenticate(username, password string) (*api.User, error) {
	user, ok := ac.Db.GetUser(nil, username)

	// Allow login if the password matches and the user exists and
	// is active.
	if ok && user.State == "active" && user.CheckPassword(password) {
		return user, nil
	}

	return nil, util.AuthFailed
}

// RenderTemplate safely renders a template for an HTTP response.
func (ac *AppContext) RenderTemplate(w http.ResponseWriter, templateName string, ctx interface{}) {
	var buf bytes.Buffer
	err := ac.Template.ExecuteTemplate(&buf, templateName, ctx)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Write(buf.Bytes())
}

// NewXsrfToken creates a new XSRF token, and sets it in the current session.
// It assumes that the user is authenticated.
func (r *djRequest) NewXsrfToken(w http.ResponseWriter) string {
	token := xsrftoken.Generate(r.Ctx.XsrfSecret, r.AuthUser, "post")
	r.Session.Values["_xsrf"] = token
	r.Session.Save(r.Request, w)
	return token
}

// ValidateXsrf checks whether the XSRF token matches in the input form
// and the current session.
func (r *djRequest) ValidateXsrf() bool {
	formToken := r.Request.FormValue("_xsrf")
	sessionToken := r.Session.Values["_xsrf"]
	return (formToken != "" && sessionToken != "" && formToken == sessionToken &&
		xsrftoken.Valid(formToken, r.Ctx.XsrfSecret, r.AuthUser, "post"))
}

// doLogin sets session values after logging in.
func doLogin(w http.ResponseWriter, r *djRequest, user *api.User) {
	r.Session.Values["logged_in"] = true
	r.Session.Values["user"] = user.Email
	r.Session.Save(r.Request, w)
}

// Check if a user is logged in. Sets r.AuthUser on success.
func checkUserLogin(r *djRequest) bool {
	loggedIn, ok := r.Session.Values["logged_in"]
	if ok && loggedIn.(bool) {
		r.AuthUser = r.Session.Values["user"].(string)
		return true
	}
	return false
}

// WithUserAuth wraps a djHandler with session-based authentication.
func WithUserAuth(h djHandler) djHandler {
	return djHandlerFunc(func(w http.ResponseWriter, r *djRequest) {
		if !checkUserLogin(r) {
			// Redirect to login URL.
			w.Header().Set("Location", "/login")
			w.WriteHeader(http.StatusFound)
			return
		}
		h.ServeHTTP(w, r)
	})
}

// WithApiAuth wraps a djHandler with API-key based authentication.
// Session-based authentication is tried first (for interactive usage).
func WithApiAuth(h djHandler) djHandler {
	return djHandlerFunc(func(w http.ResponseWriter, r *djRequest) {
		authKeyFn := func(key string) (*api.AuthKey, bool) {
			return r.Ctx.Db.GetAuthKey(nil, key)
		}

		// Check for session-based auth first.
		if !checkUserLogin(r) {

			// Attempt to decode an API key.
			apiKey, err := util.AuthenticateHttpRequest(authKeyFn, r.Request)
			if err != nil {
				switch err {
				case util.Unauthorized:
					log.Printf("Unauthorized: %v", r.Request)
					http.Error(w, err.Error(), 401)
				case util.AuthFailed:
					log.Printf("AuthFailed: %v", r.Request)
					http.Error(w, err.Error(), 403)
				default:
					http.Error(w, err.Error(), 400)
				}
				return
			}

			r.AuthUser = apiKey.User
		}

		h.ServeHTTP(w, r)
	})
}

// WithBasicAuth enforces usage of Basic HTTP authentication.
func WithBasicAuth(h djHandler) djHandler {
	return djHandlerFunc(func(w http.ResponseWriter, r *djRequest) {
		basicAuth, err := http_auth.NewBasicAuthFromRequest(r.Request)
		if err != nil {
			w.Header().Set("WWW-Authenticate", "Basic realm=\"djrandom\"")
			http.Error(w, err.Error(), 401)
			return
		}
		authKey, ok := r.Ctx.Db.GetAuthKey(nil, basicAuth.Username)
		if !ok || authKey.Secret != basicAuth.Password {
			w.Header().Set("WWW-Authenticate", "Basic realm=\"djrandom\"")
			http.Error(w, "Unauthorized", 403)
			return
		}
		h.ServeHTTP(w, r)
	})
}

func WithRequest(ac *AppContext, h djHandler) http.Handler {
	sessionopts := sessions.Options{
		MaxAge:   0,
		HttpOnly: true,
		// Secure:   true,
	}
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		session, _ := ac.SessionStore.Get(r, sessionName)
		session.Options = &sessionopts
		djreq := djRequest{
			Ctx:     ac,
			Session: session,
			Request: r,
		}
		h.ServeHTTP(w, &djreq)
	})
}

type NavLink struct {
	Name    string
	Title   string
	Icon    string
	Link    string
	Current bool
}

type templateContext struct {
	NavLinks []NavLink
	AuthUser string
	ClientDownloadURL string
}

// Create a default context to render templates.
func newTemplateContext(curPage string, r *djRequest) *templateContext {
	var t templateContext
	if r != nil {
		t.NavLinks = []NavLink{
			{"index", "Play", "", "/", (curPage == "index")},
			{"about", "About", "", "/about", (curPage == "about")},
			{"user", r.AuthUser, "icon-cog", "/user", (curPage == "user")},
		}
		t.AuthUser = r.AuthUser
	}
	t.ClientDownloadURL = *clientDownloadURL
	return &t
}

func RegisterService(ac *AppContext) {

	// Load the templates.
	ac.SessionStore = sessions.NewCookieStore([]byte("secret"))
	ac.Template = template.Must(
		template.ParseGlob(
			filepath.Join(*templatesDir, "*.html")))

	// Serve static files.
	http.Handle(staticPrefix,
		http.StripPrefix(staticPrefix,
			http.FileServer(http.Dir(*staticDir))))

	rootMux := mux.NewRouter()

	// API endpoints.
	rootMux.Handle("/api/song/{id}", WithRequest(ac, WithApiAuth(djHandlerFunc(GetSongInfo)))).Methods("GET")
	rootMux.Handle("/api/songs", WithRequest(ac, WithApiAuth(djHandlerFunc(GetManySongsInfo)))).Methods("POST")
	rootMux.Handle("/api/album_art", WithRequest(ac, WithApiAuth(djHandlerFunc(GetAlbumArt)))).Methods("GET")
	rootMux.Handle("/api/search_ids", WithRequest(ac, WithApiAuth(djHandlerFunc(SearchIds)))).Methods("POST")
	rootMux.Handle("/api/search", WithRequest(ac, WithApiAuth(djHandlerFunc(Search)))).Methods("POST")
	rootMux.Handle("/api/check_fp", WithRequest(ac, WithApiAuth(djHandlerFunc(CheckFingerprints)))).Methods("POST")
	rootMux.Handle("/api/add_playlog", WithRequest(ac, WithApiAuth(djHandlerFunc(AddPlayLog)))).Methods("POST")
	rootMux.Handle("/api/suggest", WithRequest(ac, WithApiAuth(djHandlerFunc(Suggest)))).Methods("POST")
	rootMux.Handle("/api/autocomplete/artist", WithRequest(ac, WithApiAuth(djHandlerFunc(ArtistAutocomplete)))).Methods("GET")

	rootMux.Handle("/api/user/get_auth_keys", WithRequest(ac, WithApiAuth(djHandlerFunc(UserGetAuthKeys)))).Methods("GET")
	rootMux.Handle("/api/user/create_auth_key", WithRequest(ac, WithApiAuth(djHandlerFunc(UserCreateAuthKey)))).Methods("POST")
	rootMux.Handle("/api/user/delete_auth_key", WithRequest(ac, WithApiAuth(djHandlerFunc(UserDeleteAuthKey)))).Methods("POST")

	// Upload/download.
	rootMux.Handle("/upload", WithRequest(ac, WithApiAuth(djHandlerFunc(Upload)))).Methods("PUT")
	rootMux.Handle("/dl/{id}", WithRequest(ac, WithApiAuth(djHandlerFunc(Download))))

	// Filesystem-like interface.
	artistFs := fsapi.NewByArtistFilesystem(ac.Db, ac.Index)

	// DAV handler.
	davfs := NewDavFs(artistFs, ac.Storage)
	dav := webdav.NewWebDAV("/dav/", davfs)
	davWrapper := func(w http.ResponseWriter, r *djRequest) {
		dav.ServeHTTP(w, r.Request)
	}
	rootMux.PathPrefix("/dav/").Handler(WithRequest(ac, WithBasicAuth(djHandlerFunc(davWrapper))))

	// JSON filesystem API (only exports metadata).
	jsonfs := NewJsonFs(artistFs)
	rootMux.PathPrefix("/jsonfs/").Handler(WithRequest(ac, WithApiAuth(jsonfs)))

	// User-facing views.
	rootMux.Handle("/user", WithRequest(ac, WithUserAuth(djHandlerFunc(UserPage)))).Methods("GET", "POST")
	rootMux.Handle("/login", WithRequest(ac, djHandlerFunc(Login))).Methods("GET", "POST")
	rootMux.Handle("/activate/{token}", WithRequest(ac, djHandlerFunc(ActivateUser))).Methods("GET", "POST")
	rootMux.Handle("/about", WithRequest(ac, WithUserAuth(djHandlerFunc(About))))
	rootMux.Handle("/", WithRequest(ac, WithUserAuth(djHandlerFunc(Index))))

	http.Handle("/", rootMux)
}
