package frontend

import (
	"bytes"
	"errors"
	"log"
	"net/http"
	"net/smtp"
	"net/url"
	"regexp"
	"text/template"
	"time"

	"github.com/gorilla/mux"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/secure_token"
)

var (
	// A very naive email regexp, just to catch trivial syntax errors.
	emailRx = regexp.MustCompile(`^[-_.a-zA-Z0-9]+@[-.a-z0-9]+$`)

	// Outgoing email config.
	smtpServer      = config.String("smtp_server", "localhost:25", "SMTP server address")
	emailSenderAddr = config.String("email_sender_addr", "", "Email sender address")
	publicUrl       = config.String("public_url", "", "Public URL")

	// The template for activation emails.
	activationText = `From: DJRandom <{{.Sender}}>
To: {{.Recipient}}
Subject: Activate your DJRandom account
MIME-Version: 1.0
Content-Type: multipart/mixed; boundary={{.Boundary}}

--{{.Boundary}}
Content-Type: text/plain
Content-Transfer-Encoding: 8bit

Hello,

please click on the following link to activate your DJRandom account:

{{.ActivationLink}}


--{{.Boundary}}--`

	activationTmpl = template.Must(template.New("activation").Parse(activationText))

	activateVerb = "activate"
)

func Login(w http.ResponseWriter, r *djRequest) {
	var ctx struct {
		Email        string
		ErrorMessage string
		XsrfToken    string
	}

	// Attempt to authenticate the user.
	if r.Request.Method == "POST" && r.ValidateXsrf() {
		email := r.Request.FormValue("email")
		password := r.Request.FormValue("password")
		user, err := r.Ctx.Authenticate(email, password)
		if err != nil {
			ctx.Email = email
			ctx.ErrorMessage = "Authentication Failed"
		} else {
			doLogin(w, r, user)
			// Redirect to post-auth page...
			w.Header().Set("Location", "/")
			w.WriteHeader(http.StatusFound)
			return
		}
	}
	ctx.XsrfToken = r.NewXsrfToken(w)

	r.Ctx.RenderTemplate(w, "login.html", &ctx)
}

func ActivateUser(w http.ResponseWriter, r *djRequest) {
	var ctx struct {
		Email        string
		ErrorMessage string
		XsrfToken    string
	}

	// Decode and verify the token.
	token := mux.Vars(r.Request)["token"]
	email, err := secure_token.Validate(token, activateVerb)
	if err != nil {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	// Find the User object.
	session, err := r.Ctx.Db.NewSession()
	if err != nil {
		log.Printf("ActivateUser(%s): error creating session: %s", email, err)
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}
	defer session.Close()
	user, ok := r.Ctx.Db.GetUser(session, email)
	if !ok {
		log.Printf("ActivateUser(%s): no such user", email)
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	if user.State != "pending" {
		log.Printf("ActivateUser(%s): user in state '%s'", email, user.State)
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	user.State = "active"

	// Set the new password.
	if r.Request.Method == "POST" && r.ValidateXsrf() {
		if err := changePassword(session, user, r); err != nil {
			ctx.ErrorMessage = err.Error()
		} else {
			// Everything done, simulate a login.
			doLogin(w, r, user)
			// Redirect to post-auth page...
			w.Header().Set("Location", "/")
			w.WriteHeader(http.StatusFound)
			return
		}
	}

	ctx.Email = email
	ctx.XsrfToken = r.NewXsrfToken(w)
	r.Ctx.RenderTemplate(w, "activate.html", &ctx)
}

type alertMsg struct {
	Msg  string
	Type string
}

func UserPage(w http.ResponseWriter, r *djRequest) {
	session, err := r.Ctx.Db.NewSession()
	if err != nil {
		log.Printf("UserPage(%s): error creating session: %s", r.AuthUser, err)
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}
	defer session.Close()

	user, ok := r.Ctx.Db.GetUser(session, r.AuthUser)
	if !ok || user == nil {
		log.Printf("UserPage(%s): error retrieving user", r.AuthUser)
		http.Error(w, "Internal Error", http.StatusInternalServerError)
		return
	}

	// Perform any user-requested actions.
	var msgs []alertMsg
	if r.Request.Method == "POST" && r.ValidateXsrf() {
		var okMsg string
		var err error
		formId := r.Request.FormValue("form_id")
		switch {
		case formId == "change_pw":
			err = changePassword(session, user, r)
			okMsg = "Password changed successfully"
		case formId == "create_api_key":
			err = createApiKey(session, user, r)
			okMsg = "Created new API key"
		case formId == "invite":
			err = invite(session, user, r)
			okMsg = "Invite sent"
		default:
			http.Error(w, err.Error(), http.StatusBadRequest)
			return
		}
		if err != nil {
			msgs = append(msgs, alertMsg{err.Error(), "error"})
		} else {
			msgs = append(msgs, alertMsg{okMsg, "info"})
		}
	}

	// Build the template context.
	ctx := struct {
		*templateContext
		User      *api.User
		AuthKeys  []*api.AuthKey
		Alerts    []alertMsg
		XsrfToken string
	}{newTemplateContext("user", r), user, getUserAuthKeys(session, user, r), msgs, r.NewXsrfToken(w)}

	r.Ctx.RenderTemplate(w, "user.html", &ctx)
}

func getUserAuthKeys(session services.Session, user *api.User, r *djRequest) []*api.AuthKey {
	out := make([]*api.AuthKey, 0, len(user.AuthKeyIds))
	for _, keyId := range user.AuthKeyIds {
		if key, ok := r.Ctx.Db.GetAuthKey(session, keyId); ok {
			out = append(out, key)
		}
	}
	return out
}

func invite(session services.Session, user *api.User, r *djRequest) error {
	email := r.Request.FormValue("email")
	if email == "" {
		return errors.New("No email provided")
	}
	if !emailRx.MatchString(email) {
		return errors.New("that doesn't look like an email address")
	}
	if user.InvitesLeft <= 0 {
		return errors.New("No more invites left")
	}

	// Check if the user already exists.
	if _, ok := r.Ctx.Db.GetUser(session, email); ok {
		return errors.New("User already exists")
	}

	// Create a new user in 'pending' state.
	newUser := &api.User{
		Email:       email,
		CreatedAt:   time.Now().Unix(),
		InvitedBy:   user.Email,
		InvitesLeft: 2,
		State:       "pending",
	}
	err := r.Ctx.Db.PutUser(session, newUser)
	if err != nil {
		return err
	}

	// Send the activation email.
	sendActivationEmail(email)

	// Update the number of invites left for the current user.
	// Note that this is a test-and-set operation without a lock
	// on the object that is being modified, so it is inherently
	// unreliable.
	//
	// Also, if there is a database error here, log it but do not
	// cause the request to fail (since the invite has already
	// been sent).
	user.InvitesLeft--
	r.Ctx.Db.PutUser(session, user)
	return nil
}

func changePassword(session services.Session, user *api.User, r *djRequest) error {
	password := r.Request.FormValue("password")
	passwordConfirm := r.Request.FormValue("password_confirm")
	if password == "" || passwordConfirm == "" {
		return errors.New("Passwords must not be empty")
	}
	if password != passwordConfirm {
		return errors.New("You typed two different passwords")
	}
	if len(password) < 6 {
		return errors.New("Passwords must be at least 6 characters")
	}

	user.SetPassword(password)
	return r.Ctx.Db.PutUser(session, user)
}

func createApiKey(session services.Session, user *api.User, r *djRequest) error {
	if err := r.Ctx.Db.PutAuthKey(session, user.NewAuthKey()); err != nil {
		return err
	}
	return r.Ctx.Db.PutUser(session, user)
}

func newBoundary() string {
	// TODO: return a random string.
	return "MIMEUNIQUEBOUNDARY"
}

func sendActivationEmail(email string) {
	// Build the activation URL. The link is valid for 1 month.
	activationToken := secure_token.NewSecureToken(activateVerb, email, 30*86400).Encode()
	u, _ := url.Parse(*publicUrl)
	u.Path = "/activate/" + activationToken
	activationUrl := u.String()
	log.Printf("Generating activation email for %s - link: %s", email, activationUrl)

	var body bytes.Buffer
	ctx := struct {
		Sender         string
		Recipient      string
		ActivationLink string
		Boundary       string
	}{*emailSenderAddr, email, activationUrl, newBoundary()}
	if err := activationTmpl.Execute(&body, ctx); err != nil {
		log.Printf("Error generating activation message for %s: %s", email, err)
		return
	}

	err := smtp.SendMail(
		*smtpServer,
		nil,
		*emailSenderAddr,
		[]string{email},
		body.Bytes(),
	)
	if err != nil {
		log.Printf("Error sending activation email to %s: %s", email, err)
	}
}
