#!/bin/sh
#
# Create a self-contained binary distribution of the DJRandom clients,
# including all the dynamic libraries.
#

set -e

OS=$(uname)

tempdir=$(mktemp -d /tmp/djrandom-build.tmp.XXXXXX)
trap "trap - EXIT ; rm -fr ${tempdir} ; exit 0" EXIT INT TERM

root="${tempdir}/djrandom-client"
mkdir "${root}"
mkdir "${root}/bin"
mkdir "${root}/lib"

find_local_libs() {
    local local_lib_dir=/usr/local
    local binary="$1"
    case "$OS" in
        Linux)
            ldd "${binary}" | awk '/=> \// {print $3}' | fgrep "${local_lib_dir}"
            ;;
        Darwin)
            otool -L "${binary}" | fgrep "${local_lib_dir}" | awk '{ print $1 }'
            ;;
        *)
            echo "Error: can't find dynamic libraries on this platform!" 1>&2
            exit 2
            ;;
    esac
}

install_local_libs() {
    local dst
    for lib in $(find_local_libs "$1") ; do
        dst="${root}/lib/$(basename "${lib}")"
        cp "${lib}" "${dst}"
        chmod 0755 "${dst}"
    done
}

install_binary() {
    local name="$1"
    local src="$2"
    local dst="${root}/bin/${name}"
    local real_dst="${dst}.real"

    go build -o "${real_dst}" "${src}"

    install_local_libs "${real_dst}"

    cat >"${dst}" <<EOF
#!/bin/sh
bindir=\$(dirname "\$0")
libdir="\${bindir:-.}/../lib"
export LD_LIBRARY_PATH="\${libdir}\${LD_LIBRARY_PATH:+:}\${LD_LIBRARY_PATH}"
export DYLD_LIBRARY_PATH="\${libdir}\${DYLD_LIBRARY_PATH:+:}\${DYLD_LIBRARY_PATH}"
exec "\${bindir}/${name}.real" "\$@"
EOF
    chmod 0755 "${dst}"
}

create_archive() {
    local platform
    case "$OS" in
        Linux) platform=linux ;;
        Darwin) platform=osx ;;
        *)
            echo "Error: unknown platform: ${OS}" 1>&2
            exit 2
            ;;
    esac
    local arch=$(uname -m)

    tar_name="djrandom-client-${platform}-${arch}.tar.gz"
    tar -C "${tempdir}" -czvf "${tar_name}" djrandom-client
    ls -l "${tar_name}"
}

install_binary djuploader client/upload/djuploader/djuploader.go
install_binary djplay client/djplay/djplay.go
install_binary djmpd client/mpd/djmpd/djmpd.go
create_archive
