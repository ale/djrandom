package mapreduce

import (
	"flag"
	"log"

	"git.autistici.org/ale/djrandom/services"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/services/index"
	"git.autistici.org/ale/djrandom/services/storage"
	"git.autistici.org/ale/djrandom/services/tasks"
	"git.autistici.org/ale/djrandom/util/config"
	"github.com/dgryski/dmrgo"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
)

type MapReduce struct {
	Db      services.Database
	Session services.Session
	Index   services.Index
	Storage services.Storage
	Task    services.TaskClient

	proto dmrgo.StreamProtocol
}

type Options struct {
	NoDatabase    bool
	NoSession     bool
	HasIndex      bool
	HasStorage    bool
	HasTaskClient bool
}

func (mr *MapReduce) Init(options *Options) {
	// Force protocol to JSON.
	mr.proto = &dmrgo.JSONProtocol{}

	// Connect to standard services, as requested.
	if options == nil {
		options = &Options{}
	}
	if !options.NoDatabase {
		mr.Db = db_client.NewDatabaseImplFromConfig()
	}
	if !options.NoSession {
		var err error
		mr.Session, err = mr.Db.NewSession()
		if err != nil {
			log.Fatal(err)
		}
	}
	if options.HasIndex {
		mr.Index = index.NewIndexClientFromConfig()
	}
	if options.HasStorage {
		mr.Storage = storage.NewDistributedStorageClientFromConfig()
	}
	if options.HasTaskClient {
		mr.Task = tasks.NewTaskClientFromConfig(mr.Db)
	}
}

func (mr *MapReduce) Emit(emitter dmrgo.Emitter, key, value interface{}) {
	kv := mr.proto.MarshalKV(key, value)
	emitter.Emit(kv.Key, kv.Value)
}

func (mr *MapReduce) Unmarshal(key string, values []string, outKey, outVal interface{}) {
	mr.proto.UnmarshalKVs(key, values, outKey, outVal)
}

type MapReduceJob interface {
	dmrgo.MapReduceJob

	Init(*Options)
}

func Main(mr MapReduceJob, options *Options) {
	flag.Parse()
	config.Parse(*configFile)

	mr.Init(options)

	dmrgo.Main(mr)
}
