package main

import (
	"flag"
	"log"

	"github.com/dgryski/dmrgo"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/api/task_api"
	"git.autistici.org/ale/djrandom/mapreduce"
	"git.autistici.org/ale/djrandom/server/actions/plans"
)

var (
	planName = flag.String("plan", "incoming", "Plan to execute")
	state    = flag.String("state", "", "Filter object with this state")
)

type MRReprocess struct {
	mapreduce.MapReduce
	plan *task_api.Plan
}

func (mr *MRReprocess) Map(key string, value string, emitter dmrgo.Emitter) {
	songId, err := api.ParseSongID(value)
	if err != nil {
		log.Println(err)
		return
	}

	if *state != "" {
		song, ok := mr.Db.GetSong(mr.Session, songId)
		if !ok {
			return
		}
		if song.Info.State != *state {
			return
		}
	}

	mr.Task.CreateJob(mr.plan, value)
}

func (mr *MRReprocess) MapFinal(emitter dmrgo.Emitter) {
}

func (mr *MRReprocess) Reduce(key string, values []string, emitter dmrgo.Emitter) {
}

func (mr *MRReprocess) Init(options *mapreduce.Options) {
	mr.MapReduce.Init(options)
	var ok bool
	mr.plan, ok = plans.KnownPlans[*planName]
	if !ok {
		log.Fatal("Unknown plan")
	}
}

func main() {
	mapreduce.Main(&MRReprocess{}, &mapreduce.Options{
		HasTaskClient: true,
	})
}
