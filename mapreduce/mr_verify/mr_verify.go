// Verifies that the index contents match what's stored in the
// database. It's a full sync, so it will remove elements from the
// index which are not in the db anymore (or whose status isn't
// "active").
//
// Right now this scans the entire index at once, which won't scale
// very well if the number of songs grows too large.
//
// If you need to rebuild the index in its entirety, you can pass the
// --rebuild option to this script, in which case every song will
// be re-indexed.
//
package main

import (
	"flag"
	"log"
	"strings"

	"github.com/dgryski/dmrgo"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/server/actions/plans"
	"git.autistici.org/ale/djrandom/services"

	"git.autistici.org/ale/djrandom/mapreduce"
)

var (
	rebuild                = flag.Bool("rebuild", false, "Fully rebuild the index")
	reprocessInactiveFiles = flag.Bool("reprocess-inactive", false, "Reprocess inactive files")

	// Action plans to fix tasks.
	incomingPlan = "incoming"
	reencodePlan = "reencode"
)

func hasWs(s string) bool {
	return strings.HasPrefix(s, " ") || strings.HasSuffix(s, " ")
}

type songChecker struct {
	db services.Database
	tc services.TaskClient
}

func newSongChecker(db services.Database, tc services.TaskClient) *songChecker {
	return &songChecker{db, tc}
}

func (c *songChecker) checkSongMeta(song *api.Song) bool {
	var fixes []string

	update := false
	// Check for extra whitespace in imports from old database.
	if hasWs(song.Meta.Artist) {
		song.Meta.Artist = strings.TrimSpace(song.Meta.Artist)
		fixes = append(fixes, "artist")
		update = true
	}
	if hasWs(song.Meta.Title) {
		song.Meta.Title = strings.TrimSpace(song.Meta.Title)
		fixes = append(fixes, "title")
		update = true
	}
	if hasWs(song.Meta.Album) {
		song.Meta.Album = strings.TrimSpace(song.Meta.Album)
		fixes = append(fixes, "album")
		update = true
	}
	genreLo := strings.ToLower(song.Meta.Genre)
	if genreLo != song.Meta.Genre {
		song.Meta.Genre = genreLo
		fixes = append(fixes, "genre")
		update = true
	}

	if update {
		log.Printf("song had badly formatted metadata: %v", fixes)
	}
	return update
}

func (c *songChecker) checkStatus(song *api.Song) bool {
	// If a song has artist and title set, and is inactive, check
	// it again, because that's odd.
	if song.Info.State == "inactive" && song.Meta.Artist != "" && song.Meta.Title != "" {
		log.Printf("song is inactive but probably shouldn't be")
		if *reprocessInactiveFiles {
			plan := plans.KnownPlans[incomingPlan]
			c.tc.CreateJob(plan, song.Id.String())
		}
	}
	return false
}

func (c *songChecker) checkMp3(song *api.Song) bool {
	// Check that the song has one mp3 audio file, otherwise
	// trigger re-encoding.
	hasMp3 := false
	for _, f := range song.Files {
		if f.MimeType == api.MIMETYPE_MP3 {
			hasMp3 = true
			break
		}
	}
	if !hasMp3 && len(song.Files) > 0 {
		plan := plans.KnownPlans[reencodePlan]
		c.tc.CreateJob(plan, song.Id.String())
	}
	return false
}

func (c *songChecker) CheckSong(song *api.Song, session services.Session) {
	if song.Info.State != "active" {
		// Reprocess non-active songs, in case something
		// happened.
		return
	}

	update := false
	update = update || c.checkStatus(song)
	if song.Info.State == "active" {
		update = update || c.checkSongMeta(song)
		update = update || c.checkMp3(song)
	}

	if update {
		log.Printf("updating song %s", song.Id)
		if err := c.db.PutSong(session, song); err != nil {
			log.Printf("ERROR: %s", err)
		}
	}
}

type MRVerify struct {
	mapreduce.MapReduce
}

func sliceToMap(s []api.SongID) map[api.SongID]struct{} {
	m := make(map[api.SongID]struct{})
	for _, id := range s {
		m[id] = struct{}{}
	}
	return m
}

func (mr *MRVerify) addSong(id api.SongID, doc api.Document) {
	if err := mr.Index.AddDoc(id, doc); err != nil {
		log.Printf("Error: indexing %s: %s", id, err)
	}
}

func (mr *MRVerify) deleteSong(id api.SongID) {
	mr.Index.DeleteDoc(id)
}

type SongData struct {
	Id  api.SongID
	Doc api.Document
}

// Map simply filters out 'active' songs.
func (mr *MRVerify) Map(key string, value string, emitter dmrgo.Emitter) {
	songId, err := api.ParseSongID(value)
	if err != nil {
		log.Println(err)
		return
	}

	song, ok := mr.Db.GetSong(mr.Session, songId)
	if !ok {
		log.Printf("song %s does not exist", value)
		return
	}

	// Run song checks (which might modify metadata) before
	// generating the indexed Doc.
	checker := newSongChecker(mr.Db, mr.Task)
	checker.CheckSong(song, mr.Session)

	if song.Info.State != "active" {
		return
	}

	// Emit everything under a single key so we're running a
	// single Reduce() call.
	out := SongData{
		Id:  song.Id,
		Doc: song.ToDoc(),
	}
	mr.Emit(emitter, "songs", &out)
}

func (mr *MRVerify) MapFinal(emitter dmrgo.Emitter) {
}

// Reduce compares the contents of the index with the output of the
// mappers, and adds/removes what is necessary.
func (mr *MRVerify) Reduce(key string, values []string, emitter dmrgo.Emitter) {
	var input []SongData
	mr.Unmarshal(key, values, &key, &input)

	added := 0
	removed := 0

	// Read current index contents.
	idxContents := sliceToMap(mr.Index.Scan())

	// Compute the set difference.
	for _, songData := range input {
		_, ok := idxContents[songData.Id]
		if ok {
			delete(idxContents, songData.Id)
		}
		// If 'rebuild' is set, add the document anyway.
		if *rebuild || !ok {
			log.Printf("add: %s", songData.Id)
			mr.addSong(songData.Id, songData.Doc)
			added++
		}
	}
	for songId, _ := range idxContents {
		log.Printf("delete: %s", songId)
		mr.deleteSong(songId)
		removed++
	}

	log.Printf("added: %d, removed: %d", added, removed)
}

func main() {
	mapreduce.Main(&MRVerify{}, &mapreduce.Options{
		HasIndex:      true,
		HasTaskClient: true,
	})
}
