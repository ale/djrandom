package main

import (
	"flag"
	"fmt"
	"log"

	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/util/config"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
	bucket     = flag.String("bucket", "", "Bucket to dump")
)

func main() {
	flag.Parse()
	config.Parse(*configFile)

	if *bucket == "" {
		log.Fatal("Must specify --bucket!")
	}

	dbClient := db_client.NewDatabaseRpcClientFromConfig()
	session, err := dbClient.NewSession()
	if err != nil {
		log.Fatalf("Could not create db session: %s", err)
	}

	iter, err := session.ScanKeys(*bucket, "", "\xff", -1)
	if err != nil {
		log.Fatalf("Scan error: %s", err)
	}

	for {
		key, err := iter.Next(nil)
		if err != nil {
			if err != db_client.EOF {
				log.Fatalf("Error: %s", err)
			}
			break
		}
		fmt.Printf("%s\n", key)
	}
}
