// MR job that builds the metadata index for the DAV filesystem view.

package main

import (
	"log"

	"github.com/dgryski/dmrgo"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/mapreduce"
)

const (
	davRootKey = "@@ROOT@@"
)

type MRDavFs struct {
	mapreduce.MapReduce
}

// Map emits data for each hierarchical level of the resulting filesystem.
func (mr *MRDavFs) Map(key string, value string, emitter dmrgo.Emitter) {
	songId, err := api.ParseSongID(value)
	if err != nil {
		log.Println(err)
		return
	}

	song, ok := mr.Db.GetSong(mr.Session, songId)
	if !ok {
		log.Printf("song %s does not exist", songId)
		return
	}

	if song.Meta.Artist != "" {
		if song.Meta.Album != "" {
			mr.Emit(emitter, song.Meta.Artist, song.Meta.Album)
		}
		mr.Emit(emitter, davRootKey, song.Meta.Artist)
	}
}

func (mr *MRDavFs) MapFinal(emitter dmrgo.Emitter) {
}

func uniquify(in []string) []string {
	h := make(map[string]struct{})
	for _, s := range in {
		h[s] = struct{}{}
	}
	i := 0
	for s, _ := range h {
		in[i] = s
		i++
	}
	return in[:i]
}

// Reduce collects statistics into a counter
func (mr *MRDavFs) Reduce(key string, values []string, emitter dmrgo.Emitter) {
	var svalues []string
	mr.Unmarshal(key, values, &key, &svalues)

	svalues = uniquify(svalues)
	log.Printf("%s: %d values", key, len(svalues))
	mr.Db.Put(mr.Session, "davfs", key, svalues)
}

func main() {
	mapreduce.Main(&MRDavFs{}, nil)
}
