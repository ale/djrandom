package main

import (
	"log"
	"sort"

	"github.com/dgryski/dmrgo"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/mapreduce"
)

type MRMarkov struct {
	mapreduce.MapReduce
}

// Map emits (user, playlog entry) tuples.
func (mr *MRMarkov) Map(key string, value string, emitter dmrgo.Emitter) {
	plKey := value
	//log.Printf("PlayLogEntry(%s)", plKey)
	pl, ok := mr.Db.GetPlayLog(mr.Session, plKey)
	if !ok {
		log.Printf("playlog entry %s does not exist", plKey)
		return
	}

	// Emit the transition for the global map.
	mr.Emit(emitter, api.MarkovGlobalKey, pl)

	// Emit the transition for the user's map (unless the playlog
	// entry has a nil user, in case it comes from an old data import).
	if pl.User != "" {
		mr.Emit(emitter, pl.User, pl)
	}
}

func (mr *MRMarkov) MapFinal(emitter dmrgo.Emitter) {
}

type PlayLogList []*api.PlayLogEntry

func (pl PlayLogList) Len() int      { return len(pl) }
func (pl PlayLogList) Swap(i, j int) { pl[i], pl[j] = pl[j], pl[i] }
func (pl PlayLogList) Less(i, j int) bool {
	return pl[i].Timestamp < pl[j].Timestamp
}

// Reduce generates per-user Markov maps.
func (mr *MRMarkov) Reduce(key string, values []string, emitter dmrgo.Emitter) {
	var plvalues []*api.PlayLogEntry
	var user string
	mr.Unmarshal(key, values, &user, &plvalues)

	sort.Sort(PlayLogList(plvalues))

	tc := make(chan api.Transition)
	go func() {
		for _, entry := range plvalues {
			var src, dst api.SongID
			n := len(entry.Songs)
			if n > 1 {
				src = entry.Songs[n-2]
				dst = entry.Songs[n-1]
			} else if n == 1 {
				dst = entry.Songs[0]
			} else {
				continue
			}
			// No empty destinations!
			if dst == api.NilSongID {
				continue
			}
			tc <- api.Transition{Src: src, Dst: dst}
		}
		close(tc)
	}()

	m := api.NewMarkovMap()
	m.Update(tc)
	log.Printf("Markov(%s): %d transitions, size=%d", user, len(plvalues), m.Len())

	// Store the new map in the db.
	if err := mr.Db.PutMarkovMap(mr.Session, user, m); err != nil {
		log.Printf("Could not store map: %s", err)
	}

	// Emit it anyway for now.
	mr.Emit(emitter, user, m)
}

func main() {
	mapreduce.Main(&MRMarkov{}, nil)
}
