// Compute metadata statistics for the song database.
//
package main

import (
	"fmt"
	"log"
	"strings"

	"github.com/dgryski/dmrgo"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/mapreduce"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/util/instrumentation"
)

type MRStats struct {
	mapreduce.MapReduce
}

type keyType struct {
	Stat string
	Key  string
}

// Map emits statistics for a number of song attributes.
func (mr *MRStats) Map(key string, value string, emitter dmrgo.Emitter) {
	songId, err := api.ParseSongID(value)
	if err != nil {
		log.Println(err)
		return
	}

	song, ok := mr.Db.GetSong(mr.Session, songId)
	if !ok {
		log.Printf("song %s does not exist", songId)
		return
	}

	emitStat := func(k, v string) {
		mr.Emit(emitter, &keyType{Stat: k, Key: v}, 1)
	}
	emitStat("state", song.Info.State)

	if song.Info.State != "active" {
		return
	}

	// Song metadata.
	if song.Meta.Artist != "" {
		emitStat("artist", strings.ToLower(song.Meta.Artist))
	}
	if song.Meta.Genre != "" {
		emitStat("genre", strings.ToLower(song.Meta.Genre))
	}
	if song.Meta.Year > 0 {
		emitStat("year", fmt.Sprintf("%d", song.Meta.Year))
	}
	// Audio file metadata.
	for _, af := range song.Files {
		emitStat("mime_type", af.MimeType)
		if af.BitRate > 0 {
			emitStat("bitrate", fmt.Sprintf("%d", af.BitRate))
		}
	}
}

func (mr *MRStats) MapFinal(emitter dmrgo.Emitter) {
}

// Reduce collects statistics into a counter
func (mr *MRStats) Reduce(key string, values []string, emitter dmrgo.Emitter) {
	var k keyType
	var ivalues []int
	mr.Unmarshal(key, values, &k, &ivalues)

	c := 0
	for _, count := range ivalues {
		c += count
	}

	dbkey := fmt.Sprintf("%s/%s", k.Stat, k.Key)
	mr.Db.Put(mr.Session, db_client.StatsBucket, dbkey, c)

	// Send some metrics to the stats collector.
	if k.Stat == "state" || k.Stat == "mime_type" {
		if k.Stat == "mime_type" {
			k.Key = strings.Replace(k.Key, "/", "_", -1)
		}
		instrumentation.NewGauge(fmt.Sprintf("db.%s.%s", k.Stat, k.Key), 1.0).Set(int64(c))
	}

	// This will be used later to remove old entries with a
	// two-way diff.
	mr.Emit(emitter, &k, c)
}

func main() {
	mapreduce.Main(&MRStats{}, nil)
}
