// Cleanup completed jobs.
//
package main

import (
	"flag"
	"log"
	"time"

	"github.com/dgryski/dmrgo"

	"git.autistici.org/ale/djrandom/api/task_api"
	"git.autistici.org/ale/djrandom/mapreduce"
)

var (
	cleanAllTasks = flag.Bool("all", false, "Clear all tasks (dangerous)")
	maxJobAge     = flag.Int64("max-age", 86400, "Task age cutoff")
)

type MRCleanTasks struct {
	mapreduce.MapReduce
	now int64
}

func (mr *MRCleanTasks) Map(key string, value string, emitter dmrgo.Emitter) {
	jobId := value

	// Fetch the job and delete it if it's too old.
	if !*cleanAllTasks {
		var job task_api.Job
		if err := mr.Db.Get(mr.Session, "tasks", jobId, &job); err != nil {
			return
		}
		if !job.Final || (mr.now-job.CompletedAt) < *maxJobAge {
			return
		}
	}

	mr.Db.Del(mr.Session, "tasks", jobId)
	log.Printf("removed job %s", jobId)
}

func (mr *MRCleanTasks) MapFinal(emitter dmrgo.Emitter) {
}

func (mr *MRCleanTasks) Reduce(key string, values []string, emitter dmrgo.Emitter) {
}

func main() {
	mapreduce.Main(&MRCleanTasks{now: time.Now().Unix()}, nil)
}
