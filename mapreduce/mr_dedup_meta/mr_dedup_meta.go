package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"runtime/pprof"
	"strings"
	"time"

	"github.com/dgryski/dmrgo"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/mapreduce"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/util/tokenizer"
)

var (
	memProfile = flag.String("memprofile", "", "save memory profile to file")
	apply      = flag.Bool("apply", false, "Really apply deduplication changes")
)

type MRDedupMeta struct {
	mapreduce.MapReduce
}

// Deduplication is performed on "normalized" artist/title pairs,
// which are generated using the same tokenization algorithms used by
// the indexer (which removes non-alphanumerics and diacritics).
var dedupTokenizer = tokenizer.NewTokenizer(
	tokenizer.WhitespaceTokenizer,
	// tokenizer.MinLengthFilter(2),
	tokenizer.LowercaseFilter,
	tokenizer.RemoveDiacriticsFilter)

type keyType struct {
	Artist string
	Title  string
}

func normalize(s string) string {
	return strings.Join(dedupTokenizer.Tokenize(s), ".")
}

// Map emits (artist/title, song_id) tuples.
func (mr *MRDedupMeta) Map(key string, value string, emitter dmrgo.Emitter) {
	songId, err := api.ParseSongID(value)
	if err != nil {
		log.Println(err)
		return
	}

	song, ok := mr.Db.GetSong(mr.Session, songId)
	if !ok {
		log.Printf("song %s does not exist", songId)
		return
	}

	// Filter only active songs.
	if song.Info.State != "active" {
		return
	}

	k := keyType{
		Artist: normalize(song.Meta.Artist),
		Title:  normalize(song.Meta.Title),
	}
	mr.Emit(emitter, &k, songId)
}

func (mr *MRDedupMeta) MapFinal(emitter dmrgo.Emitter) {
}

// Reduce de-duplicates identical songs.
func (mr *MRDedupMeta) Reduce(key string, values []string, emitter dmrgo.Emitter) {
	var songIds []string
	var k keyType
	mr.Unmarshal(key, values, &k, &songIds)

	// Check that we have at least two elements to deduplicate.
	if len(songIds) < 2 {
		return
	}

	mr.deduplicate(songIds)
}

func (mr *MRDedupMeta) deduplicate(songIds []string) {
	songs, _ := db_client.ParallelFetchSongs(mr.Db, api.ParseSongIDSlice(songIds))

	// Second check for number of elements, to deal with errors.
	if len(songs) < 2 {
		return
	}

	// Print what we're about to do.
	ref := pickBestSong(songs)

	fmt.Printf("deduplicating %v -> %s\n", ref.Meta, ref.Id)
	for _, song := range songs {
		fmt.Printf("    [*]  %v (%s)\n", song.Meta, song.Id)
		for _, af := range song.Files {
			fmt.Printf("     +   %v\n", af)
		}
	}

	if !*apply {
		return
	}

	for _, song := range songs {
		if song == ref {
			continue
		}
		song.Info.State = "duplicate"
		song.Info.DuplicateOf = ref.Id
		if err := mr.Db.PutSong(mr.Session, song); err != nil {
			log.Printf("error saving song %s: %s", song.Id, err)
		}
	}
}

func pickBestSong(songs []*api.Song) *api.Song {
	// Picking the 'best' song involves finding the 'best' audio
	// file. We use a map to match it to the song later.
	afToSong := make(map[string]*api.Song)
	files := make([]*api.AudioFile, 0)
	for _, s := range songs {
		for _, af := range s.Files {
			files = append(files, af)
			afToSong[af.MD5] = s
		}
	}

	af := api.GetBestAudioFile(files)
	song := afToSong[af.MD5]

	return song
}

func memProfileDumper() {
	c := time.Tick(60 * time.Second)
	for t := range c {
		profFile := fmt.Sprintf("%s.%d", *memProfile, t.Unix())
		f, err := os.Create(profFile)
		if err != nil {
			log.Fatal(err)
		}
		pprof.WriteHeapProfile(f)
		f.Close()
	}
}

func main() {
	mapreduce.Main(&MRDedupMeta{}, nil)
}
