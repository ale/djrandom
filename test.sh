#!/bin/bash

root=./test-dir-$$
test -d ${root} || mkdir ${root}

bindir=${GOPATH}/bin
music_dir="$1"

if [ ! -d "${music_dir}" ]; then
  echo "Usage: $0 <MUSIC_DIR>" 1>&2
  exit 1
fi

DAEMONS="
	db_server
	index_server
	storage_server
	task_server
	djproc
	djfe
	"

generate_server_conf() {
  cat >$1 <<EOF
{
  "db_path": "${root}/djrandom.db",
  "db_addr": "127.0.0.1:3007",
  "storage_root": "${root}/fs",
  "storage_pmap": "${root}/part.json",
  "storage_self": "http://127.0.0.1:3003",
  "storage_key_id": "abcdef",
  "storage_key_secret": "123456",
  "index_pmap": "${root}/part-index.json",
  "index_self": "127.0.0.1:3002",
  "index_file": "${root}/djindex",
  "task_server_addr": "http://127.0.0.1:3005",
  "last_fm_api_key": "1234"
}
EOF
}

generate_client_conf() {
  cat >$1 <<EOF
{
  "music_dir": "${music_dir}",
  "db_path": "${root}/djclient.db",
  "server": "http://127.0.0.1:3001",
  "auth_key": "${auth_key}",
  "auth_secret": "${auth_secret}"
}
EOF
}

generate_storage_part() {
  ${bindir}/parttool --parts=16 --file=$1 create http://127.0.0.1:3003
}

generate_index_part() {
  ${bindir}/parttool --parts=16 --file=$1 create 127.0.0.1:3002
}

start_daemons() {
  for d in $DAEMONS ; do
    local pidfile=${root}/${d}.pid
    local logfile=${root}/${d}.log
    ${bindir}/${d} --config=${root}/djrandom.conf --pid-file=${pidfile} \
      --log-file=${logfile} &
  done
}

stop_daemons() {
  for d in $DAEMONS ; do
    local pidfile=${root}/${d}.pid
    if [ -e ${pidfile} ]; then
      local pid=$(cat ${pidfile})
      kill -TERM ${pid}
      rm -f ${pidfile}
    fi
  done
}

setup_users() {
  ${bindir}/usermgmt --config=${root}/djrandom.conf \
      add-user test@example.com password
  eval $(${bindir}/usermgmt --config=${root}/djrandom.conf \
      gen-auth-key test@example.com)
}

setup() {
  generate_storage_part ${root}/part.json
  generate_index_part ${root}/part-index.json
  generate_server_conf ${root}/djrandom.conf
}

run_client() {
  generate_client_conf ${root}/client.conf
  ${bindir}/djuploader --config=${root}/client.conf --once
}

run_mr_stats() {
  ${bindir}/mr_dump_bucket --config=${root}/djrandom.conf --bucket=song \
  | ${bindir}/mr_stats --config=${root}/djrandom.conf --mapreduce
}

run_mr_davfs() {
  ${bindir}/mr_dump_bucket --config=${root}/djrandom.conf --bucket=song \
  | ${bindir}/mr_davfs --config=${root}/djrandom.conf --mapreduce
}

setup
start_daemons
sleep 1
setup_users

echo "Done."
echo ${root}

sleep 1
run_client
run_mr_stats
run_mr_davfs

echo "*** TEST COMPLETE ***"
echo
echo "Press <Enter> to terminate all daemons"
read ans

stop_daemons
