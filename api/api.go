// Objects used in the public HTTP API.

package api

type FingerprintRequest struct {
	Fingerprints []string `json:"fingerprints"`
}

type FingerprintResponse struct {
	Missing []string `json:"missing"`
	Dupes   []string `json:"dupes"`
}

type UploadResponse struct {
	Ok bool `json:"ok"`
}

type SearchIdsResponse struct {
	Results []SongID `json:"results"`
}

type SearchResponse struct {
	Results []*Song `json:"results"`
}

type GetManySongsRequest struct {
	SongIds []SongID `json:"song_ids"`
}

type GetManySongsResponse struct {
	Results []*Song `json:"results"`
}

// Add an entry to the playlog. Songs are in increasing time order
// (most recently played last).
type AddPlayLogRequest struct {
	Songs []SongID `json:"songs"`
}

type SuggestRequest struct {
	CurrentSongs []SongID `json:"current_songs"`
	NumResults   int      `json:"num_results"`
	// SkipGlobal   bool     `json:"skip_global"`
}

type SuggestResponse struct {
	Results    []*Song  `json:"results"`
	SongOrigin []string `json:"song_origin"`
}

// JsonFs data structures.
type JsonFsAttr struct {
	Name  string     `json:"name"`
	IsDir bool       `json:"is_dir"`
	Audio *AudioFile `json:"audio"`
	Meta  *Meta      `json:"meta"`
}

type JsonFsLsResponse struct {
	Results []*JsonFsAttr
}

type AcousticCorrelation struct {
	SongId string
	Value  float32
}
