package api

import (
	"encoding/json"
	"testing"
)

var (
	testSongId1 = [16]byte{0x58, 0xee, 0x09, 0x93, 0x7d, 0x51, 0xf3, 0x88, 0x6d, 0xcd, 0x0c, 0x0e, 0xa9, 0x1c, 0x05, 0x3e}
	testSongId2 = [16]byte{0x58, 0xee, 0x09, 0x93, 0x7d, 0x51, 0xf3, 0x88, 0x6d, 0xcd, 0x0c, 0x0e, 0xa9, 0x1d, 0x06, 0x3f}
)

func TestFreqDist_Simple(t *testing.T) {
	f := NewFreqDist()
	f.Incr(testSongId1)
	result := f.GetSong()
	if result != testSongId1 {
		t.Fatalf("Bad result: %v", result)
	}
}

func newTestMarkovMap() *MarkovMap {
	c := make(chan Transition)
	go func() {
		c <- Transition{testSongId1, testSongId2}
		close(c)
	}()
	m := NewMarkovMap()
	m.Update(c)
	return m
}

func TestMarkovMap_Simple(t *testing.T) {
	m := newTestMarkovMap()
	result, ok := m.GetSong(testSongId1)
	if !ok {
		t.Fatal("No result for 'a'")
	}
	if result != testSongId2 {
		t.Fatalf("Bad GetSong() result: %v", result)
	}

	result = m.GetRandomSong()
	if result != testSongId1 {
		t.Fatalf("Bad GetRandomSong() result: %v", result)
	}
}

func TestMarkovMap_Json(t *testing.T) {
	m := newTestMarkovMap()
	out, err := json.Marshal(m)
	if err != nil {
		t.Fatal("Marshal: ", err)
	}

	var m2 MarkovMap
	if err := json.Unmarshal(out, &m2); err != nil {
		t.Fatal("Unmarshal: ", err)
	}

	if m2.Len() != m.Len() {
		t.Fatalf("m != m2: %v, %v", m, m2)
	}
}
