package api

import (
	"testing"
)

func compareSlices(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, _ := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func TestSongToDoc_Artist(t *testing.T) {
	af := &AudioFile{
		MD5:      "58ee09937d51f3886dcd0c0ea91c053e",
		MimeType: "audio/mpeg",
		Size:     1234,
		//SongId:   "12345",
	}
	meta := &Meta{
		Artist: "Victor Hugo",
		Year:   1862,
	}
	song := NewSong(af, meta)
	doc := song.ToDoc()

	artist := doc["artist"]
	expected := []string{"victor", "hugo", "=Victor Hugo"}
	if !compareSlices(artist, expected) {
		t.Errorf("Artist: expected=%v, result=%v", expected, artist)
	}

	year := doc["year"]
	expected = []string{"1862"}
	if !compareSlices(year, expected) {
		t.Errorf("Year: expected=%v, result=%v", expected, year)
	}

	meta.Artist = "Fabrizio de André"
	song = NewSong(af, meta)
	doc = song.ToDoc()
	artist = doc["artist"]
	expected = []string{"fabrizio", "de", "andre", "=Fabrizio de André"}
	if !compareSlices(artist, expected) {
		t.Errorf("Artist: expected=%v, result=%v", expected, artist)
	}
}
