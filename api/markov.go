package api

import (
	"encoding/json"
	"math/rand"
)

const MarkovGlobalKey = "@GLOBAL"

var NilSongID SongID

// A transition from a song to another.
type Transition struct {
	Src SongID
	Dst SongID
}

// FreqDist contains a probability distribution for a specific N-gram
// in the Markov model.  We store counters instead of probabilities so
// that we can update the map incrementally.
type FreqDist struct {
	D   map[SongID]int
	Sum int64
}

func NewFreqDist() *FreqDist {
	return &FreqDist{
		D:   make(map[SongID]int),
		Sum: 0,
	}
}

func (fd *FreqDist) Incr(song SongID) {
	counter, ok := fd.D[song]
	if ok {
		counter++
	} else {
		fd.D[song] = 1
	}
	fd.Sum++
}

func (fd *FreqDist) GetSong() SongID {
	t := rand.Int63n(fd.Sum)
	var cur int64 = 0
	for song, count := range fd.D {
		cur += int64(count)
		if cur > t {
			return song
		}
	}

	// This should be unreachable.
	return NilSongID
}

type freqDistJson struct {
	D   map[string]int
	Sum int64
}

func (fd *FreqDist) MarshalJSON() ([]byte, error) {
	tmp := freqDistJson{
		D:   make(map[string]int),
		Sum: fd.Sum,
	}
	for id, d := range fd.D {
		tmp.D[id.String()] = d
	}
	return json.Marshal(&tmp)
}

func (fd *FreqDist) UnmarshalJSON(data []byte) error {
	var tmp freqDistJson
	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}
	fd.Sum = tmp.Sum
	fd.D = make(map[SongID]int)
	for idstr, d := range tmp.D {
		if id, err := ParseSongID(idstr); err == nil {
			fd.D[id] = d
		}
	}
	return nil
}

// MarkovMap contains probability distributions for playlists.
// The input tuple size is 1, and it's hard-coded in the interface.
type MarkovMap struct {
	M    map[SongID]*FreqDist
	keys []SongID
}

func NewMarkovMap() *MarkovMap {
	return &MarkovMap{
		M: make(map[SongID]*FreqDist),
	}
}

// GetSong returns a song following 'cur' according to the model.
func (m *MarkovMap) GetSong(cur SongID) (SongID, bool) {
	dist, ok := m.M[cur]
	if !ok {
		return NilSongID, false
	}
	return dist.GetSong(), true
}

// GetRandomSong returns a random song chosen amongst those that
// appear as seeds in the map (i.e. that have been listened to at
// least once, and have at least one transition).
func (m *MarkovMap) GetRandomSong() SongID {
	if m.keys == nil {
		m.updateKeys()
	}
	return m.keys[rand.Intn(len(m.keys))]
}

func (m *MarkovMap) Update(in chan Transition) {
	for t := range in {
		dist, ok := m.M[t.Src]
		if !ok {
			dist = NewFreqDist()
			m.M[t.Src] = dist
		}
		dist.Incr(t.Dst)
	}
	m.keys = nil
}

func (m *MarkovMap) updateKeys() {
	var keys []SongID
	for k := range m.M {
		// Exclude the 'nil' song ID, which serves a special
		// purpose as a map key.
		if k != NilSongID {
			keys = append(keys, k)
		}
	}
	m.keys = keys
}

// Len returns the total number of transitions encoded in the map
// (useful for debugging purposes).
func (m *MarkovMap) Len() int {
	var l int
	for _, dist := range m.M {
		l += len(dist.D)
	}
	return l
}

// MarshalJSON serializes the map to JSON (using a quite inefficient
// mechanism at the moment).
func (m *MarkovMap) MarshalJSON() ([]byte, error) {
	tmp := make(map[string]*FreqDist)
	for id, f := range m.M {
		tmp[id.String()] = f
	}
	return json.Marshal(tmp)
}

// UnmarshalJSON restores a MarkovMap from a JSON serialization.
func (m *MarkovMap) UnmarshalJSON(data []byte) error {
	var tmp map[string]*FreqDist
	if err := json.Unmarshal(data, &tmp); err != nil {
		return err
	}
	m.M = make(map[SongID]*FreqDist)
	for idstr, f := range tmp {
		if id, err := ParseSongID(idstr); err == nil {
			m.M[id] = f
		}
	}
	return nil
}
