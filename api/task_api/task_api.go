package task_api

import (
	"crypto/rand"
	"encoding/hex"
	"io"
	"time"
)

type StateMachineOutput struct {
	Ok  string
	Err string
}

type Action interface {
	Handle(arg string, log io.Writer) error
}

// A Plan is a declarative definition of a finite state machine,
// expressed in terms of the possible state transitions. There
// is only one final state and it's represented by the empty string.
type Plan struct {
	Name         string
	InitialState string
	M            map[string]StateMachineOutput
}

// NewPlan creates a new task plan.
func NewPlan(name string, initialState string, planMap map[string][]string) *Plan {
	m := make(map[string]StateMachineOutput)
	for k, v := range planMap {
		m[k] = StateMachineOutput{v[0], v[1]}
	}
	return &Plan{name, initialState, m}
}

type JobLogEntry struct {
	Stamp int64
	Msg   string
}

// A Job holds the runtime state of a specific job.
type Job struct {
	Id          string
	PlanName    string
	Arg         string
	State       string
	Final       bool
	Error       bool
	StartedAt   int64
	CompletedAt int64
	Logs        []JobLogEntry
}

// Return a random 128-bit hex-encoded ID.
func makeRandomJobId() string {
	buf := make([]byte, 16)
	rand.Read(buf)
	return hex.EncodeToString(buf)
}

// NewJob returns a new job associated with a specific Plan.
func NewJob(plan *Plan, arg string) *Job {
	return &Job{
		Id:        makeRandomJobId(),
		PlanName:  plan.Name,
		Arg:       arg,
		State:     plan.InitialState,
		Final:     false,
		Error:     false,
		StartedAt: time.Now().Unix(),
	}
}

func (j *Job) Log(msg string) {
	j.Logs = append(j.Logs, JobLogEntry{
		Stamp: time.Now().Unix(),
		Msg:   msg,
	})
}
