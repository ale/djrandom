package api

// Set is a simple unordered collection of songs.
type SongSet map[SongID]struct{}

// Add a value to the set.
func (s SongSet) Add(id SongID) {
	s[id] = struct{}{}
}

// Discard a value from the set.
func (s SongSet) Discard(id SongID) {
	if _, ok := s[id]; ok {
		delete(s, id)
	}
}

// Intersect returns a new Set that is the intersection of s and the
// specified argument.
func (s SongSet) Intersect(other SongSet) SongSet {
	var a, b SongSet
	if len(s) < len(other) {
		a = s
		b = other
	} else {
		a = other
		b = s
	}

	out := make(SongSet)
	for id, _ := range a {
		if _, ok := b[id]; ok {
			out[id] = struct{}{}
		}
	}
	return out
}

// Update merges a Set into this one.
func (s SongSet) Update(other SongSet) {
	for id, _ := range other {
		s.Add(id)
	}
}
