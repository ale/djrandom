package fsapi

import (
	"errors"

	"git.autistici.org/ale/djrandom/api"
)

var (
	ErrNotFound = errors.New("not found")
	ErrNotDir   = errors.New("not a directory")
)

type Attr struct {
	Name  string
	IsDir bool
	Song  *api.Song
	Audio *api.AudioFile
}

type Filesystem interface {
	Stat(path string) (Attr, error)
	Ls(path string) ([]Attr, error)
}
