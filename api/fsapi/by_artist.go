package fsapi

import (
	"fmt"
	"path/filepath"
	"strings"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
)

const (
	numParallelQueries = 10
	authorAlbumRootKey = "@@ROOT@@"
)

// ByArtistFilesystem is a Filesystem implementation that exposes an
// artist/album/song hierarchy (and tries to pick the best-sounding
// file for each song).
type ByArtistFilesystem struct {
	DbPrefix string
	db       services.Database
	index    services.Index
}

func NewByArtistFilesystem(db services.Database, index services.Index) *ByArtistFilesystem {
	return &ByArtistFilesystem{
		DbPrefix: "davfs",
		db:       db,
		index:    index,
	}
}

func splitPath(path string) []string {
	path = strings.Trim(path, "/")
	if path == "" {
		return []string{}
	}
	return strings.Split(path, "/")
}

func (afs *ByArtistFilesystem) Stat(path string) (Attr, error) {
	p := splitPath(path)

	// The only case where the result is a file is for
	// a fully-specified three-level path.
	if len(p) == 3 {
		return afs.getSong(p[0], p[1], p[2])
	} else if len(p) < 3 {
		name := "/"
		if len(p) > 0 {
			name = p[len(p)-1]
		}
		attr := Attr{
			Name:  name,
			IsDir: true,
		}
		return attr, nil
	} else {
		return Attr{}, ErrNotFound
	}
}

func (afs *ByArtistFilesystem) Ls(path string) ([]Attr, error) {
	p := splitPath(path)

	var attrs []Attr
	var err error
	switch len(p) {
	case 0:
		attrs, err = afs.getAuthors()
	case 1:
		attrs, err = afs.getAlbumsForAuthor(p[0])
	case 2:
		attrs, err = afs.getSongsForAlbum(p[0], p[1])
	default:
		err = ErrNotDir
	}
	return attrs, err
}

func (afs *ByArtistFilesystem) readDb(key string) ([]string, error) {
	var result []string
	if err := afs.db.Get(nil, afs.DbPrefix, key, &result); err != nil {
		return nil, err
	}
	return result, nil
}

func (afs *ByArtistFilesystem) getAuthors() ([]Attr, error) {
	authors, err := afs.readDb(authorAlbumRootKey)
	if err != nil {
		return nil, err
	}
	return makeDirList(authors), nil
}

func (afs *ByArtistFilesystem) getAlbumsForAuthor(author string) ([]Attr, error) {
	albums, err := afs.readDb(author)
	if err != nil {
		return nil, err
	}
	return makeDirList(albums), nil
}

func (afs *ByArtistFilesystem) getSongsForAlbum(author, album string) ([]Attr, error) {
	// Build an exact match query.
	query := fmt.Sprintf("artist:\"=%s\" album:\"=%s\"", author, album)

	songIdMap := afs.index.Search(query)
	songIds := make([]api.SongID, 0, len(songIdMap))
	for songId, _ := range songIdMap {
		songIds = append(songIds, songId)
	}

	// Fetch objects from the database in parallel.
	songs, err := db_client.ParallelFetchSongs(afs.db, songIds)
	if err != nil {
		return nil, err
	}

	attrs := make([]Attr, 0, len(songs))
	for _, song := range songs {
		attr, err := afs.songToAttr(song, "")
		if err == nil {
			attrs = append(attrs, attr)
		}
	}
	return attrs, nil

}

func (afs *ByArtistFilesystem) getSong(author, album, title string) (Attr, error) {
	// Remove the extension from the song title (which appears
	// as the file name).
	ext := filepath.Ext(title)
	title = title[:len(title)-len(ext)]

	// This is a mess from the point of view of encodings, it
	// will only work some of the time...
	query := fmt.Sprintf("artist:\"=%s\" album:\"=%s\" title:\"=%s\"", author, album, title)

	// Search for an exact match. If more than one, just pick the first.
	results := afs.index.Search(query)
	if len(results) == 0 {
		return Attr{}, ErrNotFound
	}
	var songId api.SongID
	for id, _ := range results {
		songId = id
		break
	}

	// Retrieve the song from the database and pick an AudioFile.
	song, ok := afs.db.GetSong(nil, songId)
	if !ok {
		return Attr{}, ErrNotFound
	}

	return afs.songToAttr(song, api.GetMimeTypeFromExtension(ext))
}

// Creates an Attr from a Song, possibly forcing the MIME type (if provided
// by the caller somehow).
func (afs *ByArtistFilesystem) songToAttr(song *api.Song, mimetype string) (Attr, error) {
	var file *api.AudioFile
	if mimetype == "" {
		file = song.GetBestAudioFile()
	} else {
		file = song.GetBestAudioFileByType(mimetype)
	}
	if file == nil {
		return Attr{}, ErrNotFound
	}

	return Attr{
		Name:  song.Meta.Title,
		IsDir: false,
		Song:  song,
		Audio: file,
	}, nil
}

func makeDirList(elems []string) []Attr {
	attrs := make([]Attr, len(elems))
	for i, e := range elems {
		attrs[i] = Attr{
			Name:  e,
			IsDir: true,
		}
	}
	return attrs
}
