package api

import (
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"encoding/json"
	"io"
	"log"
	"time"

	"git.autistici.org/ale/djrandom/util/crypt"
)

// Meta stores metadata about the song.
type Meta struct {
	Artist   string `json:"artist"`
	Title    string `json:"title"`
	Album    string `json:"album"`
	Genre    string `json:"genre"`
	Year     int    `json:"year"`
	TrackNum int    `json:"track_num"`
}

// AudioFile describes a single audio file that has been uploaded.
type AudioFile struct {
	MD5           string  `json:"md5"`
	MimeType      string  `json:"mime_type"`
	Size          int64   `json:"size"`
	SampleRate    int     `json:"sample_rate"`
	BitRate       int     `json:"bitrate"`
	Channels      int     `json:"channels"`
	BitsPerSample int     `json:"bits_per_sample"`
	Duration      float64 `json:"duration"`
	SongId        SongID
}

// AcousticData stores acoustic analysis output (as a binary blob,
// which is definitely not optimal).
type AcousticData struct {
	Features []byte `json:"f"`
}

// SongID is the primary key type (in-memory). It consists of a MD5
// hash, related to the audio contents of the song. We use a binary
// in-memory representation to save space, but the type is serialized
// to a hex-encoded string in JSON encodings and in the public API in
// general.
type SongID [16]byte

func ParseSongID(s string) (SongID, error) {
	var id SongID
	data, err := hex.DecodeString(s)
	if err != nil {
		return id, err
	}
	copy(id[:], data)
	return id, nil
}

func ParseSongIDSlice(sl []string) []SongID {
	var out []SongID
	for _, s := range sl {
		if id, err := ParseSongID(s); err == nil {
			out = append(out, id)
		}
	}
	return out
}

func (s SongID) String() string {
	return hex.EncodeToString(s[:])
}

func (s SongID) MarshalJSON() ([]byte, error) {
	buf := make([]byte, hex.EncodedLen(len(s)))
	hex.Encode(buf, s[:])
	return json.Marshal(string(buf))
}

func (s *SongID) UnmarshalJSON(src []byte) error {
	var str string
	if err := json.Unmarshal(src, &str); err != nil {
		return err
	}
	data, err := hex.DecodeString(str)
	if err != nil {
		return err
	}
	copy(s[:], data)
	return nil
}

// SongInfo contains the internal song state.
type SongInfo struct {
	State       string `json:"state"`
	CreatedAt   int64  `json:"created_at"`
	DuplicateOf SongID `json:"duplicate_of"`
}

// Song has the comprehensive song state. Multiple AudioFiles can
// actually correspond to the same song (different rip, multiple
// versions, etc). This object is not stored directly in the db.
type Song struct {
	Id    SongID       `json:"id"`
	Files []*AudioFile `json:"files"`
	Meta  Meta         `json:"meta"`
	Info  SongInfo     `json:"info"`
}

// NewSong assembles a Song object from a single audio file. The song
// ID will be equal to the file's MD5 checksum. The song will be
// created in the 'incoming' state.
func NewSong(af *AudioFile, am *Meta) *Song {
	songId, _ := ParseSongID(af.MD5)
	af.SongId = songId
	return &Song{
		Id:    songId,
		Meta:  *am,
		Files: []*AudioFile{af},
		Info: SongInfo{
			State:     "incoming",
			CreatedAt: time.Now().Unix(),
		},
	}
}

// SongIntl is the on-database representation of the above structure,
// with the various fields wrapped.
type SongIntl struct {
	Id    SongID   `json:"id"`
	Files []string `json:"files"`
	Meta  Meta     `json:"meta"`
	Info  SongInfo `json:"info"`
}

// Fingerprint holds the binary audio fingerprint data
type Fingerprint struct {
	Code    string `json:"code"`
	Version string `json:"version"`
	Ok      bool   `json:"ok"`
	Stamp   int64  `json:"stamp"`
}

// AuthKey stores credentials for a single API key.
type AuthKey struct {
	KeyId  string `json:"key_id"`
	Secret string `json:"secret"`
	User   string `json:"user"`
}

// User holds user information.
type User struct {
	Email       string   `json:"email"`
	Password    string   `json:"password"`
	CreatedAt   int64    `json:"created_at"`
	InvitedBy   string   `json:"invited_by"`
	InvitesLeft int      `json:"invites_left"`
	State       string   `json:"state"`
	AuthKeyIds  []string `json:"auth_keys"`
}

func (u *User) CheckPassword(password string) bool {
	encPw := crypt.Crypt(password, u.Password)
	return (encPw == u.Password && encPw != "")
}

func (u *User) SetPassword(password string) {
	u.Password = crypt.Crypt(password, crypt.GenRandomSalt())
}

func (u *User) NewAuthKey() *AuthKey {

	// To reduce collisions, auth key ids are namespaced using the
	// unique user identifier (email), plus a few random bytes for
	// uniqueness.
	h := sha1.New()
	io.WriteString(h, u.Email)
	io.CopyN(h, rand.Reader, 4)

	// The secret is a purely random byte sequence.
	b := make([]byte, 16)
	n, err := io.ReadFull(rand.Reader, b)
	if n != len(b) || err != nil {
		log.Fatal("Error generating new auth key: %s", err)
	}

	key := &AuthKey{
		KeyId:  hex.EncodeToString(h.Sum(nil)),
		Secret: hex.EncodeToString(b),
		User:   u.Email,
	}
	u.AuthKeyIds = append(u.AuthKeyIds, key.KeyId)
	return key
}

// PlayLogEntry records a single 'play' action by a user.
type PlayLogEntry struct {
	// Username.
	User string

	// History of played songs, from oldest to most recent.
	Songs []SongID

	// Timestamp of this log entry.
	Timestamp int64
}
