package api

import (
	"strconv"

	"git.autistici.org/ale/djrandom/util/tokenizer"
)

// MultiValuedMap is a map of attributes to multiple values.
type MultiValuedMap map[string][]string

// Add an attribute/value pair to the document.
func (m MultiValuedMap) Add(key, value string) {
	m[key] = append(m[key], value)
}

// Document is an entry in the index.
type Document MultiValuedMap

func (d Document) Add(key, value string) {
	MultiValuedMap(d).Add(key, value)
}

// ToDoc extracts the indexable tokens from a Song object.
func (song *Song) ToDoc() Document {
	doc := make(Document)

	var yearStr string
	if song.Meta.Year > 0 {
		yearStr = strconv.Itoa(song.Meta.Year)
	}
	extractionTable := []struct {
		key, value string
		addLiteral bool
	}{
		{"title", song.Meta.Title, true},
		{"artist", song.Meta.Artist, true},
		{"album", song.Meta.Album, true},
		{"genre", song.Meta.Genre, false},
		{"year", yearStr, false},
	}

	for _, ext := range extractionTable {
		if ext.value == "" {
			continue
		}
		for _, t := range tokenizer.Tokenize(ext.value) {
			doc.Add(ext.key, t)
		}
		if ext.addLiteral {
			doc.Add(ext.key, "="+ext.value)
		}
	}

	// Add (unique) mime types for each audio file.
	mimeTypesMap := make(map[string]struct{})
	for _, af := range song.Files {
		mimeTypesMap[af.MimeType] = struct{}{}
	}
	for mimeType, _ := range mimeTypesMap {
		doc.Add("mime_type", mimeType)
	}

	return doc
}
