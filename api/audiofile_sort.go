package api

import (
	"sort"
)


type afLessFunc func(a1, a2 *AudioFile) bool

type afMultiSorter struct {
	files []*AudioFile
	less []afLessFunc
}

func (ms *afMultiSorter) Sort(files []*AudioFile) {
	ms.files = files
	sort.Sort(ms)
}

func orderedBy(less ...afLessFunc) *afMultiSorter {
	return &afMultiSorter{
		less: less,
	}
}

func (ms *afMultiSorter) Len() int {
	return len(ms.files)
}

func (ms *afMultiSorter) Swap(i, j int) {
	ms.files[i], ms.files[j] = ms.files[j], ms.files[i]
}

func (ms *afMultiSorter) Less(i, j int) bool {
	p, q := ms.files[i], ms.files[j]
	var k int
	for k = 0; k < len(ms.less)-1; k++ {
		less := ms.less[k]
		switch {
		case less(p, q):
			return true
		case less(q, p):
			return false
		}
	}
	return ms.less[k](p, q)
}

func GetBestAudioFile(files []*AudioFile) *AudioFile {
	if files == nil || len(files) == 0 {
		return nil
	}
	if len(files) == 1 {
		return files[0]
	}

	mimeTypeOrder := map[string]int{
		MIMETYPE_FLAC: 2,
		MIMETYPE_MP3: 1,
	}

	mimeType := func(a1, a2 *AudioFile) bool {
		// Reversed.
		return mimeTypeOrder[a1.MimeType] > mimeTypeOrder[a2.MimeType]
	}

	sampleRate := func(a1, a2 *AudioFile) bool {
		// Reversed.
		return a1.SampleRate > a2.SampleRate
	}

	bitRate := func(a1, a2 *AudioFile) bool {
		// Reversed.
		return a1.BitRate > a2.BitRate
	}

	channels := func(a1, a2 *AudioFile) bool {
		// Reversed.
		return a1.Channels > a2.Channels
	}

	orderedBy(mimeType, sampleRate, bitRate, channels).Sort(files)
	return files[0]
}

// GetBestAudioFile returns the 'best' audio file for this song.
func (s *Song) GetBestAudioFile() *AudioFile {
	files := make([]*AudioFile, len(s.Files))
	copy(files, s.Files)
	return GetBestAudioFile(files)
}

// GetBestAudioFileByType returns the 'best' file with the given MIME type.
func (s *Song) GetBestAudioFileByType(mimetype string) *AudioFile {
	files := make([]*AudioFile, 0, len(s.Files))
	for _, f := range s.Files {
		if f.MimeType == mimetype {
			files = append(files, f)
		}
	}
	return GetBestAudioFile(files)
}
