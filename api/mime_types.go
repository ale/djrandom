package api

import (
	"strings"
)

const (
	MIMETYPE_FLAC = "audio/x-flac"
	MIMETYPE_MP3  = "audio/mpeg"
)

var (
	mimeTypesMap = map[string]string{
		".flac": MIMETYPE_FLAC,
		".mp3":  MIMETYPE_MP3,
		".mpa":  MIMETYPE_MP3,
	}
)

func GetExtensionForMimeType(mimeType string) string {
	switch mimeType {
	case MIMETYPE_MP3:
		return ".mp3"
	case MIMETYPE_FLAC:
		return ".flac"
	}
	return ""
}

func GetFfmpegFormatForMimeType(mimeType string) string {
	switch mimeType {
	case MIMETYPE_MP3:
		return "mp3"
	case MIMETYPE_FLAC:
		return "flac"
	}
	return ""
}

func GetMimeTypeFromExtension(ext string) string {
	return mimeTypesMap[strings.ToLower(ext)]
}
