package api

import (
	"encoding/json"
	"testing"
)

var (
	testSongId     = [16]byte{0x58, 0xee, 0x09, 0x93, 0x7d, 0x51, 0xf3, 0x88, 0x6d, 0xcd, 0x0c, 0x0e, 0xa9, 0x1c, 0x05, 0x3e}
	testSongStr    = "58ee09937d51f3886dcd0c0ea91c053e"
	testSongIdJson = `"58ee09937d51f3886dcd0c0ea91c053e"`
)

func TestSongID_Parse(t *testing.T) {
	id, err := ParseSongID(testSongStr)
	if err != nil {
		t.Fatal(err)
	}
	if id != testSongId {
		t.Fatalf("ParseSongID(%s): got %v, want %v", testSongStr, id, testSongId)
	}
}

func TestSongID_String(t *testing.T) {
	s := SongID(testSongId).String()
	if s != testSongStr {
		t.Fatalf("SongID.String(): got %s, want %s", s, testSongStr)
	}
}

func TestSongID_JsonMarshal(t *testing.T) {
	id := SongID(testSongId)
	data, err := json.Marshal(id)
	if err != nil {
		t.Fatal(err)
	}
	if string(data) != testSongIdJson {
		t.Fatalf("SongID.MarshalJSON(): got %s, want %s", string(data), testSongIdJson)
	}
}

func TestSongID_JsonUnmarshal(t *testing.T) {
	var id SongID
	err := json.Unmarshal([]byte(testSongIdJson), &id)
	if err != nil {
		t.Fatal(err)
	}
	if id != testSongId {
		t.Fatalf("SongID.MarshalJSON(): got %v, want %v", id, testSongId)
	}
}
