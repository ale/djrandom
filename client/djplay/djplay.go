package main

import (
	"flag"
	"fmt"
	"io"
	"log"
	"net/url"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/client"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
)

var (
	configFile = flag.String("config", util.ExpandTilde("~/.djrandom.conf"), "Config file location")

	serverUrl     = config.String("server", "https://djrandom.incal.net", "Server URL")
	authKeyId     = config.String("auth_key", "", "API authentication key")
	authKeySecret = config.String("auth_secret", "", "API authentication secret")
	player        = config.String("player", "avplay -vn -nodisp -", "Audio player program")
	doPlaylist    = flag.Bool("playlist", false, "Output a playlist (m3u)")
	doDownload    = flag.Bool("download", false, "Download the results of the search")
	withDAV       = flag.Bool("with-dav", false, "Use the WebDAV API (deprecated)")
	trackSort     = flag.Bool("sort", false, "Sort output by album/track")
	fill          = flag.Int("fill", -1, "Fill the playlist with pseudo-random results up to the specified number of total songs")
)

func addAuthToUrl(u string) string {
	pfxcount := 8
	if strings.HasPrefix(u, "http:") {
		pfxcount = 7
	}
	return fmt.Sprintf("%s%s:%s@%s", u[:pfxcount], *authKeyId, *authKeySecret, u[pfxcount:])
}

func escape(s string) string {
	return strings.Replace(s, " ", "%20", -1)
}

func songURL(song *api.Song) string {
	af := song.GetBestAudioFile()
	if af == nil {
		return ""
	}

	url := addAuthToUrl(*serverUrl)
	if *withDAV {
		return fmt.Sprintf("%s/dav/%s/%s/%s%s",
			url,
			escape(song.Meta.Artist),
			escape(song.Meta.Album),
			escape(song.Meta.Title),
			api.GetExtensionForMimeType(af.MimeType))
	}
	return fmt.Sprintf("%s/dl/%s", url, af.MD5)
}

func outputSongs(songs []*api.Song) {
	fmt.Printf("#EXTM3U\n\n")
	for _, song := range songs {
		if len(song.Files) == 0 {
			continue
		}
		fmt.Printf("#EXTINF:-1,%s - %s - %s\n%s\n\n",
			song.Meta.Artist,
			song.Meta.Album,
			song.Meta.Title,
			songURL(song))
	}
}

func play(client *util.HttpClient, song *api.Song) {
	if len(song.Files) == 0 {
		return
	}

	af := song.GetBestAudioFile()
	resp, err := client.GetRaw("/dl/" + af.MD5)
	if err != nil {
		log.Fatal(err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		log.Printf("Error in request: status = %d", resp.StatusCode)
		return
	}

	fmt.Printf("(*) %s - %s\n", song.Meta.Artist, song.Meta.Title)
	playCmd := strings.Split(*player, " ")
	cmd := exec.Command(playCmd[0], playCmd[1:]...)
	cmd.Stdout = os.Stderr
	cmd.Stderr = os.Stderr
	stdin, err := cmd.StdinPipe()
	if err != nil {
		log.Fatal(err)
	}
	if err := cmd.Start(); err != nil {
		log.Fatal(err)
	}

	_, err = io.Copy(stdin, resp.Body)
	if err != nil {
		log.Printf("Error copying audio: %v", err)
	}
	stdin.Close()

	// Ignore errors.
	cmd.Wait()
}

func playSongs(client *util.HttpClient, songs []*api.Song) {
	for _, song := range songs {
		play(client, song)
	}
}

func download(client *util.HttpClient, song *api.Song) {
	// Create the destination path.
	dstPath := "."
	if song.Meta.Artist != "" {
		dstPath = filepath.Join(dstPath, song.Meta.Artist)
	}
	if song.Meta.Album != "" {
		dstPath = filepath.Join(song.Meta.Artist, song.Meta.Album)
	}
	if dstPath != "." {
		os.MkdirAll(dstPath, 0755)
	}

	// Find a suitable name for the file.
	af := song.GetBestAudioFile()
	ext := api.GetExtensionForMimeType(af.MimeType)
	var dstName string
	if song.Meta.TrackNum > 0 {
		dstName = fmt.Sprintf("%02d - %s%s", song.Meta.TrackNum, song.Meta.Title, ext)
	} else {
		dstName = fmt.Sprintf("%s%s", song.Meta.Title, ext)
	}

	file, err := os.Create(filepath.Join(dstPath, dstName))
	if err != nil {
		log.Printf("Error creating output file: %s", err)
		return
	}
	defer file.Close()

	resp, err := client.GetRaw("/dl/" + af.MD5)
	if err != nil {
		log.Printf("Error downloading file %s: %s", af.MD5, err)
		return
	}
	defer resp.Body.Close()

	if resp.StatusCode != 200 {
		log.Printf("Error downloading file %s: HTTP status %d", af.MD5, resp.StatusCode)
		return
	}

	if _, err := io.Copy(file, resp.Body); err != nil {
		log.Printf("Error downloading file %s: %s", af.MD5, err)
		return
	}
}

func downloadSongs(client *util.HttpClient, songs []*api.Song) {
	for _, song := range songs {
		download(client, song)
	}
}

func search(client *util.HttpClient, query string) []*api.Song {

	values := url.Values{}
	values.Add("q", query)
	req, _ := client.NewRequest("POST", "/api/search", strings.NewReader(values.Encode()))
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	var resp api.SearchResponse
	if err := client.DoJSON(nil, req, &resp); err != nil {
		log.Fatal(err)
	}

	return resp.Results
}

const maxCurSongs = 3

func suggest(client *util.HttpClient, curResults []*api.Song, n int) []*api.Song {
	if len(curResults) >= n {
		return curResults
	}

	// Fill in the CurrentSongs field starting from the
	// end.
	pos := len(curResults) - maxCurSongs
	if pos < 0 {
		pos = 0
	}
	var c []api.SongID
	for i := pos; i < len(curResults); i++ {
		c = append(c, curResults[i].Id)
	}

	// Call 'suggest'.
	req := api.SuggestRequest{
		CurrentSongs: c,
		NumResults:   n - len(curResults),
	}
	log.Printf("suggest() <- %v", req)
	var resp api.SuggestResponse
	if err := client.Post("/api/suggest", &req, &resp); err != nil {
		log.Printf("Error in suggest(): %s", err)
		return curResults
	}

	// If there are no results, bail out.
	if len(resp.Results) == 0 {
		log.Printf("suggest: no more results")
		return curResults
	}

	log.Printf("suggest() -> %v", resp)

	curResults = append(curResults, resp.Results...)
	return curResults
}

type lessFunc func(s1, s2 *api.Song) bool

type multiSorter struct {
	songs []*api.Song
	less  []lessFunc
}

func (ms *multiSorter) Sort(songs []*api.Song) {
	ms.songs = songs
	sort.Sort(ms)
}

func OrderedBy(less ...lessFunc) *multiSorter {
	return &multiSorter{
		less: less,
	}
}

func (ms *multiSorter) Len() int {
	return len(ms.songs)
}

func (ms *multiSorter) Swap(i, j int) {
	ms.songs[i], ms.songs[j] = ms.songs[j], ms.songs[i]
}

func (ms *multiSorter) Less(i, j int) bool {
	p, q := ms.songs[i], ms.songs[j]
	var k int
	for k = 0; k < len(ms.less)-1; k++ {
		less := ms.less[k]
		switch {
		case less(p, q):
			return true
		case less(q, p):
			return false
		}
	}
	return ms.less[k](p, q)
}

func sortSongsByTrack(songs []*api.Song) {
	artist := func(s1, s2 *api.Song) bool {
		return s1.Meta.Artist < s2.Meta.Artist
	}
	album := func(s1, s2 *api.Song) bool {
		return s1.Meta.Album < s2.Meta.Album
	}
	track := func(s1, s2 *api.Song) bool {
		return s1.Meta.TrackNum < s2.Meta.TrackNum
	}
	title := func(s1, s2 *api.Song) bool {
		return s1.Meta.Title < s2.Meta.Title
	}

	OrderedBy(artist, album, track, title).Sort(songs)
}

func main() {
	flag.Parse()
	if err := config.Parse(*configFile); err != nil {
		log.Printf("Warning: could not read config file %s: %s", *configFile, err.Error())
	}
	if *doPlaylist && *doDownload {
		log.Fatal("Specify only one out of --play and --download")
	}

	query := strings.Join(flag.Args(), " ")
	if query == "" {
		log.Fatal("Usage: djplay [<OPTIONS>] <QUERY>")
	}

	authKey := &api.AuthKey{
		KeyId:  *authKeyId,
		Secret: *authKeySecret,
	}
	certPool := client.LoadCA()
	client := util.NewHttpClient(*serverUrl, authKey, certPool)

	results := search(client, query)
	if *fill > 0 {
		results = suggest(client, results, *fill)
	}
	if *trackSort {
		sortSongsByTrack(results)
	}

	// Output the results.
	if *doPlaylist {
		outputSongs(results)
	} else if *doDownload {
		downloadSongs(client, results)
	} else {
		playSongs(client, results)
	}
}
