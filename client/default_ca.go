package client

import (
	"crypto/x509"
	"flag"
	"io/ioutil"
	"log"

	"git.autistici.org/ale/djrandom/util/config"
)

var (
	caPath = config.String("ca", "", "CA X509 certificate, PEM-encoded (default: internal)")
	useSystemCertPool = flag.Bool("use-system-cert-pool", false, "Use the system-wide X509 certificate pool for SSL verification")
)

var defaultCaCertificate = `
-----BEGIN CERTIFICATE-----
MIIGdDCCBFygAwIBAgIEAuJwkzANBgkqhkiG9w0BAQUFADBaMQswCQYDVQQGEwJJ
VDESMBAGA1UEChMJSW5jYWwubmV0MQswCQYDVQQLEwJDQTEqMCgGA1UEAxMhSW5j
YWwubmV0IENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTEyMDIwODExMTQ0MVoX
DTIyMDIwNTExMTQ0MVowWjELMAkGA1UEBhMCSVQxEjAQBgNVBAoTCUluY2FsLm5l
dDELMAkGA1UECxMCQ0ExKjAoBgNVBAMTIUluY2FsLm5ldCBDZXJ0aWZpY2F0aW9u
IEF1dGhvcml0eTCCAiIwDQYJKoZIhvcNAQEBBQADggIPADCCAgoCggIBAONwjVgE
x8mmd9qnalICtxkPrfUQXFeRQIhGVpLr2opM8smJwtuoF+DFy4UQ4/vGOUnmzWvB
BcW1Fo/As7VAZA9TKDS36mENprTgiuG2RfeU79qEDpw/QenVsGEPBoxcFwkiMAg5
5Vl4XxGkOShRqm/o40FeIfK8a9F5tLHQVya+5RjlQaIrRXJvWMOfdbKP/jt5bOVx
ysVfGp7hmlLdROQtuPGzzm+8i+rE9TymCBK/jFO8n/t+4nJuCNOoLld879lt/uR1
VpoX6cEOEY+B+Y1M4X2mMQrRX+bb3mjfE1kS95sDst+4MCm/F+T9Okir0sWj5qbx
qvVWbu9OEaUnUo+mB7Ve1RodfO10Z6z4j3Hbl1HlEIXql+L/51oRKRNKYUGqLAKq
oli9o75WzuUBRldtMHHziJr9mmK/9jh1Sk8LAk2zS3TbNgD19XrFxfnu21dCyNqQ
rGxjL8rmXX0gjnVWATTti3I3kv8pvhuzvHzlMK+2NBRhUEbMHxR8bAjWpIsudn44
aVmuAByJimoekdEJSI8gCb0wynM8Xzn4COg22/JfxV1ueS6bxQ2DxtOYJjz7G2Bt
0cYtzETkYbnX3M2uZ0tcP0N+veN+KajW7hScfa8NfnJXUcyP1TV+w3/KPb+57TRr
R7fz7u0/wr1RScruHu4j9GMCxqcrnbbovWWDAgMBAAGjggFAMIIBPDAdBgNVHQ4E
FgQU9+NI2fWaMOKo+c31dza79zpcUzYwgYUGA1UdIwR+MHyAFPfjSNn1mjDiqPnN
9Xc2u/c6XFM2oV6kXDBaMQswCQYDVQQGEwJJVDESMBAGA1UEChMJSW5jYWwubmV0
MQswCQYDVQQLEwJDQTEqMCgGA1UEAxMhSW5jYWwubmV0IENlcnRpZmljYXRpb24g
QXV0aG9yaXR5ggQC4nCTMA8GA1UdEwEB/wQFMAMBAf8wCwYDVR0PBAQDAgEGMBEG
CWCGSAGG+EIBAQQEAwIABzAwBglghkgBhvhCAQ0EIxYhSW5jYWwubmV0IENlcnRp
ZmljYXRpb24gQXV0aG9yaXR5MBcGA1UdEQQQMA6BDGNhQGluY2FsLm5ldDAXBgNV
HRIEEDAOgQxjYUBpbmNhbC5uZXQwDQYJKoZIhvcNAQEFBQADggIBAMXHRzd+NXNg
idOqAbkECg7HzMCVFoJ9AIWQmX5np+JasSJhxAZ7qbu0M9JW4ZHhUhPYYD59UhF4
WsQMOVsFhkrr8wtdZ8UAsSM341AY0rKgKMZefQUZQjAmrbYzF8ZPfNvaBfPN4VYV
t1Quq3egVsS2ajRO4ru9OKS1MhEST7AjIOg0yz93eERVJzPqfjjepc9ENww/GlZa
m4X2HIqmGKzc4XYCgs2g0d+Lfl3ww4yvkUNj17tA1gawo+1944Q9huJLgWK9Vyvv
MYsHw88H00ZOQmhFtqsiJUAhAdbQqOKIUDm4R++ePs4Z76cCwcDpaf54m1bPKXVx
lI/v1/Kie2lEVLTOmjus/x24h3bVKPlqdsYqQLnKw+JRXtiP++s1RBBUP9Ww511A
qISiowgf46ZSw+Ec8CgDu7cPQ53dBvzX/0dtoCJMA3vvPTSqA3Zqs8GmkA9VTPAW
c+AEIV5tkflxwDHHzpk+kYRoSJ5ULzJfL9Roa4fY5++JvVPlEM19JktLAxDX7NSB
Aos369dlA8zrSXHWAhSv1tVvPDseFD4P4E0t99RW3I87XIo5PoeYnq4jxFFTmUYZ
ujlsptsUv+StaZ4cavGFePGzUaU0WLtfCCZVBtHOEbsHVVhSKLpPqYVXd0xxF0mn
2Zmz+vMyl7vyVyBDcnq4UntbohynF626
-----END CERTIFICATE-----
`

func LoadCA() *x509.CertPool {
	if *useSystemCertPool {
		return nil
	}

	data := []byte(defaultCaCertificate)
	if *caPath != "" {
		var err error
		data, err = ioutil.ReadFile(*caPath)
		if err != nil {
			log.Fatalf("Error reading CA certificate: %s", err)
		}
	}

	pool := x509.NewCertPool()
	if !pool.AppendCertsFromPEM(data) {
		log.Fatal("No certificate could be read")
	}
	return pool
}
