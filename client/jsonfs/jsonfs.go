package jsonfs

import (
	"crypto/x509"
	"io"
	"net/url"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/util"
)

type JsonFsClient struct {
	client *util.HttpClient
}

func NewJsonFsClient(serverUrl, authKeyId, authKeySecret string, caPool *x509.CertPool) *JsonFsClient {
	authKey := api.AuthKey{KeyId: authKeyId, Secret: authKeySecret}
	return &JsonFsClient{
		util.NewHttpClient(serverUrl, &authKey, caPool),
	}
}

func (c *JsonFsClient) call(op, path string, out interface{}) error {
	values := url.Values{}
	values.Add("op", op)
	values.Add("path", path)

	reqUrl := "/jsonfs/?" + values.Encode()

	return c.client.Get(reqUrl, out)
}

func (c *JsonFsClient) Ls(path string) ([]*api.JsonFsAttr, error) {
	var resp api.JsonFsLsResponse
	if err := c.call("ls", path, &resp); err != nil {
		return nil, err
	}
	return resp.Results, nil
}

func (c *JsonFsClient) Stat(path string) (api.JsonFsAttr, error) {
	var resp api.JsonFsAttr
	err := c.call("stat", path, &resp)
	return resp, err
}

func (c *JsonFsClient) Open(attr api.JsonFsAttr) (io.ReadCloser, error) {
	resp, err := c.client.GetRaw("/dl/" + attr.Audio.MD5)
	if err != nil {
		return nil, err
	}
	return resp.Body, nil
}
