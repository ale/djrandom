package main

import (
	"flag"
	"fmt"
	"log"

	"git.autistici.org/ale/djrandom/client/mpd"
	"git.autistici.org/ale/gompd"
)

var (
	port = flag.Int("port", 6600, "Port to listen on (MPD)")
)

func main() {
	flag.Parse()

	db := djmpd.NewDJRandomDatabase()
	m := mpd.NewMPD(db, db.GetPlayRecorder())
	m.HandleURL("djrandom", db)
	log.Fatal(m.ListenAndServe(fmt.Sprintf(":%d", *port)))
}
