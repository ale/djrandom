package djmpd

import (
	"bytes"
	"container/list"
	"errors"
	"flag"
	"fmt"
	"io"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"sync"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/client"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"

	"git.autistici.org/ale/gompd"
)

var (
	configFile = flag.String("djrandom-config", util.ExpandTilde("~/.djrandom.conf"), "Config file location")
	maxResults = flag.Int("max-search-results", 200, "maximum number of search results to return")

	serverUrl     = config.String("djrandom-server", "https://djrandom.incal.net", "Server URL")
	authKeyId     = config.String("auth_key", "", "API authentication key")
	authKeySecret = config.String("auth_secret", "", "API authentication secret")

	djrandomOnce sync.Once
)

type DJRandomSong struct {
	*api.Song

	audiofile *api.AudioFile
	client    *util.HttpClient
}

func newDJRandomSong(song *api.Song, client *util.HttpClient) *DJRandomSong {
	af := song.GetBestAudioFile()
	return &DJRandomSong{
		Song:      song,
		audiofile: af,
		client:    client,
	}
}

func (s *DJRandomSong) Channels() int {
	if s.audiofile != nil {
		return s.audiofile.Channels
	}
	return 2
}

func (s *DJRandomSong) SampleRate() float64 {
	if s.audiofile != nil {
		return float64(s.audiofile.SampleRate)
	}
	return 44100
}

func (s *DJRandomSong) Duration() int {
	if s.audiofile != nil {
		return int(s.audiofile.Duration)
	}
	return 0
}

func (s *DJRandomSong) URL() string {
	return fmt.Sprintf("djrandom://%s", s.Id.String())
}

func (s *DJRandomSong) Info() string {
	var buf bytes.Buffer
	fmt.Fprintf(&buf, "file: %s\n", s.URL())
	fmt.Fprintf(&buf, "Last-Modified: 2010-12-16T18:02:14Z\n")
	if s.Meta.Artist != "" {
		fmt.Fprintf(&buf, "Artist: %s\n", s.Meta.Artist)
	}
	if s.Meta.Title != "" {
		fmt.Fprintf(&buf, "Title: %s\n", s.Meta.Title)
	}
	if s.Meta.Album != "" {
		fmt.Fprintf(&buf, "Album: %s\n", s.Meta.Album)
	}
	if s.Meta.Genre != "" {
		fmt.Fprintf(&buf, "Genre: %s\n", s.Meta.Genre)
	}
	if s.Meta.Year > 0 {
		fmt.Fprintf(&buf, "Date: %d\n", s.Meta.Year)
	}
	if s.Meta.TrackNum > 0 {
		fmt.Fprintf(&buf, "Track: %d\n", s.Meta.TrackNum)
	}
	if s.audiofile != nil {
		fmt.Fprintf(&buf, "Time: %d\n", int(s.audiofile.Duration))
	}
	return buf.String()
}

func (s *DJRandomSong) Open() (io.ReadCloser, error) {
	if s.audiofile == nil {
		return nil, errors.New("no audio file")
	}

	return &songReader{
		client: s.client,
		url:    "/dl/" + s.audiofile.MD5,
		size:   s.audiofile.Size,
	}, nil
}

// A streaming HTTP reader that can resume the download on error.
type songReader struct {
	client    *util.HttpClient
	url       string
	pos, size int64
	resp      *http.Response
	closed    bool
}

func (r *songReader) Read(buf []byte) (int, error) {
	var err error
	for i := 0; !r.closed && i < 3; i++ {
		if r.pos >= r.size {
			return 0, io.EOF
		}
		if r.resp == nil {
			r.resp, err = r.client.GetRawRange(r.url, r.pos, r.size)
			if err != nil {
				return 0, err
			}
			if r.resp.StatusCode > 300 {
				return 0, fmt.Errorf("HTTP error %d", r.resp.StatusCode)
			}
		}

		var n int
		n, err = r.resp.Body.Read(buf)
		if n > 0 {
			r.pos += int64(n)
			return n, err
		}
		r.resp.Body.Close()
		r.resp = nil
	}
	return 0, err
}

func (r *songReader) Close() error {
	r.closed = true
	if r.resp != nil {
		r.resp.Body.Close()
	}
	return nil
}

type DJRandomDatabase struct {
	client *util.HttpClient
}

func NewDJRandomDatabase() *DJRandomDatabase {
	djrandomOnce.Do(func() {
		if err := config.Parse(*configFile); err != nil {
			log.Printf("Warning: could not read DJRandom config file: %v", err)
		}
	})

	authKey := &api.AuthKey{
		KeyId:  *authKeyId,
		Secret: *authKeySecret,
	}
	client := util.NewHttpClient(*serverUrl, authKey, client.LoadCA())
	return &DJRandomDatabase{
		client: client,
	}
}

func (d *DJRandomDatabase) doQuery(query string) ([]mpd.Song, error) {
	log.Printf("literal query: %s", query)

	values := url.Values{}
	values.Add("q", query)
	values.Add("limit", strconv.Itoa(*maxResults))
	req, err := d.client.NewRequest("POST", "/api/search", strings.NewReader(values.Encode()))
	if err != nil {
		return nil, err
	}
	req.Header.Set("Content-Type", "application/x-www-form-urlencoded")

	var resp api.SearchResponse
	if err := d.client.DoJSON(nil, req, &resp); err != nil {
		return nil, err
	}

	// Convert []api.Song to []Song.
	var out []mpd.Song
	for _, s := range resp.Results {
		out = append(out, newDJRandomSong(s, d.client))
	}
	return out, nil
}

func buildQuery(args []string, exact bool) string {
	var qprefix string
	if exact {
		qprefix = "="
	}
	i := 0
	var qparts []string
	for i < len(args) {
		i++
		if i >= len(args) {
			break
		}
		what := strings.ToLower(args[i-1])
		q := args[i]
		if what == "any" {
			qparts = append(qparts, fmt.Sprintf("\"%s%s\"", qprefix, q))
		} else {
			qparts = append(qparts, fmt.Sprintf("%s:\"%s%s\"", what, qprefix, q))
		}
		i++
	}
	return strings.Join(qparts, " ")
}

func (d *DJRandomDatabase) Search(args []string) ([]mpd.Song, error) {
	return d.doQuery(buildQuery(args, false))
}

func (d *DJRandomDatabase) Find(args []string) ([]mpd.Song, error) {
	return d.doQuery(buildQuery(args, true))
}

func (d *DJRandomDatabase) GetSong(songURL *url.URL) (mpd.Song, error) {
	songID := songURL.Host
	if songID == "" {
		return nil, errors.New("empty song ID")
	}

	req, err := d.client.NewRequest("GET", "/api/song/"+songID, nil)
	if err != nil {
		return nil, err
	}

	var resp api.Song
	if err := d.client.DoJSON(nil, req, &resp); err != nil {
		return nil, err
	}

	// Convert api.Song to Song.
	return newDJRandomSong(&resp, d.client), nil
}

func (d *DJRandomDatabase) GetPlayRecorder() *PlayRecorder {
	return newPlayRecorder(d.client)
}

type PlayRecorder struct {
	client *util.HttpClient
	played *list.List
	count  int
	size   int
	lock   sync.Mutex
}

func newPlayRecorder(client *util.HttpClient) *PlayRecorder {
	return &PlayRecorder{
		client: client,
		played: list.New(),
		size:   5,
	}
}

func (r *PlayRecorder) Notify(song mpd.Song) {
	s, ok := song.(*DJRandomSong)
	if !ok {
		return
	}

	r.lock.Lock()
	defer r.lock.Unlock()

	// songID := s.Id
	r.played.PushBack(s.Id)
	if r.count < r.size {
		r.count++
	} else {
		r.played.Remove(r.played.Front())
	}

	var ids []api.SongID
	for e := r.played.Front(); e != nil; e = e.Next() {
		ids = append(ids, e.Value.(api.SongID))
	}

	// Make the HTTP request in a separate goroutine so that we
	// don't block on network issues (ignores errors).
	go func() {
		log.Printf("add_playlog: %v", ids)
		req := api.AddPlayLogRequest{Songs: ids}
		var resp bool
		if err := r.client.Post("/api/add_playlog", &req, &resp); err != nil {
			log.Printf("add_playlog error: %v", err)
		}
	}()
}
