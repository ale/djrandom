package upload

import (
	"errors"
	"log"
	"os"
)

func isFileUnchanged(fileinfo *FileInfo) (bool, error) {
	curInfo, err := os.Stat(fileinfo.Path)
	if err != nil {
		log.Printf("Couldn't stat %s: %s", fileinfo.Path, err.Error())
		return false, nil
	}

	// Verify that the file size and mtime still match: this should
	// hopefully catch files that are still being written to.
	if curInfo.ModTime() != fileinfo.Info.ModTime() || curInfo.Size() != fileinfo.Info.Size() {
		log.Printf("File %s changed while looking at it", fileinfo.Path)
		return false, errors.New("file changed")
	}

	return true, nil
}

func StableFileFilter() Filter {
	return FilterFunc(isFileUnchanged)
}
