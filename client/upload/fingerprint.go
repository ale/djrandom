package upload

import (
	"errors"
	"fmt"
	"log"
	"path/filepath"
	"strings"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/metadata_extraction"
)

type FpChecker struct {
	http           *util.HttpClient
	mimeTypesByExt map[string]string
}

func NewFingerprintChecker(httpClient *util.HttpClient, mimeTypesByExt map[string]string) *FpChecker {
	c := &FpChecker{
		http:           httpClient,
		mimeTypesByExt: mimeTypesByExt,
	}
	return c
}

// getFingerprint is a fingerprint function for audio files, which
// uses the 'metadata_extraction' package.
func (c *FpChecker) getFingerprint(fileinfo *FileInfo) (string, error) {
	// Detect the mime type using the file extension.
	ext := strings.ToLower(filepath.Ext(fileinfo.Path))
	mimeType, ok := c.mimeTypesByExt[ext]
	if !ok {
		return "", fmt.Errorf("unknown mime type (extension: %s)", ext)
	}

	af, _, err := metadata_extraction.GetMetadata(fileinfo.Path, mimeType)
	if err != nil {
		return "", err
	}

	// For some reason, in certain cases GetMetadata() returns
	// successfully with an invalid fingerprint (all zeros).
	if af.MD5 == "00000000000000000000000000000000" {
		return "", errors.New("invalid fingerprint (all zeros)")
	}

	return af.MD5, nil
}

// Perform a remote call to the server to check a batch of fingerprints.
func (c *FpChecker) checkFingerprints(fingerprints []string) ([]string, []string, error) {
	req := api.FingerprintRequest{Fingerprints: fingerprints}
	var resp api.FingerprintResponse
	err := c.http.Post("/api/check_fp", &req, &resp)
	if err != nil {
		log.Printf("Error checking fingerprint for %v: %s", fingerprints, err)
		return []string{}, []string{}, err
	}
	return resp.Missing, resp.Dupes, nil
}

// Handle returns true if the file should be uploaded (it is missing
// on the server).
func (c *FpChecker) Handle(fileinfo *FileInfo) (bool, error) {
	// Compute the file fingerprint.
	fp, err := c.getFingerprint(fileinfo)
	if err != nil {
		// Fingerprinting errors are permanent.
		return false, nil
	}
	fileinfo.Meta["fingerprint"] = fp

	// Run the remote check.
	missingFp, _, err := c.checkFingerprints([]string{fp})
	if err != nil {
		// An API error is temporary.
		return false, err
	}

	// If the file is missing on the server, go ahead.
	if len(missingFp) > 0 && missingFp[0] == fp {
		return true, nil
	}
	return false, nil
}
