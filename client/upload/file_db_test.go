package upload

import (
	"io/ioutil"
	"os"
	"testing"
	"time"
)

var (
	testDbPath = ".dbtest1"
	testFile   = ".dbtestfile"
)

func createTestFile() *FileInfo {
	ioutil.WriteFile(testFile, []byte("data"), 0600)
	stat, _ := os.Stat(testFile)
	return NewFileInfo(testFile, stat)
}

func TestFileDb_Check(t *testing.T) {
	dbPath := testDbPath + ".1"
	db := NewFileDb(dbPath)
	defer os.RemoveAll(dbPath)

	info := createTestFile()
	defer os.Remove(testFile)

	seen, err := db.Check(info)
	if err != nil {
		t.Fatalf("Error in Check(): %s", err.Error())
	}
	if seen != false {
		t.Errorf("Check() returned true")
	}
}

func TestFileDb_Update(t *testing.T) {
	dbPath := testDbPath + ".2"
	db := NewFileDb(dbPath)
	defer os.RemoveAll(dbPath)

	info := createTestFile()
	defer os.Remove(testFile)

	seen, err := db.Check(info)
	if err != nil {
		t.Fatalf("Error in Check(): %s", err.Error())
	}
	if seen != false {
		t.Errorf("Check() returned true")
	}

	err = db.Update(info)
	if err != nil {
		t.Fatalf("Error in Update(): %s", err.Error())
	}

	// Check again, with the same stat info.
	seen, err = db.Check(info)
	if err != nil {
		t.Fatalf("Error in Check(): %s", err.Error())
	}
	if seen != true {
		t.Errorf("Check() returned false after update")
	}

	// Update file and stat again. Expect results to change. In
	// order to get a different mtime, we'll wait for 1 sec.
	time.Sleep(time.Second)

	info = createTestFile()
	seen, err = db.Check(info)
	if err != nil {
		t.Fatalf("Error in Check(): %s", err.Error())
	}
	if seen != false {
		t.Errorf("Check() returned true with changed stat data")
	}
}
