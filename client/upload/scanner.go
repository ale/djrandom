package upload

import (
	"fmt"
	"html/template"
	"io"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"sync"
	"time"

	"git.autistici.org/ale/djrandom/util/status"
)

const debugText = `<html>
<body>
<title>Uploader State</title>
<table>
 <thead>
  <tr>
   <th>Worker#</th>
   <th>State</th>
  </tr>
 </thead>
 <tbody>
  {{range $idx, $state := .States}}
  <tr><td>{{$idx}}</td><td>{{$state}}</td></tr>
  {{end}}
 </tbody>
</table>
<h3>Error Log</h3>
<p>{{range .GetErrorLog}}{{.}}<br>{{end}}</p>
</body>
</html>`

var debug = template.Must(template.New("Scanner debug").Parse(debugText))

// getAbsPath finds a directory's absolute path by cd'ing into it and
// calling Getwd.
func getAbsPath(path string) (string, error) {
	oldCwd, oldErr := os.Getwd()
	err := os.Chdir(path)
	if err != nil {
		return path, err
	}
	abs, err := os.Getwd()
	if err != nil {
		return abs, err
	}
	if oldErr == nil {
		os.Chdir(oldCwd)
	}
	return abs, nil
}

// Scanner state, for debugging.
type scannerState struct {
	Counters   map[string]int64
	States     []string
	errLog     []string
	errLogHead int
	lock       sync.Mutex
}

// Keep a single, global pointer to the current scan state.
var curScannerState *scannerState

func newScannerState(numWorkers int) *scannerState {
	return &scannerState{
		Counters: make(map[string]int64),
		States:   make([]string, numWorkers),
		errLog:   make([]string, 100),
	}
}

func (st *scannerState) Log(msg string) {
	st.lock.Lock()
	defer st.lock.Unlock()

	msg = fmt.Sprintf("[%s] %s", time.Now().Format(time.Stamp), msg)
	st.errLog[st.errLogHead] = msg
	st.errLogHead++
	if st.errLogHead > len(st.errLog) {
		st.errLogHead = 0
	}
}

func (st *scannerState) GetErrorLog() []string {
	out := make([]string, 0, len(st.errLog))
	for i := st.errLogHead - 1; i != st.errLogHead; i-- {
		if i < 0 {
			i = len(st.errLog) - 1
		}
		if st.errLog[i] != "" {
			out = append(out, st.errLog[i])
		}
	}
	return out
}

func (st *scannerState) Incr(c string) {
	st.lock.Lock()
	defer st.lock.Unlock()
	st.Counters[c] = st.Counters[c] + 1
}

func (st *scannerState) SetState(worker int, state string) {
	st.lock.Lock()
	defer st.lock.Unlock()
	st.States[worker] = state
}

func (st *scannerState) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	st.lock.Lock()
	defer st.lock.Unlock()
	debug.Execute(w, st)
}

func debugScannerStateHandler(w http.ResponseWriter, r *http.Request) {
	if curScannerState != nil {
		curScannerState.ServeHTTP(w, r)
	} else {
		io.WriteString(w, "<p>No scan running.</p>")
	}
}

func init() {
	http.Handle("/debug/djuploader", http.HandlerFunc(debugScannerStateHandler))
	status.RegisterDebugEndpoint("/debug/djuploader")
}

// FSScanner represents a filesystem scanner that can include or exclude
// specific patterns.
type FSScanner struct {
	Root            string
	db              *FileDb
	state           *scannerState
	numWorkers      int
	excludePatterns []string
	includePatterns []string
}

func NewFSScanner(root string, db *FileDb, numWorkers int) *FSScanner {
	absPath, err := getAbsPath(root)
	if err != nil {
		absPath = root
	}
	return &FSScanner{
		Root:       absPath,
		db:         db,
		state:      newScannerState(numWorkers),
		numWorkers: numWorkers,
	}
}

func (fs *FSScanner) AddExcludePattern(pat string) {
	fs.excludePatterns = append(fs.excludePatterns, pat)
}

func (fs *FSScanner) AddIncludePattern(pat string) {
	fs.includePatterns = append(fs.includePatterns, pat)
}

func matchAny(name string, patterns []string) bool {
	for _, pattern := range patterns {
		if match, _ := filepath.Match(pattern, name); match {
			return true
		}
	}
	return false
}

func (fs *FSScanner) match(path string, info os.FileInfo, err error, out chan *FileInfo) error {
	if err != nil {
		return nil
	}
	if fs.excludePatterns != nil && matchAny(info.Name(), fs.excludePatterns) {
		if info.IsDir() {
			return filepath.SkipDir
		} else {
			return nil
		}
	}
	if fs.includePatterns != nil && !matchAny(info.Name(), fs.includePatterns) {
		return nil
	}
	out <- NewFileInfo(path, info)
	return nil
}

func (fs *FSScanner) Walk(out chan *FileInfo) error {
	return filepath.Walk(fs.Root, func(path string, info os.FileInfo, err error) error {
		return fs.match(path, info, err, out)
	})
}

// SinkFunc is a function that processes its input.
type SinkFunc func(*FileInfo)

// Filter is the generic interface for filters.
type Filter interface {
	Handle(*FileInfo) (bool, error)
}

type filterFuncWrapper struct {
	fn func(*FileInfo) (bool, error)
}

// FilterFunc lets you wrap a plain function with a Filter interface.
func FilterFunc(fn func(*FileInfo) (bool, error)) Filter {
	return &filterFuncWrapper{fn}
}

func (f *filterFuncWrapper) Handle(fileinfo *FileInfo) (bool, error) {
	return f.fn(fileinfo)
}

// A simple filter that selects files with size greater than 0.
func NonEmptyFileFilter() Filter {
	return FilterFunc(func(fileinfo *FileInfo) (bool, error) {
		return (!fileinfo.Info.IsDir() && fileinfo.Info.Size() > 0), nil
	})
}

// Filter files with a minimum size.
func MinimumSizeFilter(minSize int64) Filter {
	return FilterFunc(func(fileinfo *FileInfo) (bool, error) {
		return (fileinfo.Info.Size() > minSize), nil
	})
}

// WalkWithFilters scans the filesystem and applies the specified
// filters to the files that are found.
func (fs *FSScanner) WalkWithFilters(filters []Filter) {
	var wg sync.WaitGroup
	ch := make(chan *FileInfo)

	curScannerState = fs.state

	log.Printf("starting scan of %s", fs.Root)
	for i := 0; i < fs.numWorkers; i++ {
		wg.Add(1)
		go func(workerId int) {
			fs.state.SetState(workerId, "idle")
			for fileinfo := range ch {
				seen, _ := fs.db.Check(fileinfo)
				if seen {
					continue
				}
				fs.state.SetState(workerId, "processing "+fileinfo.Path)
				fs.state.Incr("files/all")

				var ok bool
				var err error
				for _, f := range filters {
					ok, err = f.Handle(fileinfo)
					// If something happened, break out of the loop.
					if !ok || err != nil {
						break
					}
				}
				if ok {
					fs.state.Incr("files/ok")
				} else {
					fs.state.Incr("files/not_ok")
				}

				// Temporary error, skip this file and retry later.
				if err != nil {
					fs.state.Incr("files/errors")
					fs.state.Log(fmt.Sprintf("error processing %s: %s", fileinfo.Path, err.Error()))
				} else {
					fs.db.Update(fileinfo)
				}
				fs.state.SetState(workerId, "idle")
			}
			wg.Done()
		}(i)
	}

	go func() {
		fs.Walk(ch)
		close(ch)
	}()

	wg.Wait()

	log.Println("scan terminated")
	curScannerState = nil
}
