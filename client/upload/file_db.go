package upload

import (
	"bytes"
	"encoding/gob"
	"log"

	"github.com/jmhodges/levigo"
)

// FileDb is a database that keeps track of which files have been seen
// and whether they have been modified.
type FileDb struct {
	db     *levigo.DB
	cache  *levigo.Cache
	filter *levigo.FilterPolicy
}

type FileStat struct {
	Mtime int64
	Size  int64
}

func NewFileDb(path string) *FileDb {
	opts := levigo.NewOptions()
	cache := levigo.NewLRUCache(2 << 28)
	opts.SetCache(cache)
	opts.SetCreateIfMissing(true)
	filter := levigo.NewBloomFilter(12)
	opts.SetFilterPolicy(filter)
	db, err := levigo.Open(path, opts)
	if err != nil {
		log.Fatal("Error creating database: %s", err)
	}

	return &FileDb{db, cache, filter}
}

func (db *FileDb) Close() {
	db.db.Close()
	db.cache.Close()
	db.filter.Close()
}

// Check returns true if the file is new (has not been seen before, or
// the modification time or size have changed since).
func (db *FileDb) Check(fileinfo *FileInfo) (bool, error) {
	ro := levigo.NewReadOptions()
	defer ro.Close()

	data, err := db.db.Get(ro, []byte(fileinfo.Path))
	if err == nil && data == nil {
		return false, err
	}

	var stat FileStat
	err = gob.NewDecoder(bytes.NewReader(data)).Decode(&stat)

	if fileinfo.Info.ModTime().Unix() > stat.Mtime || fileinfo.Info.Size() != stat.Size {
		return false, nil
	}
	return true, nil
}

// Update saves a file's data in the database.
func (db *FileDb) Update(fileinfo *FileInfo) error {
	stat := FileStat{
		Mtime: fileinfo.Info.ModTime().Unix(),
		Size:  fileinfo.Info.Size(),
	}

	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(&stat); err != nil {
		return err
	}

	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return db.db.Put(wo, []byte(fileinfo.Path), buf.Bytes())
}
