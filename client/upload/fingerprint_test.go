package upload

import (
	"os"
	"testing"

	"git.autistici.org/ale/djrandom/util"
)

func TestUploader_GetFingerprint(t *testing.T) {
	path := "../../testdata/chet.mp3"
	stat, _ := os.Stat(path)
	info := NewFileInfo(path, stat)
	info.Meta["mime-type"] = "audio/mpeg"

	client := util.NewHttpClient(
		"http://localhost", nil, nil)
	fpc := NewFingerprintChecker(
		client,
		map[string]string{
			".mp3": "audio/mpeg",
		})
	_, err := fpc.getFingerprint(info)
	if err != nil {
		t.Fatalf("getFingerprint() returned error: %s", err.Error())
	}
}
