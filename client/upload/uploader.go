package upload

import (
	"io"
	"log"
	"os"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/util"
)

type Uploader struct {
	limiter *RateLimiter
	http    *util.HttpClient
}

func NewUploader(httpClient *util.HttpClient, rateLimit int) *Uploader {
	u := new(Uploader)
	u.http = httpClient
	if rateLimit > 0 {
		u.limiter = NewRateLimiter(rateLimit * 1024)
	}
	return u
}

// Handle uploads files to the server.
func (u *Uploader) Handle(fileinfo *FileInfo) (bool, error) {
	log.Printf("Uploading %s", fileinfo.Path)
	err := u.upload(fileinfo)
	if err != nil {
		// Upload errors are retriable.
		log.Printf("Error uploading %s: %s", fileinfo.Path, err.Error())
		return false, err
	}
	return true, nil
}

func (u *Uploader) upload(fileinfo *FileInfo) error {
	var in io.ReadCloser
	var err error
	if u.limiter != nil {
		in, err = RateLimitedOpen(fileinfo.Path, u.limiter)
	} else {
		in, err = os.Open(fileinfo.Path)
	}
	if err != nil {
		return err
	}
	defer in.Close()

	var resp api.UploadResponse
	size := fileinfo.Info.Size()
	return u.http.Put("/upload", in, size, &resp)
}
