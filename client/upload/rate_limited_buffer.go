package upload

import (
	"log"
	"os"
	"time"
)

var (
	MIN_BLOCK_SIZE = 1024
)

// RateLimiter represents a global rate limit in bytes/sec.
// The time resolution of the global ticker is controlled by the
// base block size.
type RateLimiter struct {

	// BlockSize holds the size in bytes of a limiter 'tick'.
	blockSize int

	// Rate is measured in bytes per second.
	ticker <-chan time.Time

	// The signaling channel.
	ch chan time.Time
}

func NewRateLimiter(rate int) *RateLimiter {
	period := time.Duration(100 * time.Millisecond)
	blockSize := int(float64(rate) * period.Seconds())

	// If the rate is too low, extend the block size so we don't
	// kill the host with too many iops.
	if blockSize < MIN_BLOCK_SIZE {
		blockSize = MIN_BLOCK_SIZE
		period = time.Second / time.Duration(rate/blockSize)
	}

	// Catch existential errors.
	if period.Seconds() == 0 {
		log.Fatal("Attempted to create NewRateLimiter with period=0")
	}

	log.Printf("NewRateLimiter(period=%d ms, blockSize=%d)",
		int(1000*period.Seconds()), blockSize)

	rl := &RateLimiter{
		blockSize: blockSize,
		ticker:    time.Tick(period),
		ch:        make(chan time.Time, 0),
	}

	// Spawn the ticker in the background. The ticker never
	// blocks, it will only send to 'ch' if the ticks are being
	// consumed.
	go func() {
		for {
			t := <-rl.ticker
			select {
			case rl.ch <- t:
			default:
			}
		}
	}()

	return rl
}

func (rl *RateLimiter) Tick() int {
	<-rl.ch
	return rl.blockSize
}

type RateLimitedBuffer struct {
	rl     *RateLimiter
	file   *os.File
	tokens int
}

func RateLimitedOpen(path string, rl *RateLimiter) (*RateLimitedBuffer, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}

	rbuf := RateLimitedBuffer{rl: rl, file: file, tokens: 0}
	return &rbuf, nil
}

func (rbuf *RateLimitedBuffer) Read(p []byte) (int, error) {
	nr, err := rbuf.file.Read(p)
	if nr > 0 {
		// The leaky bucket abstraction: consume 'tokens' out
		// of the buffer, obtain new ones by polling the
		// ticker (and waiting).
		for rbuf.tokens < nr {
			rbuf.tokens += rbuf.rl.Tick()
		}
		rbuf.tokens -= nr
	}
	return nr, err
}

func (rbuf *RateLimitedBuffer) Close() error {
	return rbuf.file.Close()
}
