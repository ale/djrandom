package main

import (
	"flag"
	"log"
	// "net/http"
	"time"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/client"
	"git.autistici.org/ale/djrandom/client/upload"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/daemon"
)

var (
	configFile  = flag.String("config", util.ExpandTilde("~/.djrandom.conf"), "Config file location")
	once        = flag.Bool("once", false, "Run once and then exit")
	httpAddr    = flag.String("debug-http", ":4040", "Port to run the debug HTTP server on")
	threadCount = config.Int("threads", 3, "How many uploads to run in parallel")
	bwLimit     = config.Int("bw_limit", -1, "Bandwidth limit for uploads (in KBytes/s)")
	musicDir    = config.String("music_dir", "~/Music", "Music directory to scan")
	dbPath      = config.String("db_path", "~/.djrandom.db", "Local database directory")

	serverUrl     = config.String("server", "https://djrandom.incal.net", "Server URL")
	authKeyId     = config.String("auth_key", "", "API authentication key")
	authKeySecret = config.String("auth_secret", "", "API authentication secret")

	// Default parameters for the file scanner.
	mimeTypesMap = map[string]string{
		".flac": api.MIMETYPE_FLAC,
		".mp3":  api.MIMETYPE_MP3,
		".mpa":  api.MIMETYPE_MP3,
	}
)

const (
	minFileSize = 65536
)

func runScan(musicDir string, httpClient *util.HttpClient, db *upload.FileDb, numWorkers int) {
	// Create the filesystem scanner and configure it to select
	// files with known extensions.
	fs := upload.NewFSScanner(musicDir, db, numWorkers)
	fs.AddExcludePattern(".*")
	fs.AddExcludePattern(".AppleDouble") // ok, slightly redundant...
	fs.AddExcludePattern("__MACOSX")
	for ext, _ := range mimeTypesMap {
		fs.AddIncludePattern("*" + ext)
	}

	filters := []upload.Filter{
		upload.NonEmptyFileFilter(),
		upload.MinimumSizeFilter(minFileSize),
		upload.StableFileFilter(),
		upload.NewFingerprintChecker(httpClient, mimeTypesMap),
		upload.NewUploader(httpClient, *bwLimit),
	}

	fs.WalkWithFilters(filters)
}

func main() {
	flag.Parse()
	if err := config.Parse(*configFile); err != nil {
		log.Printf("Warning: could not read config file %s: %s", *configFile, err.Error())
	}

	authKey := api.AuthKey{
		KeyId:  config.MustString("auth_key", *authKeyId),
		Secret: config.MustString("auth_secret", *authKeySecret),
	}

	musicDir := util.ExpandTilde(*musicDir)
	db := upload.NewFileDb(util.ExpandTilde(*dbPath))
	certPool := client.LoadCA()
	httpClient := util.NewHttpClient(*serverUrl, &authKey, certPool)

	daemon.Setup()
	go func() {
		log.Fatal(util.ListenAndServe(*httpAddr, nil))
	}()

	for {
		runScan(musicDir, httpClient, db, *threadCount)
		if *once {
			break
		}
		time.Sleep(time.Hour * 3)
	}
}
