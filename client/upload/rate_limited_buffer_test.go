package upload

import (
	"fmt"
	"io"
	"log"
	"os"
	"sync"
	"testing"
	"time"
)

func createEmptyFile(path string, size int) {
	zerofd, err := os.Open("/dev/zero")
	if err != nil {
		log.Fatalf("Could not open /dev/zero: %s", err.Error())
	}
	defer zerofd.Close()

	file, err := os.Create(path)
	if err != nil {
		log.Fatalf("Could not open %s: %s", path, err.Error())
	}
	defer file.Close()

	log.Printf("creating empty file %s of %d bytes", path, size)
	io.CopyN(file, zerofd, int64(size))
}

func TestRateLimitedBufferSlow(t *testing.T) {
	runBandwidthTest(t, 10*1024, 2)
}

func TestRateLimitedBufferFast(t *testing.T) {
	runBandwidthTest(t, 10*1024*1024, 2)
}

func TestRateLimitedBufferMany(t *testing.T) {
	runBandwidthTest(t, 10*1024, 10)
}

func runBandwidthTest(t *testing.T, rate int, procs int) {
	n := rate

	rl := NewRateLimiter(int(n))

	bufs := []*RateLimitedBuffer{}
	for i := 0; i < procs; i++ {
		filename := fmt.Sprintf(".zerotest-%d", i)
		createEmptyFile(filename, n)
		defer os.Remove(filename)

		rlbuf, err := RateLimitedOpen(filename, rl)
		if err != nil {
			t.Fatalf("Error opening %s: %s", filename, err.Error())
		}

		bufs = append(bufs, rlbuf)
	}

	copyBuf := func(rlbuf *RateLimitedBuffer) {
		nullfd, err := os.OpenFile("/dev/null", os.O_WRONLY, 0)
		if err != nil {
			t.Fatalf("Could not open /dev/null: %s", err.Error())
		}
		log.Println("starting copy...")
		start := time.Now()
		nw, err := io.CopyN(nullfd, rlbuf, int64(n))
		elapsed := time.Since(start)
		log.Printf("elapsed time: %gs - %d bytes/sec",
			elapsed.Seconds(), int(float64(nw)/elapsed.Seconds()))
	}

	start := time.Now()

	var wg sync.WaitGroup
	for _, rlbuf := range bufs {
		wg.Add(1)
		go func(rlbuf *RateLimitedBuffer) {
			copyBuf(rlbuf)
			wg.Done()
		}(rlbuf)
	}
	log.Println("Waiting...")
	wg.Wait()

	elapsed := time.Since(start)
	bytesTransfered := procs * n
	observedRate := float64(bytesTransfered) / elapsed.Seconds()

	for _, rlbuf := range bufs {
		rlbuf.Close()
	}

	var rateOfRates float64
	if observedRate > float64(rate) {
		rateOfRates = observedRate / float64(rate)
	} else {
		rateOfRates = float64(rate) / observedRate
	}

	log.Printf("elapsed time = %gs, observed rate = %.4g bytes/s (%.4g)",
		elapsed.Seconds(), observedRate, rateOfRates)
	if rateOfRates > 1.1 {
		t.Fatalf("Observed rate deviates from expectation by more than 10%% (%.4g)", rateOfRates)
	}
}
