package upload

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"
)

var (
	testDir   = ".scannertestdir"
	testFiles = []string{
		"scannertest1",
		"scannertest2.zzz",
		"otherfile",
	}
)

func createFiles() {
	os.Mkdir(testDir, 0700)
	for _, path := range testFiles {
		fullPath := filepath.Join(testDir, path)
		ioutil.WriteFile(fullPath, []byte("data"), 0600)
	}
}

func removeFiles() {
	os.RemoveAll(testDir)
}

type testScannerCtx struct {
	scanner *FSScanner
	db      *FileDb
	dbpath  string
}

func (c *testScannerCtx) Close() {
	c.db.Close()
	os.RemoveAll(c.dbpath)
}

func newTestScannerCtx() *testScannerCtx {
	dbpath := "./.testdb"
	db := NewFileDb(dbpath)
	return &testScannerCtx{
		scanner: NewFSScanner(testDir, db, 1),
		db:      db,
		dbpath:  dbpath,
	}
}

func TestFSScanner_Walk(t *testing.T) {
	createFiles()
	defer removeFiles()

	c := newTestScannerCtx()
	defer c.Close()

	output := []*FileInfo{}
	ch := make(chan *FileInfo)
	go func() {
		err := c.scanner.Walk(ch)
		if err != nil {
			t.Fatalf("Error in Walk(): %s", err.Error())
		}
		close(ch)
	}()
	for fi := range ch {
		output = append(output, fi)
	}

	// Add one expected entry for the directory.
	expectedLen := 1 + len(testFiles)
	if len(output) != expectedLen {
		t.Errorf("Wrong number of results: %d (expected: %d)",
			len(output), expectedLen)
	}
}

func TestFSScanner_WalkExclude(t *testing.T) {
	createFiles()
	defer removeFiles()

	c := newTestScannerCtx()
	defer c.Close()

	// This should return a single file only.
	c.scanner.AddExcludePattern("*.zzz")

	output := []*FileInfo{}
	ch := make(chan *FileInfo)
	go func() {
		err := c.scanner.Walk(ch)
		if err != nil {
			t.Fatalf("Error in Walk(): %s", err.Error())
		}
		close(ch)
	}()
	for fi := range ch {
		output = append(output, fi)
	}

	// 1 (dir) + len(testfiles) - 1 (excluded).
	if len(output) != len(testFiles) {
		t.Fatalf("Wrong number of results: %d (expected: %d)",
			len(output), len(testFiles))
	}
}

func TestFSScanner_WalkInclude(t *testing.T) {
	createFiles()
	defer removeFiles()

	c := newTestScannerCtx()
	defer c.Close()

	// This should return a single file only.
	c.scanner.AddIncludePattern("*.zzz")

	output := []*FileInfo{}
	ch := make(chan *FileInfo)
	go func() {
		err := c.scanner.Walk(ch)
		if err != nil {
			t.Fatalf("Error in Walk(): %s", err.Error())
		}
		close(ch)
	}()
	for fi := range ch {
		output = append(output, fi)
	}

	// 1 value expected.
	if len(output) != 1 {
		t.Fatalf("Wrong number of results: %d (expected: %d)",
			len(output), 1)
	}

	resultBase := filepath.Base(output[0].Path)
	if resultBase != "scannertest2.zzz" {
		t.Errorf("Wrong result: %s (expected: scannertest2.zzz)", output[0].Path)
	}
}
