package upload

import (
	"os"
)

type FileInfo struct {
	Path string
	Info os.FileInfo
	Meta map[string]string
}

func NewFileInfo(path string, stat os.FileInfo) *FileInfo {
	return &FileInfo{
		Path: path,
		Info: stat,
		Meta: make(map[string]string),
	}
}
