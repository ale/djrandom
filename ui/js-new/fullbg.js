/**
 * jQuery.fullBg
 * Version 1.0
 * Copyright (c) 2010 c.bavota - http://bavotasan.com
 * Dual licensed under MIT and GPL.
 * Date: 02/23/2010
**/
(function($) {
  $.fn.fullBg = function(){
    var bgImg = $(this);		
    
    function resizeImg() {
      var imgwidth = bgImg.width();
      var imgheight = bgImg.height();

      var winwidth = $(window).width();
      var winheight = $(window).height();

      var widthratio = winwidth / imgwidth;
      var heightratio = winheight / imgheight;

      var widthdiff = heightratio * imgwidth;
      var heightdiff = widthratio * imgheight;

      var newwidth, newheight;
      if(heightdiff>winheight) {
          newwidth = winwidth;
          newheight = heightdiff;
      } else {
          newwidth = widthdiff;
          newheight = winheight;
      }
      var xoffset = (newwidth - winwidth) / 2,
        yoffset = (newheight - winheight) / 2;

      bgImg.css({
          width: newwidth+'px',
          height: newheight+'px',
          top: '-'+yoffset+'px',
          left: '-'+xoffset+'px'
      });
    } 
    resizeImg();
    $(window).resize(function() {
      resizeImg();
    });

    return this;
  };
})(jQuery)
