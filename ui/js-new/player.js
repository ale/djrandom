// Player demo with buzz.js

var pl = {};

djrandom.EventObject = function() {
  var events = {};

  this.emit = function(event) {
    if (!events[event]) return;

    var args = Array.prototype.slice.call(arguments, 1),
    callbacks = events[event];

    for (var i = 0, len = callbacks.length; i < len; i++) {
      callbacks[i].apply(null, args);
    }
  };

  this.on = function(event, fn) {
    events[event] || (events[event] = []);
    events[event].push(fn);
  };

  this.off = function(event, fn) {
    var eventsOf = events[event],
    index = eventsOf.indexOf(fn);
      
    ~index && eventsOf.splice(index, 1);
  };
};


djrandom.PlayerUI = function() {

  var state = 'paused',
      el = $('.player .button'),
      ev = new djrandom.EventObject();

  function updateui() {
    if (state == 'playing') {
      el.toggleClass('pause', true);
    } else {
      el.toggleClass('pause', false);
    }
  }

  function toggleState() {
    if (state == 'playing') {
      state = 'paused';
      ev.emit('pause');
    } else {
      state = 'playing';
      ev.emit('play');
    }
    updateui();
  }

  el.click(function() {
    toggleState();
  });

  this.SetState = function(playing) {
    if (playing) {
      state = 'playing';
    } else {
      state = 'paused';
    }
    updateui();
  };

  this.GetState = function() {
    return state;
  };

  this.on = function(event, fn) {
    ev.on(event, fn);
  };
};

djrandom.Player = function() {

  var sound = null,
      playing = false,
      ev = new djrandom.EventObject();

  // Play a new song.
  this.Play = function(song) {
    console.log('playing(' + song.id + ')...');
    var url = '/dl/' + song.audio_file.md5;
    newSound = new buzz.sound(url, {preload: true});
    ev.emit('loading', song);

    // 'next' is raised when a song ends.
    newSound.bind('ended', function(e) {
      ev.emit('next');
    });
    // 'error' is raised on errors.
    newSound.bind('error', function(e) {
      console.log('Error: ' + this.getErrorMessage());
      ev.emit('pause');
      ev.emit('error');
      this.stop();
    });
    // 'pause' is raised when no audio is being output. It is a UI signal.
    newSound.bind('ended pause', function(e) {
      // emit the pause signal to the UI
      console.log('ended/pause');
      ev.emit('pause');
    });
    // 'play' is raised when audio is playing. It is a UI signal.
    newSound.bind('playing', function(e) {
      // emit the play signal to the UI
      console.log('play');
      ev.emit('play');
    });

    // Play the new sound, possibly with a faded transition.
    ev.emit('playing', song);
    if (sound && !sound.isEnded() && !sound.isPaused()) {
      sound.unbind('ended pause');
      sound.fadeWith(newSound.play(), 200);
    } else {
      newSound.play().fadeIn(200);
    }

    sound = newSound;
  };

  // Pauses *and* unpauses the currently playing sound.
  this.Pause = function() {
    if (sound) {
      sound.togglePlay();
    }
  };

  // Stop the current sound and destroy it.
  this.Stop = function() {
    if (sound) {
      console.log('stopping...');
      sound.stop();
      sound = null;
    }
  };

  this.on = function(event, fn) {
    ev.on(event, fn);
  };

};

// Sort an array using a key function.
// The key function is only called once for every input item (so you
// can do things like return a random number to obtain a randomly
// ordered list, etc).
function sortBy(list, keyfn) {
    var keys = [];
    for (var i = 0; i < list.length; i++) {
        keys.push({index: i, key: keyfn(list[i])});
    }
    keys.sort(function(a, b) {
        return (a.key>b.key) ? 1 : (a.key<b.key) ? -1 : 0;
    });
    var result = [];
    for (var i = 0; i < keys.length; i++) {
        result.push(list[keys[i].index]);
    }
    return result;
}

djrandom.Playlist = function(api, playlistId) {

  var songTemplate = _.template($('#tpl-song').html()),
    playlistDiv = $('#' + playlistId),
    songList = [],              // list of song objects
    songHistory = [],           // list of song IDs
    maxSongHistoryLen = 3,
    curSong = null,             // song ID
    dedupMeta = true,
    ev = new djrandom.EventObject();

  function isInSongList(song) {
    for (var i = 0; i < songList.length; i++) {
      if (songList[i].id == song.id || (dedupMeta && songList[i].meta.artist == song.meta.artist && songList[i].meta.title == song.meta.title)) {
        return true;
      }
    }
    return false;
  }

  function getSongAfter(songId) {
    for (var i = 0; i < songList.length - 1; i++) {
      if (songList[i].id == songId) {
        return songList[i + 1];
      }
    }
    return null;
  }

  function clearSongList(keepId) {
    if (keepId) { console.log('Playlist: clear (except for ' + keepId + ')'); }
    else { console.log('Playlist: clear'); }

    var newSongList = [];
    for (var i = 0; i < songList.length; i++) {
      var song = songList[i];
      if (song.id == keepId) {
        newSongList.push(song);
      } else {
        $('#song_' + song.id).hide().remove();
      }
    }
    songList = newSongList;
    ev.emit('update');
  }

  function deleteFromSongList(songId) {
    console.log('Playlist: deleting song ' + songId);
    var newSongList = [];
    $.each(songList, function(idx, song) {
      if (song.id != songId) {
        newSongList.push(song);
      }
    });
    songList = newSongList;
    $('#song_' + songId).hide().remove();
    ev.emit('update');
  }

  function addToSongHistory(songId) {
    songHistory.push(songId);
    if (songHistory.length > maxSongHistoryLen) {
      songHistory.splice(0, 1);
    }
  }

  function doPlay(song) {
    ev.emit('play', song);
    curSong = song.id;
    $('.song').toggleClass('playing', false);
    $('#song_' + song.id).toggleClass('playing', true);
  }

  function moreLikeThis(songId) {
    clearSongList(songId);
    api.Suggest([songId], 20, function(moreSongs) {
      if (moreSongs && moreSongs.length > 0) {
        for (var i = 0; i < moreSongs.length; i++) {
          addSong(moreSongs[i]);
        }
      } else {
        console.log('No suggestions could be found!');
      }
    }, function() {
      console.log('Error retrieving suggestions!');
    });    
  }

  function renderSong(song) {
    var songHtml = $(songTemplate({song: song}));
    songHtml.find('.title').click(function() {
      console.log('Playlist: play ' + song.id);
      doPlay(song);
    });
    songHtml.find('.song-remove').click(function() {
      deleteFromSongList(song.id);
    });
    songHtml.find('.song-more-like-this').click(function() {
      moreLikeThis(song.id);
    });
    return songHtml;
  }

  function addSong(song) {
    if (isInSongList(song)) {
      console.log('Playlist: song ' + song.id + ' already present');
      return;
    }
    // Verify that the incoming 'song' object has the 'audio_file'
    // attribute set, so that we know which file to play.
    if (!song.audio_file) {
      console.log('song ' + song.id + ' has no good audio file');
      return;
    }
    console.log('Playlist: adding song ' + song.id);
    songList.push(song);
    var songHtml = renderSong(song);
    songHtml.appendTo(playlistDiv);
    ev.emit('update');
  }

  // Add a song to the playlist. 
  this.AddSong = function(song) {
    addSong(song);
  };

  // Remove a song from the playlist.
  this.DeleteSong = function(songId) {
    deleteFromSongList(songId);
  };

  // Remove all songs from the playlist.
  this.Clear = function() {
    clearSongList();
  };

  // Play the specified song (warning: doesn't check that the song is
  // in the playlist).
  this.Play = function(song) {
    doPlay(song);
  };

  // Play the next song (based on the currently playing, or last
  // played, one).
  this.PlayNextSong = function() {
    if (curSong) {
      // Current song has played succesfully, record this fact.
      if (songHistory[songHistory.length-1] != curSong) {
        addToSongHistory(curSong);
        api.AddToPlayLog(songHistory);
      }
    } else {
      // Just start from the first song.
      if (songList.length > 0) {
        newSong = songList[0];
      }
    }

    // Play the next song, if any.
    var newSong = getSongAfter(curSong);
    if (newSong) {
      console.log('Next song: ' + newSong.id);
      doPlay(newSong);
    } else {
      console.log('No more songs...');
      ev.emit('end_playlist');
    }
  };

  // Extend the current playlist with random suggestions.
  this.ExtendWithSuggestions = function(callback) {
    api.Suggest(songHistory, 10, function(moreSongs) {
      for (var i = 0; i < moreSongs.length; i++) {
        addSong(moreSongs[i]);
      }
      callback();
    }, function() {
      console.log('No suggestions could be found!');
    });
  };

  function facetNormalizeStr(s) {
      return s.toLowerCase().replace(/\s+/g, '');
  }

  // Build facets for the current playlist. The results are aggregated
  // song counts organized in a hierarchical fashion, which will be
  // rendered as a multi-level drop-down menu.
  this.GetFacets = function() {
    var facets = {artists: {}, genres: {}};
    $.each(songList, function(idx, song) {
      if (song.meta.genre) {
        var genre = facetNormalizeStr(song.meta.genre);
        if (!facets.genres[genre]) {
            facets.genres[genre] = {count: 1}
        } else {
            facets.genres[genre].count++;
        }
      }
      if (song.meta.artist) {
        var artist = facetNormalizeStr(song.meta.artist);
        if (!facets.artists[artist]) {
          facets.artists[artist] = {count: 1, albums: {}};
        } else {
          facets.artists[artist].count++;
        }

        if (song.meta.album) {
          var album = facetNormalizeStr(song.meta.album);
          if (!facets.artists[artist].albums[album]) {
            facets.artists[artist].albums[album] = {count: 1};
          } else {
            facets.artists[artist].albums[album].count++;
          }
        }
      }
    });
    return facets;
  };

  // Apply an attribute-level filter to the playlist. The filter will
  // be matched for equality against each song.
  this.ApplyFilter = function(filterData) {
    var newSongList = [];
    $.each(songList, function(idx, song) {
      var ok = true;
      for (var attr in filterData) {
          if (facetNormalizeStr(song.meta[attr]) != filterData[attr]) {
              ok = false;
              $('#song_' + song.id).hide().remove();
              break;
          }
      }
      if (ok) {
          newSongList.push(song);
      }
    });
    songList = newSongList;
    ev.emit('update');
  };

  // Sort the playlist according to a sort function, which should
  // return a sorting key when passed a song as argument.
  this.Sort = function(sortFn) {
    newSongList = sortBy(songList, sortFn);
    // Clear and recreate the playlist.
    clearSongList();
    for (var i = 0; i < newSongList.length; i++) {
      addSong(newSongList[i]);
    }
    // ev.emit('update'); -- not really an update.
  };

  this.on = function(event, fn) {
    ev.on(event, fn);
  };
};
