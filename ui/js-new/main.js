// Miscellaneous useful functions.

var delay = (function(){
  var timer = 0;
  return function(callback, ms){
    clearTimeout (timer);
    timer = setTimeout(callback, ms);
  };
})();

djrandom.windowResize = function() {
  delay(function() {
    // Resize the playlist view.
    // var plv = $('#playlist_view');
    // plv.height($(window).height() - 16 - plv.offset().top);

    // Recenter the player.
    var ploff = $(window).height() / 2 - 100;
    $('#player').css('margin-top', '' + ploff + 'px');
  }, 100);
};

djrandom.generateFacetDropdownMenus = function() {
    var dst = $('#dFilterMenu');
    var out = '';
    var filters = [];
    var dfcount = 0;

    // Create the menu entries directly as HTML for the Bootstrap
    // drop-downs.
    function addFilter(filterData) {
        var result = dfcount;
        filters[dfcount] = filterData;
        dfcount++;
        return result;
    }
            
    function addElem(name, count, filterData) {
        return ('<li><a role="menuitem" href="#" data-filter="' 
                + addFilter(filterData) + '">' + name 
                + ' (' + count + ')</a></li>');
    }

    var f = djrandom.playlist.GetFacets();

    out += '<li><small>By artist:</small></li>';
    $.each(f.artists, function(artist, artistdata) {
        out += '<li class="dropdown-submenu">';
        out += '<a href="#" data-filter="' + addFilter({artist: artist}) + '">';
        out += artist + ' (' + artistdata.count + ')';
        out += '<ul class="dropdown-menu">';
        $.each(artistdata.albums, function(album, albumdata) {
            out += addElem(album, albumdata.count, {artist: artist, album: album});
        });
        out += '</ul></li>';
    });

    out += '<li class="divider"></li>';
    out += '<li><small>By genre:</small></li>';
    $.each(f.genres, function(genre, genredata) {
        out += addElem(genre, genredata.count, {genre: genre});
    });

    dst.html(out);

    dst.find('a').click(function() {
        var filterId = parseInt($(this).attr('data-filter'));
        var filterData = filters[filterId];
        console.log('resulting filter: ' + JSON.stringify(filterData));
        djrandom.playlist.ApplyFilter(filterData);
    });
};

// Initialize the UI.
$(document).ready(function() {
    djrandom.api = new djrandom.API('/');
    djrandom.albumart = new djrandom.AlbumArt(djrandom.api);
    djrandom.player = new djrandom.Player();
    djrandom.player_ui = new djrandom.PlayerUI();
    djrandom.playlist = new djrandom.Playlist(djrandom.api, 'playlist');
    djrandom.search = new djrandom.Searcher(djrandom.api, 'searchForm');

    // Connect the Player and PlayerUI so that the play/pause states
    // are synchronized.
    djrandom.player.on('play', function() {
      djrandom.player_ui.SetState(true);
    });
    djrandom.player.on('pause', function() {
      djrandom.player_ui.SetState(false);
    });
    djrandom.player_ui.on('play', function() {
      djrandom.player.Pause();
    });
    djrandom.player_ui.on('pause', function() {
      djrandom.player.Pause();
    });

    // Show the album art once a song starts playing.
    djrandom.player.on('playing', function(song) {
      console.log('now playing "' + song.meta.title + '"');
      djrandom.albumart.Set(song);
    });

    // If the user clicks on a song, we should play it.
    djrandom.playlist.on('play', function(song) {
      djrandom.player.Play(song);
    });

    // When a song is over (either because it was fully played, or
    // because there were errors), skip to the following one in the
    // playlist.
    djrandom.player.on('next', function() {
      djrandom.playlist.PlayNextSong();
    });
    djrandom.player.on('error', function() {
      console.log('error, skipping to next song...');
      djrandom.playlist.PlayNextSong();
    });

    // When we reach the end of the playlist, extend it with random
    // suggestions.
    djrandom.playlist.on('end_playlist', function() {
      console.log('extending playlist with random songs...');
      djrandom.playlist.ExtendWithSuggestions(
          djrandom.playlist.PlayNextSong);
    });

    // When search results are ready, load them into the playlist
    // (replacing the previous contents).
    djrandom.search.on('search', function(results) {
      djrandom.playlist.Clear();  
      $.each(results, function(idx, song) {
        djrandom.playlist.AddSong(song);
      });
      // Ensure that the play button is visible.
      $('#player').show();
    });

    // Set up autocompletion on the search box.
    $('#searchField').focus();
    $('#searchField').typeahead({
        source: djrandom.api.Autocomplete,
        items: 12,
        minLength: 2
    });

    // Regenerate the filter menu when the playlist is updated.
    djrandom.playlist.on('update', djrandom.generateFacetDropdownMenus);

    // Set click handlers on the Sort drop-down menu.
    $('#dSortMenuRandom').click(function() {
        djrandom.playlist.Sort(function(song) {
            return Math.random();
        });
    });
    $('#dSortMenuAlbum').click(function() {
        djrandom.playlist.Sort(function(song) {
            return [song.meta.artist, song.meta.album, song.meta.track_num,
                    song.meta.title];
        });
    });

    // Set up other UI events and various miscellanea.
    djrandom.windowResize();
    $(window).resize(djrandom.windowResize);
});

