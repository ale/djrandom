
djrandom.API = function(baseUrl) {

  // Perform the AJAX request to the API server.
  function doRequest(method, url, data, callback, errback) {
    $.ajax({
      url: baseUrl + url,
      type: method,
      data: data,
      dataType: 'json',
      error: function(jqxhr, textStatus, errorThrown) {
        errback(textStatus + ' ' + errorThrown);
      },
      success: function(data, textStatus, jqxhr) {
        callback(data);
      }
    });
  }

  // Select audio files that are appropriate for this interface
  // (specifically, mp3). This sets the 'song.audio_file' attribute
  // for later access.
  //
  // TODO: improve the selection by sorting by bitrate.
  function pickAudioFile(song) {
    for (var i = 0; i < song.files.length; i++) {
      var af = song.files[i];
      if (af.mime_type == "audio/mpeg") {
        song.audio_file = af;
        return af;
      }
    }
    return null;
  }

  // Filter search results by calling pickAudioFile().
  function pickSongsWithFiles(songs) {
    var result = [];
    for (var i = 0; i < songs.length; i++) {
      var s = songs[i];
      if (pickAudioFile(s)) {
        result.push(s);
      }
    }
    return result;
  }

  this.Search = function(query, callback, errback) {
    doRequest('POST', 'api/search', {q: query}, function(data) {
      callback(pickSongsWithFiles(data.results));
    }, errback);
  };

  this.Autocomplete = function(query, process) {
    doRequest('GET', 'api/autocomplete/artist', {prefix: query}, function(data) {
      process(data.entries);
    }, function() {
      process([]);
    });
  };

  this.Suggest = function(curSongs, numResults, callback, errback) {
    var data = JSON.stringify({current_songs: curSongs, num_results: numResults});
    doRequest('POST', 'api/suggest', data, function(response) {
      callback(pickSongsWithFiles(response.results));
    }, errback);
  };

  this.AddToPlayLog = function(songs) {
    var data = JSON.stringify({Songs: songs});
    doRequest('POST', 'api/add_playlog', data, function(){}, function(){});
  };

  this.GetAlbumArtUrl = function(song) {
    return baseUrl + 'api/album_art?artist=' + encodeURIComponent(song.meta.artist) + '&album=' + encodeURIComponent(song.meta.album);
  };

};
