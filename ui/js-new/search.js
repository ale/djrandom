
djrandom.Searcher = function(api, formId) {
  var form = $('#' + formId);
  var searchField = form.find('.search-query');
  var searchMsg = $('#searchMessage');  
  var ev = new djrandom.EventObject();

  function doSearch(query) {
    console.log('Search: ' + query);
    searchField.toggleClass('loading', true);
    api.Search(query, function(results) {
      searchField.toggleClass('loading', false);

      // Show results
      if (results.length > 0) {
        searchMsg.hide();
        ev.emit('search', results);
      } else {
        searchMsg.text('No results found.').show();
      }
    }, function(error) {
      searchField.toggleClass('loading', false);
      searchMsg.text(error).show();
      console.log('Search error: ' + error);
    });
  };

  console.log('Installing submit handler...');
  form.submit(function(e) {
    var qstr = searchField.val();
    if (qstr) {
      doSearch(qstr);
    }
    return false;
  });

  this.Search = function(query) {
    doSearch(query);
  };

  this.on = function(event, fn) {
    ev.on(event, fn);
  };
};
