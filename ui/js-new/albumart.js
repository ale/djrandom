
djrandom.AlbumArt = function(api) {
    // Initialize load hook on the <img> object.
    $('#albumart-image').load(function() {
        $('#albumart-image').fullBg().show();
    });

    this.Set = function(song) {
        var url = api.GetAlbumArtUrl(song);
        $('#albumart-image').attr('src', url);
    };
};
