package partition

import (
	"net/rpc"
	"net/url"
	"sort"
	"strings"
	"time"
)

type PartitionStatus struct {
	Name     string
	Target   string
	Version  string
	Stamp    time.Time
	Counters map[string]int
	Err      error
}

func (s PartitionStatus) Total() int {
	var total int
	for _, v := range s.Counters {
		total += v
	}
	return total
}

func (s PartitionStatus) Partitions() int {
	return len(s.Counters)
}

type statusMap map[string]*PartitionStatus

func (m statusMap) AllVersions() []string {
	tmp := make(map[string]struct{})
	for _, s := range m {
		if s.Err == nil {
			tmp[s.Version] = struct{}{}
		}
	}
	var versions []string
	for v := range tmp {
		versions = append(versions, v)
	}
	return versions
}

type partitionStatusList []*PartitionStatus

func (l partitionStatusList) Len() int      { return len(l) }
func (l partitionStatusList) Swap(i, j int) { l[i], l[j] = l[j], l[i] }
func (l partitionStatusList) Less(i, j int) bool {
	if l[i].Version > l[j].Version {
		return true
	}
	if l[i].Stamp.After(l[j].Stamp) {
		return true
	}
	return false
}

func (m statusMap) Sort() []*PartitionStatus {
	var out []*PartitionStatus
	for _, v := range m {
		out = append(out, v)
	}
	sort.Sort(partitionStatusList(out))
	return out
}

func target2addr(target string) string {
	if strings.HasPrefix(target, "http://") || strings.HasPrefix(target, "https://") {
		if u, err := url.Parse(target); err == nil {
			return u.Host
		}
	}
	return target
}

func getPartitionStatus(name, target string) *PartitionStatus {
	client, err := rpc.DialHTTP("tcp", target2addr(target))
	if err != nil {
		return &PartitionStatus{Name: name, Target: target, Err: err}
	}
	defer client.Close()

	req := &GetVersionRequest{Name: name}
	var resp GetVersionResponse
	if err := client.Call("Partition.GetVersion", req, &resp); err != nil {
		return &PartitionStatus{Name: name, Target: target, Err: err}
	}
	var sreq struct{}
	var sresp PartitionStatsResponse
	if err := client.Call("PartitionedService.GetStats", &sreq, &sresp); err != nil {
		return &PartitionStatus{Name: name, Target: target, Err: err}
	}

	return &PartitionStatus{
		Name:     name,
		Target:   target,
		Version:  resp.Version,
		Stamp:    resp.Stamp,
		Counters: sresp.Counters,
	}
}

func getGlobalPartitionStatus(pmap *PartitionMap, name string) statusMap {
	c := make(chan *PartitionStatus)
	defer close(c)

	targets := pmap.AllTargets()
	for _, t := range targets {
		go func(t string) {
			c <- getPartitionStatus(name, t)
		}(t)
	}

	status := make(statusMap)
	for i := 0; i < len(targets); i++ {
		result := <-c
		status[result.Target] = result
	}
	return status
}
