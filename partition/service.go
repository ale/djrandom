package partition

import (
	"flag"
	"log"
	"net/rpc"
	"time"

	"git.autistici.org/ale/djrandom/util/instrumentation"
)

var (
	numRebalanceWorkers = flag.Int("rebalance-workers", 3, "Number of rebalance workers to run")
)

type PartitionedServiceBase struct {
	// Partition map (used by the client).  Self is our target ID.
	Pmap       *PartitionMap
	SelfTarget string

	// Partition counters.
	Pcounters map[string]int
}

func (pb *PartitionedServiceBase) GetPartitionMap() *PartitionMap {
	return pb.Pmap
}

func (pb *PartitionedServiceBase) GetPartitionCounters() map[string]int {
	return pb.Pcounters
}

func (pb *PartitionedServiceBase) SetPartitionCounters(c map[string]int) {
	pb.Pcounters = c
}

func (pb *PartitionedServiceBase) GetSelfTarget() string {
	return pb.SelfTarget
}

// Interface of a service that supports partitioning.  You should
// provide your own Scan() and Move() methods, but it is possible to
// extend PartitionedServiceBase for the boilerplate methods.
type PartitionedService interface {
	GetPartitionMap() *PartitionMap
	GetPartitionCounters() map[string]int
	SetPartitionCounters(map[string]int)
	GetSelfTarget() string
	Scan() <-chan string
	Move(key string) error
}

type partitionedServiceWrapper struct {
	PartitionedService
	moveCh chan string

	counters *instrumentation.Counter
	gauges   *instrumentation.Gauge
}

func newPartitionedServiceWrapper(s PartitionedService) *partitionedServiceWrapper {
	return &partitionedServiceWrapper{
		PartitionedService: s,
		moveCh:             make(chan string, 10),
		counters:           instrumentation.NewCounter("partition", 1),
		gauges:             instrumentation.NewGauge("partition", 1),
	}
}

func (svc *partitionedServiceWrapper) mover() {
	me := svc.GetSelfTarget()
	for key := range svc.moveCh {
		log.Printf("Rebalance(%s): moving %s", me, key)
		if err := svc.Move(key); err != nil {
			svc.counters.IncVar("rebalance.errors", 1)
			log.Printf("Rebalance(%s): Error: key %s: %s", me, key, err)
		} else {
			svc.counters.IncVar("rebalance.ok", 1)
		}
	}
}

func (svc *partitionedServiceWrapper) rebalance() {
	pmap := svc.GetPartitionMap()
	me := svc.GetSelfTarget()
	tot := 0
	counters := make(map[string]int)
	var toMove []string

	// Scan the local service and build a list of out-of-place items.
	for key := range svc.Scan() {
		tot++
		p := pmap.GetPartition(key)

		// Increment the partition counter.
		c, ok := counters[p.EndKey]
		if !ok {
			c = 0
		}
		counters[p.EndKey] = c + 1

		// Rebalance this object if it doesn't belong to us.
		t := p.Target
		if t != me {
			log.Printf("Rebalance(%s): must move %s to %s", me, key, t)
			toMove = append(toMove, key)
			//svc.moveCh <- key
		}
	}

	// Update partition counters.
	log.Printf("PartitionedService(%s): partition stats = %v", me, counters)
	svc.SetPartitionCounters(counters)

	// Start a loop feeding the items to be rebalanced into
	// moveCh. Update statistics once in a while.
	for i, key := range toMove {
		if (i % 20) == 0 {
			svc.gauges.SetVar("rebalance.queue", int64(len(toMove)-i))
			log.Printf("Rebalance(%s): in progress, items (total/moved/to_move): %d/%d/%d", me, tot, i, len(toMove))
		}
		svc.moveCh <- key
	}
}

type partitionStatsServer struct {
	svc PartitionedService
}

type PartitionStatsResponse struct {
	Counters map[string]int
}

func (p *partitionStatsServer) GetStats(req *struct{}, resp *PartitionStatsResponse) error {
	resp.Counters = p.svc.GetPartitionCounters()
	return nil
}

func RegisterStatsService(svc PartitionedService) {
	rpc.RegisterName("PartitionedService", &partitionStatsServer{svc})
}

func RunRebalancer(svc PartitionedService, period time.Duration) {
	swrap := newPartitionedServiceWrapper(svc)

	// Start the background rebalance workers.
	for i := 0; i <= *numRebalanceWorkers; i++ {
		go swrap.mover()
	}

	// Run the main rebalance/sleep loop.
	for {
		swrap.rebalance()
		time.Sleep(period)
	}
}
