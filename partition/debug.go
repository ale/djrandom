package partition

import (
	"errors"
	"fmt"
	"html/template"
	"net/http"
	"net/rpc"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ale/djrandom/util/status"
)

const debugDumpPartitionText = `<html>
<head>
<title>Partition Map: {{.Name}}</title>
</head>
<body>
<h1>Partition "{{.Name}}" (raw)</h1>
<table>
 <thead>
  <tr>
   <th>EndKey</th><th>Target</th>
  </tr>
 </thead>
 <tbody>
  {{range .Pmap.P}}
  <tr>
   <td><tt>{{.EndKey | printf "%v" }}</tt></td>
   <td><a href="{{.Target | httpize}}">{{.Target}}</a></td>
  </tr>
  {{end}}
 </tbody>
</table>
<p>Version: <tt>{{.Pmap.GetVersion}}</tt></p>
<p><a href="/debug/part/{{.Name}}">Back to overview</a></p>
</body>
</html>`

const debugShowPartitionText = `<html>
<head>
<title>Partition Map: {{.Name}}</title>
</head>
<body>
<h1>Partition "{{.Name}}" (overview)</h1>
<p>
Versions:
{{range .GlobalStatus.AllVersions}}{{.}} {{end}}
</p>
<table>
 <thead>
  <tr>
   <th>Host</th>
   <th>Version</th>
   <th>Stamp</th>
   <th>#Part</th>
   <th>#Items</th>
  </tr>
 </thead>
 <tbody>
 {{range $s := .GlobalStatus.Sort}}
  <tr>
   <td><a href="{{$s.Target | httpize}}/debug/part/{{.Name}}">{{$s.Target}}</a></td>
   {{if $s.Err}}
   <td colspan="4">{{$s.Err}}</td>
   {{else}}
   <td>{{$s.Version}}</td>
   <td>{{$s.Stamp}}</td>
   <td>{{$s.Partitions}}</td>
   <td>{{$s.Total}}</td>
   {{end}}
  </tr>
 {{end}}
 </tbody>
</table>
<p><a href="/debug/part/raw/{{.Name}}">Raw partition map</a></p>
</body>
</html>`

const debugListPartitionsText = `<html>
<head>
<title>Partition Maps</title>
</head>
<body>
<h1>Partition Maps</h1>
<p>
  {{range .}}
  <a href="/debug/part/{{.}}">{{.}}</a><br>
  {{end}}
</p>
</body>
</html>`

func httpize(s string) string {
	if !strings.HasPrefix(s, "http") {
		return fmt.Sprintf("http://%s", s)
	}
	return s
}

var (
	debugDumpPartitionTpl  = template.Must(template.New("dump_partition").Funcs(template.FuncMap{"httpize": httpize}).Parse(debugDumpPartitionText))
	debugShowPartitionTpl  = template.Must(template.New("show_partition").Funcs(template.FuncMap{"httpize": httpize}).Parse(debugShowPartitionText))
	debugListPartitionsTpl = template.Must(template.New("list_partitions").Parse(debugListPartitionsText))

	knownPartitionsMap     = make(map[string]*PartitionMap)
	knownPartitionsMapLock sync.Mutex
)

func Register(pmap *PartitionMap, name string) {
	knownPartitionsMapLock.Lock()
	defer knownPartitionsMapLock.Unlock()
	if _, ok := knownPartitionsMap[name]; ok {
		panic(fmt.Sprintf("Partition map '%s' registered more than once", name))
	}
	knownPartitionsMap[name] = pmap
}

type debugRPC struct{}

type GetVersionRequest struct {
	Name string
}

type GetVersionResponse struct {
	Name    string
	Version string
	Stamp   time.Time
}

func (server debugRPC) GetVersion(req *GetVersionRequest, resp *GetVersionResponse) error {
	knownPartitionsMapLock.Lock()
	pmap, ok := knownPartitionsMap[req.Name]
	knownPartitionsMapLock.Unlock()
	if !ok {
		return errors.New("Not found")
	}
	resp.Name = req.Name
	resp.Version = pmap.GetVersion()
	resp.Stamp = pmap.GetStamp()
	return nil
}

func debugHTTPDumpPartition(w http.ResponseWriter, r *http.Request) {
	name := r.URL.Path

	if name == "" {
		http.Error(w, "No such partition", http.StatusNotFound)
		return
	}

	knownPartitionsMapLock.Lock()
	pmap, ok := knownPartitionsMap[name]
	knownPartitionsMapLock.Unlock()
	if !ok {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	ctx := struct {
		Pmap *PartitionMap
		Name string
	}{pmap, name}
	if err := debugDumpPartitionTpl.Execute(w, &ctx); err != nil {
		fmt.Fprintf(w, "debug: error executing template: %v", err)
	}
}

func debugHTTPShowPartition(w http.ResponseWriter, name string) {
	if name == "" {
		http.Error(w, "No such partition", http.StatusNotFound)
		return
	}

	knownPartitionsMapLock.Lock()
	pmap, ok := knownPartitionsMap[name]
	knownPartitionsMapLock.Unlock()
	if !ok {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	status := getGlobalPartitionStatus(pmap, name)

	ctx := struct {
		Name         string
		Pmap         *PartitionMap
		GlobalStatus statusMap
	}{name, pmap, status}
	if err := debugShowPartitionTpl.Execute(w, &ctx); err != nil {
		fmt.Fprintf(w, "debug: error executing template: %v", err)
	}
}

func debugHTTPMain(w http.ResponseWriter, r *http.Request) {
	name := strings.TrimPrefix(r.URL.Path, "/")

	var err error
	if name == "" {
		pmapList := make([]string, 0, len(knownPartitionsMap))
		knownPartitionsMapLock.Lock()
		for name, _ := range knownPartitionsMap {
			pmapList = append(pmapList, name)
		}
		knownPartitionsMapLock.Unlock()
		if err = debugListPartitionsTpl.Execute(w, pmapList); err != nil {
			fmt.Fprintf(w, "debug: error executing template: %v", err)
		}
	} else {
		debugHTTPShowPartition(w, name)
	}
}

func init() {
	mux := http.NewServeMux()
	mux.Handle("/raw/", http.StripPrefix("/raw/", http.HandlerFunc(debugHTTPDumpPartition)))
	mux.Handle("/", http.HandlerFunc(debugHTTPMain))

	http.Handle("/debug/part/", http.StripPrefix("/debug/part", mux))
	rpc.RegisterName("Partition", &debugRPC{})
	status.RegisterDebugEndpoint("/debug/part/")
}
