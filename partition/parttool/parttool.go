package main

import (
	"encoding/json"
	"flag"
	"log"
	"net/rpc"
	"os"
	"strings"

	"git.autistici.org/ale/djrandom/partition"
	"git.autistici.org/ale/djrandom/util/toolcli"
)

var (
	partFile = flag.String("file", "part.json", "Partition map file")
	nParts   = flag.Int("parts", 1, "Number of partitions to create")
	nBits    = flag.Uint("bits", 128, "Key bit size")
)

func dehttpify(s string) string {
	if strings.HasPrefix(s, "http://") {
		return s[7:]
	}
	return s
}

func findPartition(pmap *partition.PartitionMap, endKey string) *partition.Partition {
	for idx, p := range pmap.P {
		if p.EndKey == endKey {
			return &(pmap.P[idx])
		}
	}
	return nil
}

func movePartition(pmap *partition.PartitionMap, endKey, target string) {
	part := findPartition(pmap, endKey)
	if part == nil {
		log.Fatalf("No partition with end key = '%s'", endKey)
	}
	// Change the target in-place.
	part.Target = target
}

func collectStats(pmap *partition.PartitionMap) {
	c := make(chan map[string]int)

	// Run GetStats RPC calls in parallel to all targets.
	targets := pmap.AllTargets()
	for _, target := range targets {
		go func(target string) {
			client, err := rpc.DialHTTP("tcp", target)
			if err != nil {
				log.Printf("Error from %s: %s", target, err)
				c <- nil
				return
			}
			defer client.Close()
			req := struct{}{}
			var resp partition.PartitionStatsResponse
			if err = client.Call("PartitionedService.GetStats", &req, &resp); err != nil {
				log.Printf("RPC Error from %s: %s", target, err)
				c <- nil
				return
			}
			c <- resp.Counters
		}(dehttpify(target))
	}

	// Merge all results
	globalCounters := make(map[string]int)
	for _, _ = range targets {
		counters := <-c
		if counters == nil {
			continue
		}
		for k, v := range counters {
			gv, ok := globalCounters[k]
			if !ok {
				gv = 0
			}
			globalCounters[k] = gv + v
		}
	}

	// Output stats in JSON format.
	json.NewEncoder(os.Stdout).Encode(globalCounters)
}

func runCreate(args []string) {
	p := partition.NewHexEncodedRandomPartitionMap(*nParts, *nBits, args)
	p.Save(*partFile)
}

func runStats(args []string) {
	p := partition.MustLoadPartitionMap(*partFile)
	collectStats(p)
}

func runMove(args []string) {
	p := partition.MustLoadPartitionMap(*partFile)
	movePartition(p, args[0], args[1])
	p.Save(*partFile)
}

func main() {
	commands := toolcli.CommandList{
		toolcli.Command{
			"create", "<target>...",
			"Create a new partition map with the specified targets.",
			runCreate,
			1, -1,
		},
		toolcli.Command{
			"stats", "",
			"Show live partition statistics.",
			runStats,
			0, 0,
		},
		toolcli.Command{
			"move", "<endkey> <target>",
			"Move a partition to a specific target.",
			runMove,
			2, 2,
		},
	}
	toolcli.Run(commands)
}
