package partition

import (
	"fmt"
	"math/rand"
	"os"
	"testing"
)

func TestPartitionMap_Simple(t *testing.T) {
	target := "target"
	p := NewSimplePartitionMap(target)
	result := p.GetTarget("aaa")
	if result != target {
		t.Fatalf("Unexpected GetTarget() result: %s", result)
	}
}

func TestPartitionMap_Random(t *testing.T) {
	targets := []string{"t1", "t2", "t3"}
	notInTargets := func(s string) bool {
		for _, t := range targets {
			if t == s {
				return false
			}
		}
		return true
	}

	p := NewHexEncodedRandomPartitionMap(100, 32, targets)
	for i := 0; i < 10000; i++ {
		k := fmt.Sprintf("%08x", rand.Int31())
		target := p.GetTarget(k)
		if notInTargets(target) {
			t.Errorf("GetTarget(%s) returned %s", k, target)
		}
	}
}

func TestPartition_Save(t *testing.T) {
	path := ".testpartition.json"
	defer os.Remove(path)

	target := "target"
	p := NewSimplePartitionMap(target)
	err := p.Save(path)
	if err != nil {
		t.Fatalf("Save() error: %s", err.Error())
	}

	p2, err := LoadPartitionMap(path)
	if err != nil {
		t.Fatalf("Load() error: %s", err.Error())
	}

	result := p2.GetTarget("aaa")
	if result != target {
		t.Fatalf("Unexpected GetTarget() result: %s", result)
	}
}
