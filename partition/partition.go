package partition

import (
	"crypto/md5"
	"encoding/hex"
	"encoding/json"
	"io"
	"log"
	"math/big"
	"math/rand"
	"os"
	"sort"
	"time"
)

type Partition struct {
	EndKey string
	Target string
}

type partitionList []Partition

func (pl partitionList) Len() int           { return len(pl) }
func (pl partitionList) Swap(i, j int)      { pl[i], pl[j] = pl[j], pl[i] }
func (pl partitionList) Less(i, j int) bool { return pl[i].EndKey < pl[j].EndKey }

type PartitionMap struct {
	P       partitionList
	version string
	stamp   time.Time
}

// GetVersion lazily computes a fingerprint of the partition map.
// This can be useful to verify that all instances of a program are
// using the same partition map.
func (pmap *PartitionMap) GetVersion() string {
	if pmap.version == "" {
		h := md5.New()
		for _, p := range pmap.P {
			io.WriteString(h, p.EndKey)
			io.WriteString(h, p.Target)
		}
		pmap.version = hex.EncodeToString(h.Sum(nil))
	}
	return pmap.version
}

func (pmap *PartitionMap) GetStamp() time.Time {
	return pmap.stamp
}

// GetPartition returns the Partition for a key.
func (pmap *PartitionMap) GetPartition(key string) Partition {
	// Run a binary search to find the partition.
	i := sort.Search(len(pmap.P), func(i int) bool { return pmap.P[i].EndKey > key })
	if i >= len(pmap.P) {
		// Key is out of range of the last partition. Account to
		// the last available partition anyway.
		i = len(pmap.P) - 1
	}

	return pmap.P[i]
}

// GetTarget returns a target for the specified key.
func (pmap *PartitionMap) GetTarget(key string) string {
	return pmap.GetPartition(key).Target
}

// GetEndKey returns the end key for the partition containing a key.
func (pmap *PartitionMap) GetEndKey(key string) string {
	return pmap.GetPartition(key).EndKey
}

// AllTargets returns a list of unique targets.
func (pmap *PartitionMap) AllTargets() []string {
	tmp := make(map[string]struct{})
	for _, p := range pmap.P {
		tmp[p.Target] = struct{}{}
	}
	result := make([]string, 0, len(tmp))
	for t, _ := range tmp {
		result = append(result, t)
	}
	return result
}

// Save stores the partition map in a file, in JSON format.
func (pmap *PartitionMap) Save(path string) error {
	file, err := os.Create(path)
	if err != nil {
		return err
	}
	defer file.Close()
	return json.NewEncoder(file).Encode(pmap)
}

func NewPartitionMap() *PartitionMap {
	return &PartitionMap{
		P:     []Partition{},
		stamp: time.Now(),
	}
}

func NewSimplePartitionMap(target string) *PartitionMap {
	pmap := NewPartitionMap()
	pmap.P = append(pmap.P, Partition{EndKey: "z", Target: target})
	return pmap
}

func LoadPartitionMap(path string) (*PartitionMap, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	var pmap PartitionMap
	if err = json.NewDecoder(file).Decode(&pmap); err != nil {
		return nil, err
	}

	stat, err := file.Stat()
	if err != nil {
		return nil, err
	}
	pmap.stamp = stat.ModTime()

	sort.Sort(pmap.P)
	return &pmap, nil
}

func MustLoadPartitionMap(path string) *PartitionMap {
	pmap, err := LoadPartitionMap(path)
	if err != nil {
		log.Fatalf("Could not load partition map from %s: %s", path, err)
	}
	return pmap
}

func NewHexEncodedRandomPartitionMap(nParts int, keyBits uint, targets []string) *PartitionMap {
	pmap := NewPartitionMap()

	// Generate equally spaced partitions across the entire key
	// range, associated with randomly chosen targets.
	one := big.NewInt(1)
	keyMax := big.NewInt(0).Lsh(one, keyBits)
	keyMax = big.NewInt(0).Sub(keyMax, one)
	bigNparts := big.NewInt(int64(nParts))

	for i := 0; i < nParts; i++ {
		endKey := big.NewInt(0).Mul(big.NewInt(int64(i+1)), keyMax)
		endKey = big.NewInt(0).Div(endKey, bigNparts)
		endKeyStr := hex.EncodeToString(endKey.Bytes())
		target := targets[rand.Intn(len(targets))]
		pmap.P = append(pmap.P, Partition{EndKey: endKeyStr, Target: target})
	}

	return pmap
}
