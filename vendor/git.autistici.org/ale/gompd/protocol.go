package mpd

import (
	"bufio"
	"errors"
	"fmt"
	"io"
	"log"
	"net"
	"strings"
	"sync"
	"unicode"
)

const helloString = "OK MPD 0.16.0\n"

// States for the connection state machine.
const (
	connStateDefault = iota
	connStateList
	connStateIdle
)

// Standard MPD error codes.
const (
	MpdNotListErrorCode        = 1
	MpdArgErrorCode            = 2
	MpdPasswordErrorCode       = 3
	MpdPermissionErrorCode     = 4
	MpdUnknownCommandErrorCode = 5
	MpdNoExistErrorCode        = 50
	MpdPlaylistMaxErrorCode    = 51
	MpdSystemErrorCode         = 52
	MpdPlaylistLoadErrorCode   = 53
	MpdUpdateAlreadyErrorCode  = 54
	MpdPlayerSyncErrorCode     = 55
	MpdExistErrorCode          = 56
)

// An MPD error, with command details. Its Error method will return
// the error formatted according to the MPD protocol (an ACK reply).
type MpdError struct {
	Code    int
	CmdNum  int
	Cmd     string
	Message string
}

func (e *MpdError) Error() string {
	return fmt.Sprintf("ACK [%d@%d] {%s} %s\n", e.Code, e.CmdNum, e.Cmd, e.Message)
}

var (
	// Special internal errors.
	errNoOutput = errors.New("no output")
	errClose    = errors.New("close")

	// Common errors.
	ErrBadArgs           = &MpdError{Code: MpdArgErrorCode, Message: "bad arguments"}
	ErrNoCommandGiven    = &MpdError{Code: MpdUnknownCommandErrorCode, Message: "No command given"}
	ErrUnsupportedScheme = &MpdError{Code: MpdNoExistErrorCode, Message: "unsupported URL scheme"}
)

// Create a new "unknown command" error.
func unknownCommandError(cmd string) error {
	return &MpdError{
		Code:    MpdUnknownCommandErrorCode,
		Message: fmt.Sprintf("unknown command \"%s\"", cmd),
	}
}

// No-op handler.
func nopCmd(w io.Writer, args []string) error {
	return nil
}

// Objects implementing the Handler interface can be registered with a
// Mux to handle a particular MPD command. The command should do its
// own argument parsing (returning ErrBadArgs in case of errors), and
// write its output to the specified io.Writer.
type Handler interface {
	ServeMPD(w io.Writer, args []string) error
}

// CommandFunc turns a function into a Handler.
type CommandFunc func(w io.Writer, args []string) error

func (f CommandFunc) ServeMPD(w io.Writer, args []string) error {
	return f(w, args)
}

// Mux is a multiplexer for MPD commands. Handlers can be registered
// to deal with specific commands.
type Mux struct {
	commands map[string]Handler
}

func NewMux() *Mux {
	return &Mux{
		commands: make(map[string]Handler),
	}
}

// Handle installs a new command handler.
func (m *Mux) Handle(name string, c Handler) {
	m.commands[name] = c
}

// HandleFunc installs a new command handler (saving a cast to CommandFunc).
func (m *Mux) HandleFunc(name string, f func(io.Writer, []string) error) {
	m.Handle(name, CommandFunc(f))
}

func (m *Mux) ServeMPD(w io.Writer, args []string) error {
	if len(args) < 1 {
		return ErrNoCommandGiven
	}
	cmd := args[0]
	args = args[1:]
	h, ok := m.commands[cmd]
	if !ok {
		log.Printf("unknown command: %s %v", cmd, args)
		return unknownCommandError(cmd)
	}

	log.Printf("command: %s %v", cmd, args)
	return h.ServeMPD(w, args)
}

// A server that speaks the MPD protocol.
type Server struct {
	*Mux

	subscribers []net.Conn
	subLock     sync.Mutex
	notifyCh    chan string
}

func NewServer() *Server {
	s := &Server{
		Mux:      NewMux(),
		notifyCh: make(chan string, 50),
	}

	// Set up some fundamental commands.
	s.HandleFunc("ping", nopCmd)
	s.HandleFunc("close", func(w io.Writer, args []string) error {
		return errClose
	})
	s.HandleFunc("commands", func(w io.Writer, args []string) error {
		// 'command_list_begin' and related commands should
		// not appear in this list as they are not really
		// commands.
		for cmd := range s.commands {
			fmt.Fprintf(w, "command: %s\n", cmd)
		}
		// 'idle' and 'noidle' are treated as proper commands.
		io.WriteString(w, "command: idle\n")
		io.WriteString(w, "command: noidle\n")
		return nil
	})
	s.HandleFunc("notcommands", nopCmd)

	return s
}

// Handles a new connection, receiving commands and handling them. The
// MPD protocol is a relatively simple line-based text protocol, with
// a couple of interesting features (idle polling and command lists)
// which are handled transparently by this function.
func (s *Server) handleConnection(c net.Conn) {
	defer s.removeSubscriber(c)
	defer c.Close()

	// Send our greeting.
	log.Printf("connection from %v", c.RemoteAddr())
	io.WriteString(c, helloString)

	state := connStateDefault
	var commandList []string
	listDumpStatus := false

	scanner := bufio.NewScanner(c)
	for scanner.Scan() {
		line := scanner.Text()
		err := errNoOutput

		// Handle the possible connection states using a
		// simple state machine. No output will be sent to the
		// client unless 'err' is set.
		switch {
		case state == connStateIdle && line == "noidle":
			state = connStateDefault
			s.removeSubscriber(c)
			// Send 'OK' to the client.
			err = nil
		case state == connStateDefault && (line == "idle" || strings.HasPrefix(line, "idle ")):
			// Ignore the actual arguments to the "idle" command.
			state = connStateIdle
			s.registerSubscriber(c)
		case state == connStateDefault && line == "command_list_begin":
			state = connStateList
			commandList = nil
			listDumpStatus = false
		case state == connStateDefault && line == "command_list_ok_begin":
			state = connStateList
			commandList = nil
			listDumpStatus = true
		case state == connStateList && line == "command_list_end":
			state = connStateDefault
			err = s.execCommandList(c, commandList, listDumpStatus)
		case state == connStateList:
			commandList = append(commandList, line)
		case state == connStateDefault:
			err = s.execCommandList(c, []string{line}, false)
		}

		if err == errNoOutput {
			continue
		}
		if err == errClose {
			break
		}

		// Detect errors sending data back to the client,
		// which should cause us to close the connection.
		var oerr error
		if err != nil {
			if merr, ok := err.(*MpdError); ok {
				_, oerr = io.WriteString(c, merr.Error())
			} else {
				merr = &MpdError{Message: err.Error()}
				_, oerr = io.WriteString(c, merr.Error())
				log.Printf("Internal error: %v", err)
			}
		} else {
			_, oerr = io.WriteString(c, "OK\n")
		}
		if oerr != nil {
			log.Printf("write error: %v: %s", c.RemoteAddr(), oerr)
			break
		}
	}

	if err := scanner.Err(); err != nil {
		log.Printf("read error: %v: %s", c.RemoteAddr(), err)
	}
}

// Execute all commands in a command list (which may consist of a
// single command).
func (s *Server) execCommandList(w io.Writer, commands []string, dumpStatus bool) error {
	for i, cmd := range commands {
		args := splitArgs(cmd)
		if err := s.ServeMPD(w, args); err != nil {
			return errorForCommand(err, i, args[0])
		}
		if dumpStatus {
			io.WriteString(w, "list_OK\n")
		}
	}
	return nil
}

func (s *Server) removeSubscriber(c net.Conn) {
	s.subLock.Lock()
	defer s.subLock.Unlock()
	if len(s.subscribers) == 0 {
		return
	}
	out := make([]net.Conn, 0, len(s.subscribers)-1)
	for _, sc := range s.subscribers {
		if c != sc {
			out = append(out, sc)
		}
	}
	s.subscribers = out
}

func (s *Server) registerSubscriber(c net.Conn) {
	s.subLock.Lock()
	defer s.subLock.Unlock()
	s.subscribers = append(s.subscribers, c)
}

func (s *Server) pubsubLoop() {
	// Trivial loop, let's hope we don't have slow clients.
	for tag := range s.notifyCh {
		tag = fmt.Sprintf("changed: %s\n", tag)
		s.subLock.Lock()
		for _, c := range s.subscribers {
			io.WriteString(c, tag)
		}
		s.subLock.Unlock()
	}
}

// Trigger sends a notification about 'tag' to all available
// subscribers (clients in idle mode).
func (s *Server) Trigger(tag string) {
	select {
	case s.notifyCh <- tag:
	default:
	}
}

// ListenAndServe starts the MPD server on the specified TCP address
// and runs our Mux to handle incoming commands.
func (s *Server) ListenAndServe(addr string) error {
	ln, err := net.Listen("tcp", addr)
	if err != nil {
		return err
	}
	go s.pubsubLoop()
	for {
		c, err := ln.Accept()
		if err != nil {
			return err
		}
		go s.handleConnection(c)
	}
}

// Split a command into space-separated fields (allowing quoting for
// fields containing spaces).
func splitArgs(s string) []string {
	var lastQuote rune
	f := func(c rune) bool {
		switch {
		case c == lastQuote:
			lastQuote = rune(0)
			return false
		case lastQuote != rune(0):
			return false
		case c == '"':
			lastQuote = c
			return false
		default:
			return unicode.IsSpace(c)
		}
	}
	var out []string
	for _, field := range strings.FieldsFunc(s, f) {
		out = append(out, strings.Trim(field, "\""))
	}
	return out
}

// Augment error information with command-specific data (but leave our
// special state-handling errors untouched).
func errorForCommand(err error, cmdNum int, cmd string) error {
	if err == errClose || err == errNoOutput {
		return err
	}
	if err, ok := err.(*MpdError); ok {
		err.CmdNum = cmdNum
		err.Cmd = cmd
		return err
	}
	return &MpdError{
		Code:    MpdSystemErrorCode,
		CmdNum:  cmdNum,
		Cmd:     cmd,
		Message: err.Error(),
	}
}
