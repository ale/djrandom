package mpd

import (
	"io"
	"io/ioutil"
	"log"
	"os"
	"sync"
)

func newDiskBackedReader(src io.ReadCloser) (*diskBackedReader, error) {
	tmpf, err := ioutil.TempFile("", "djmpd_data_")
	if err != nil {
		return nil, err
	}
	os.Remove(tmpf.Name())
	c := &diskBackedReader{
		f:   tmpf,
		c:   make(chan bool, 1),
		src: src,
	}
	go c.sourceloop()
	return c, nil
}

type diskBackedReader struct {
	f          *os.File
	c          chan bool
	wpos, rpos int64
	finalized  bool
	src        io.ReadCloser
	lock       sync.Mutex
}

func (r *diskBackedReader) Close() error {
	r.f.Close()
	return r.src.Close()
}

func (r *diskBackedReader) sourceloop() {
	buf := make([]byte, 16384)
	for {
		n, err := r.src.Read(buf)
		r.lock.Lock()
		if err != nil || n == 0 {
			r.finalized = true
			if err != nil {
				log.Printf("read error: %v", err)
			}
			break
		}

		r.f.Seek(r.wpos, 0)
		r.f.Write(buf[:n])
		r.wpos += int64(n)
		r.lock.Unlock()
		select {
		case r.c <- true:
		default:
		}
	}

}

func (r *diskBackedReader) Read(buf []byte) (int, error) {
	// Wait until there is something to read.
	r.lock.Lock()
	for !r.finalized && r.rpos+int64(len(buf)) > r.wpos {
		r.lock.Unlock()
		<-r.c
		r.lock.Lock()
	}
	r.f.Seek(r.rpos, 0)
	n, err := r.f.Read(buf)
	r.rpos += int64(n)
	r.lock.Unlock()
	return n, err
}
