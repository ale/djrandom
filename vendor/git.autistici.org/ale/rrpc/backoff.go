package rrpc

import (
	"math/rand"
	"time"
)

// Inspired by github.com/cenkalti/backoff.
type backoff struct {
	InitialInterval     time.Duration
	MaxInterval         time.Duration
	RandomizationFactor float64
	Multiplier          float64

	currentInterval time.Duration
}

const (
	defaultInitialInterval     = 100 * time.Millisecond
	defaultRandomizationFactor = 0.3
	defaultMultiplier          = 1.4
	defaultMaxInterval         = 60 * time.Second
)

func newBackoff() *backoff {
	return &backoff{
		InitialInterval:     defaultInitialInterval,
		RandomizationFactor: defaultRandomizationFactor,
		Multiplier:          defaultMultiplier,
		MaxInterval:         defaultMaxInterval,
		currentInterval:     defaultInitialInterval,
	}
}

func (b *backoff) nextBackoff() time.Duration {
	defer b.incrementCurrentInterval()
	return getRandomValueFromInterval(b.RandomizationFactor, rand.Float64(), b.currentInterval)
}

func (b *backoff) incrementCurrentInterval() {
	if float64(b.currentInterval) >= float64(b.MaxInterval)/b.Multiplier {
		b.currentInterval = b.MaxInterval
	} else {
		b.currentInterval = time.Duration(float64(b.currentInterval) * b.Multiplier)
	}
}

// Returns a random value from the interval:
// 	[randomizationFactor * currentInterval, randomizationFactor * currentInterval].
func getRandomValueFromInterval(randomizationFactor, random float64, currentInterval time.Duration) time.Duration {
	var delta = randomizationFactor * float64(currentInterval)
	var minInterval = float64(currentInterval) - delta
	var maxInterval = float64(currentInterval) + delta
	// Get a random value from the range [minInterval, maxInterval].
	// The formula used below has a +1 because if the minInterval is 1 and the maxInterval is 3 then
	// we want a 33% chance for selecting either 1, 2 or 3.
	return time.Duration(minInterval + (random * (maxInterval - minInterval + 1)))
}

type backoffTicker struct {
	C    chan time.Time
	b    *backoff
	stop chan bool
}

func newBackoffTicker(b *backoff) *backoffTicker {
	t := &backoffTicker{
		C:    make(chan time.Time),
		stop: make(chan bool),
		b:    b,
	}
	go t.run()
	return t
}

func (t *backoffTicker) Stop() {
	close(t.stop)
}

func (t *backoffTicker) run() {
	defer close(t.C)
	var c <-chan time.Time
	for {
		delay := t.b.nextBackoff()
		c = time.After(delay)
		select {
		case tick := <-c:
			sendTick(tick, t.C)
		case <-t.stop:
			return
		}
	}
}

func sendTick(tick time.Time, c chan time.Time) {
	select {
	case c <- tick:
	default:
	}
}
