package rrpc

import (
	"bufio"
	"errors"
	"io"
	"log"
	"net"
	"net/http"
	"net/rpc"
	"strings"
	"sync"
	"time"
)

var (
	ErrDeadline    = errors.New("deadline exceeded")
	ErrNoAddresses = errors.New("no addresses could be resolved")
)

type Options struct {
	// Connection timeout.
	ConnectTimeout time.Duration

	// Maximum time the whole call can take (including retries).
	Deadline time.Duration

	// Maximum number of retries (or -1 for unlimited).
	MaxRetries int

	// Support application-specific retriable errors. This
	// function will be called whenever the remote rpc.Call
	// returns an error, if it returns true the error is
	// considered temporary and the call will be retried.
	IsRetriableError func(error) bool
}

func (o Options) merge(other *Options) Options {
	if other.ConnectTimeout > 0 {
		o.ConnectTimeout = other.ConnectTimeout
	}
	if other.Deadline > 0 {
		o.Deadline = other.Deadline
	}
	if other.MaxRetries > 0 {
		o.MaxRetries = other.MaxRetries
	}
	if other.IsRetriableError != nil {
		o.IsRetriableError = other.IsRetriableError
	}
	return o
}

var defaultOptions = Options{
	ConnectTimeout: 5 * time.Second,
	Deadline:       900 * time.Second,
	MaxRetries:     -1,
}

// Client represents a specific abstract endpoint.
type Client struct {
	Addrs    []string
	Resolver Resolver
	Policy   Policy

	DefaultOptions Options

	rpcClient *rpc.Client
	lock      sync.Mutex
}

// New returns a client with default options.
func New(addr string, options *Options) *Client {
	c := Client{
		Addrs:          strings.Split(addr, ","),
		Resolver:       NewSimpleCachingResolver(&DnsResolver{}),
		Policy:         &RandomPolicy{},
		DefaultOptions: defaultOptions,
	}
	if options != nil {
		c.DefaultOptions = c.DefaultOptions.merge(options)
	}
	return &c
}

func (c *Client) dial(oldClient *rpc.Client, timeout time.Duration) (*rpc.Client, error) {
	c.lock.Lock()
	defer c.lock.Unlock()

	// Optimize the case where multiple clients get an error and
	// try to reconnect: only the first one wins, so we avoid a
	// connection storm.
	if c.rpcClient != nil && c.rpcClient != oldClient {
		return c.rpcClient, nil
	}

	if c.rpcClient != nil {
		go c.rpcClient.Close()
	}

	addr, err := c.pickAddress()
	if err != nil {
		return nil, err
	}

	client, err := dialHTTPTimeout("tcp", addr, timeout)
	if err == nil {
		c.rpcClient = client
		log.Printf("rpc: connected to %s", addr)
	}
	return client, err
}

func (c *Client) pickAddress() (string, error) {
	var candidates []string
	for _, addr := range c.Addrs {
		if addrs, err := c.Resolver.Resolve(addr); err == nil {
			candidates = append(candidates, addrs...)
		}
	}
	if len(candidates) == 0 {
		return "", ErrNoAddresses
	}
	return c.Policy.Select(candidates), nil
}

func withDeadline(f func() error, deadline time.Duration) error {
	resultCh := make(chan error, 1)
	go func() {
		resultCh <- f()
	}()

	deadlineCh := time.After(deadline)
	var err error
	select {
	case err = <-resultCh:
	case <-deadlineCh:
		err = ErrDeadline
	}
	return err
}

// Retry 'f' with exponential backoff until 'deadline' expires, or until 'quit' is triggered.
func retry(f func() error, shouldRetry func(error) bool, deadline time.Duration, maxRetries int, quit chan bool) error {
	nconn := 0
	b := newBackoff()
	ticker := newBackoffTicker(b)
	defer ticker.Stop()

	var err error

	for {
		err = f()
		if err == nil || !shouldRetry(err) {
			break
		}

		// Reconnect.
		nconn++
		if maxRetries > 0 && nconn >= maxRetries {
			break
		}
		select {
		case <-ticker.C:
		case <-quit:
			return nil
		}
	}

	return err
}

// Call invokes the named function, waits for it to complete, and returns
// its error status.
func (c *Client) Call(method string, req, resp interface{}, options *Options) error {
	o := c.DefaultOptions
	if options != nil {
		o = o.merge(options)
	}

	quit := make(chan bool)
	defer close(quit)

	var client *rpc.Client

	return withDeadline(func() error {
		return retry(func() error {
			var err error
			client, err = c.dial(client, o.ConnectTimeout)
			if err != nil {
				return err
			}
			return client.Call(method, req, resp)
		}, func(err error) bool {
			result := isRpcTransportError(err) || (o.IsRetriableError != nil && o.IsRetriableError(err))
			// log.Printf("is error retriable? %v -> %v", err, result)
			return result
		}, o.Deadline, o.MaxRetries, quit)
	}, o.Deadline)
}

func (c *Client) Close() {
}

// Create a RPC client from an HTTP connection, with a timeout.
// Basically just a clone of rpc.DialHTTP with added timeouts.  We
// wouldn't really need this but it helps clients if unreachable hosts
// are detected quickly.
func dialHTTPTimeout(network, address string, timeout time.Duration) (*rpc.Client, error) {
	var err error
	conn, err := net.DialTimeout(network, address, timeout)
	if err != nil {
		return nil, err
	}
	io.WriteString(conn, "CONNECT "+rpc.DefaultRPCPath+" HTTP/1.0\n\n")

	resp, err := http.ReadResponse(bufio.NewReader(conn), &http.Request{Method: "CONNECT"})
	if err == nil && resp.StatusCode == 200 {
		return rpc.NewClient(conn), nil
	}
	if err == nil {
		err = errors.New("unexpected HTTP response: " + resp.Status)
	}
	conn.Close()
	return nil, &net.OpError{
		Op:   "dial-http",
		Net:  network + " " + address,
		Addr: nil,
		Err:  err,
	}
}

func isRpcTransportError(err error) bool {
	return (err == io.EOF || err == io.ErrUnexpectedEOF || err == rpc.ErrShutdown)
}
