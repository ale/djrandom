package rrpc

import (
	"errors"
	"math/rand"
	"net"
	"strconv"
	"strings"
	"sync"
	"time"
)

// A Resolver finds one multiple endpoints for a service, given an
// address string. The address string take the form of a path, where
// the prefix specifies which method to use for name resolution.
//
// A few resolvers are provided by default for simple DNS-based name
// resolution.
//
type Resolver interface {
	Resolve(addr string) ([]string, error)
}

// A Policy selects one specific address out of all the possible ones
// returned by a Resolver.
type Policy interface {
	Select(addrs []string) string
}

// RandomPolicy picks a random address among those available.
type RandomPolicy struct{}

func (rr *RandomPolicy) Select(addrs []string) string {
	return addrs[rand.Intn(len(addrs))]
}

// Plan A/AAAA DNS resolver.
type DnsResolver struct{}

func (r *DnsResolver) Resolve(addr string) ([]string, error) {
	host, port, err := net.SplitHostPort(addr)
	if err != nil {
		return nil, err
	}

	addrs, err := net.LookupIP(host)
	if err != nil {
		return nil, err
	}
	out := make([]string, 0, len(addrs))
	for _, ip := range addrs {
		out = append(out, net.JoinHostPort(ip.String(), port))
	}
	return out, nil
}

// SRV-based DNS resolution, retrieving port information as well.
// It expects addresses in the "NAME/SERVICE" format, which translates
// to an SRV query for "_SERVICE._tcp.NAME".
type DnsSrvResolver struct{}

func (r DnsSrvResolver) Resolve(addr string) ([]string, error) {
	parts := strings.Split(addr, "/")
	if len(parts) != 2 {
		return nil, errors.New("bad address format")
	}

	_, results, err := net.LookupSRV(parts[1], "tcp", parts[0])
	if err != nil {
		return nil, err
	}

	resolved := make([]string, 0, len(results))
	for _, result := range results {
		resolved = append(resolved, net.JoinHostPort(result.Target, strconv.Itoa(int(result.Port))))
	}
	return resolved, nil
}

type cacheEntry struct {
	addrs []string
	err   error
	stamp time.Time
}

// A caching resolver that wraps an upstream Resolver. Note that the
// cache locking serializes all name resolution, so you shouldn't use
// this object if you expect to independently resolve a large number
// of different addresses.
type SimpleCachingResolver struct {
	TTL time.Duration

	parent Resolver
	cache  map[string]cacheEntry
	lock   sync.Mutex
}

func NewSimpleCachingResolver(r Resolver) *SimpleCachingResolver {
	return &SimpleCachingResolver{
		TTL:    60 * time.Second,
		parent: r,
		cache:  make(map[string]cacheEntry),
	}
}

func (r *SimpleCachingResolver) Resolve(addr string) ([]string, error) {
	r.lock.Lock()
	defer r.lock.Unlock()

	now := time.Now()
	entry, ok := r.cache[addr]
	if !ok || now.Sub(entry.stamp) > r.TTL {
		addrs, err := r.parent.Resolve(addr)
		entry = cacheEntry{addrs, err, now}
		r.cache[addr] = entry
	}

	return entry.addrs, entry.err
}
