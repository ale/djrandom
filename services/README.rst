
Internal Services Implementation
--------------------------------

Every service has an interface and an implementation. Interfaces are
defined in the ``djrandom/services`` package, while the implementations
are contained in subdirectories. Every implementation must define a
client type that users of the service can instantiate, and it will
usually provide a server implementation.

Distributed services implement the PartitionedService defined in the
'djrandom/partition' package, which allows a very basic distributed
(partitioned) behavior based on a single primary key structure (with
rebalancing after partition changes).

Each implementation is RPC-based, it has a ``client.go`` file that
contains the client, and a ``service.go`` (and other files) that
define the server side. The only odd service is the database, where
``database.go`` actually holds the high-level database logic, which is
independent of the underlying key/value db implementation.
