package backend_leveldb

import (
	"fmt"
	"log"
	"os"
	"testing"
)

var (
	dbPath = ".leveldbtest-1"
)

func createDb() *LevelDbBackend {
	return NewLevelDbBackend(dbPath)
}

func removeDb(s *LevelDbBackend) {
	s.Close()
	os.RemoveAll(dbPath)
}

type testObj struct {
	A int
	B string
}

func TestLevelDbBackend_Put(t *testing.T) {
	db := createDb()
	defer removeDb(db)

	data := []byte("some data")
	err := db.Put("bucket", "key", data)
	if err != nil {
		t.Fatalf("Put() failed: %s", err)
	}
}

func TestLevelDbBackend_Get(t *testing.T) {
	db := createDb()
	defer removeDb(db)

	data := []byte("some data")
	err := db.Put("bucket", "key", data)
	if err != nil {
		t.Fatalf("Put() failed: %s", err)
	}

	var result []byte
	result, err = db.Get("bucket", "key")
	if err != nil {
		t.Fatalf("Get() failed: %s", err)
	}
	if string(result) != "some data" {
		t.Fatalf("Get() results do not match!")
	}

	result, err = db.Get("wrong bucket", "key")
	if err == nil {
		t.Fatalf("Get() on wrong bucket returned no error")
	}
}

func TestLevelDbBackend_Scan(t *testing.T) {
	db := createDb()
	defer removeDb(db)

	objs := []string{"one", "two", "three"}
	for _, s := range objs {
		err := db.Put("bucket", s, []byte(s))
		if err != nil {
			t.Fatalf("Put() failed: %s", err)
		}
	}

	i, err := db.Scan("bucket", "t", "zebra", -1, false)
	if err != nil {
		t.Fatalf("Scan error: %s", err)
	}

	out := [][]byte{}
	for {
		key, data, err := i.Next()
		if err == EOF {
			break
		} else if err != nil {
			t.Errorf("Next() error: %s", err)
		}
		log.Printf("key: %s", key)
		out = append(out, data)
	}

	if len(out) != 2 {
		t.Fatalf("Unexpected scan result length %d", len(out))
	}
}

func BenchmarkLevelDb_Put(b *testing.B) {
	b.StopTimer()
	db := createDb()
	defer removeDb(db)

	// Create objects.
	b.StartTimer()
	data := []byte("some data")
	for i := 0; i < b.N; i++ {
		key := fmt.Sprintf("%016x", i)
		err := db.Put("bucket", key, data)
		if err != nil {
			b.Fatalf("Put() failed: %s", err)
		}
	}
}

func BenchmarkLevelDb_Get(b *testing.B) {
	b.StopTimer()
	db := createDb()
	defer removeDb(db)

	// Create a few objects.
	nObjs := 100
	data := []byte("some more data just for this test")
	keys := make([]string, nObjs)
	for i := 0; i < nObjs; i++ {
		keys[i] = fmt.Sprintf("%016x", i)
		db.Put("bucket", keys[i], data)
	}

	// Retrieve objects.
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		key := keys[i%nObjs]
		if _, err := db.Get("bucket", key); err != nil {
			b.Fatalf("Get() failed: %s", err)
		}
	}
	b.StopTimer()
}
