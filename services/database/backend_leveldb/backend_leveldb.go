package backend_leveldb

import (
	"bytes"
	"errors"
	"fmt"
	"log"
	"strings"

	"github.com/jmhodges/levigo"

	"git.autistici.org/ale/djrandom/services/database"
)

var (
	EOF      = errors.New("EOF")
	NotFound = errors.New("Not Found")
)

type LevelDbBackend struct {
	db     *levigo.DB
	cache  *levigo.Cache
	filter *levigo.FilterPolicy
}

func NewLevelDbBackend(path string) *LevelDbBackend {
	opts := levigo.NewOptions()
	cache := levigo.NewLRUCache(2 << 27)
	opts.SetCache(cache)
	opts.SetCreateIfMissing(true)
	filter := levigo.NewBloomFilter(10)
	opts.SetFilterPolicy(filter)
	db, err := levigo.Open(path, opts)
	if err != nil {
		log.Fatal("Error creating database: %s", err)
	}

	return &LevelDbBackend{db, cache, filter}
}

func (be *LevelDbBackend) Close() {
	be.db.Close()
	be.cache.Close()
	be.filter.Close()
}

func (be *LevelDbBackend) Get(bucket, key string) ([]byte, error) {
	row := makeLevelDbRowId(bucket, key)
	ro := levigo.NewReadOptions()
	defer ro.Close()

	// We need to return an error if the data was not found.
	data, err := be.db.Get(ro, row)
	if err == nil && data == nil {
		return nil, NotFound
	}
	return data, err
}

func (be *LevelDbBackend) Put(bucket, key string, data []byte) error {
	row := makeLevelDbRowId(bucket, key)
	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return be.db.Put(wo, row, data)
}

func (be *LevelDbBackend) Del(bucket, key string) error {
	row := makeLevelDbRowId(bucket, key)
	wo := levigo.NewWriteOptions()
	defer wo.Close()
	return be.db.Delete(wo, row)
}

type levelDbIterator struct {
	it       *levigo.Iterator
	opts     *levigo.ReadOptions
	rowEnd   []byte
	count    int
	limit    int
	keysOnly bool
}

func (i *levelDbIterator) Next() (string, []byte, error) {
	if !i.it.Valid() || (i.limit > 0 && i.count >= i.limit) || bytes.Compare(i.it.Key(), i.rowEnd) >= 0 {
		return "", nil, EOF
	}
	_, key := parseLevelDbRowId(i.it.Key())
	var value []byte
	if !i.keysOnly {
		value = i.it.Value()
	}
	i.it.Next()
	i.count += 1
	return key, value, nil
}

func (i *levelDbIterator) Close() {
	i.it.Close()
	i.opts.Close()
}

func (be *LevelDbBackend) Scan(bucket, startKey, endKey string, limit int, keysOnly bool) (database.BackendIterator, error) {
	rowStart := makeLevelDbRowId(bucket, startKey)
	rowEnd := makeLevelDbRowId(bucket, endKey)
	ro := levigo.NewReadOptions()
	ro.SetFillCache(false)
	it := be.db.NewIterator(ro)
	it.Seek(rowStart)
	return &levelDbIterator{
		it:       it,
		rowEnd:   rowEnd,
		limit:    limit,
		opts:     ro,
		keysOnly: keysOnly,
	}, nil
}

func makeLevelDbRowId(bucket, key string) []byte {
	return []byte(fmt.Sprintf("%s/%s", bucket, key))
}

func parseLevelDbRowId(row []byte) (string, string) {
	kvparts := strings.SplitN(string(row), "/", 2)
	return kvparts[0], kvparts[1]
}
