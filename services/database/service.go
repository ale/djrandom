package database

import (
	"log"
	"net/rpc"

	"git.autistici.org/ale/djrandom/util/instrumentation"
)

// An iterator for the backend storage. Call Next() until it returns
// an error.
type BackendIterator interface {
	Next() (string, []byte, error)
	Close()
}

// Backend is the interface that must be implemented by the backend
// storage driver for the default, RPC-based, database service.
type Backend interface {
	Get(bucket, key string) ([]byte, error)
	Put(bucket, key string, data []byte) error
	Del(bucket, key string) error
	Scan(bucket, startKey, endKey string, limit int, keysOnly bool) (BackendIterator, error)
}

type DatabaseService struct {
	Backend Backend

	counters *instrumentation.Counter
}

func NewDatabaseService(backend Backend) *DatabaseService {
	return &DatabaseService{
		Backend:  backend,
		counters: instrumentation.NewCounter("rpc", 1),
	}
}

type EmptyResponse struct{}

type DbGetRequest struct {
	Bucket string
	Key    string
}

type DbGetResponse struct {
	Data []byte
}

type DbPutRequest struct {
	Bucket string
	Key    string
	Data   []byte
}

type DbDelRequest struct {
	Bucket string
	Key    string
}

type Datum struct {
	Key  string
	Data []byte
}

type DbScanRequest struct {
	Bucket   string
	StartKey string
	EndKey   string
	Limit    int
	KeysOnly bool
}

type DbScanResponse struct {
	Results []Datum
}

func (db *DatabaseService) Get(req *DbGetRequest, res *DbGetResponse) error {
	defer db.counters.IncVar("get", 1)
	var err error
	res.Data, err = db.Backend.Get(req.Bucket, req.Key)
	log.Printf("Get(%s, %s) -> err=%v", req.Bucket, req.Key, err)
	return err
}

func (db *DatabaseService) Put(req *DbPutRequest, res *EmptyResponse) error {
	defer db.counters.IncVar("put", 1)
	log.Printf("Put(%s, %s)", req.Bucket, req.Key)
	return db.Backend.Put(req.Bucket, req.Key, req.Data)
}

func (db *DatabaseService) Del(req *DbDelRequest, res *EmptyResponse) error {
	defer db.counters.IncVar("del", 1)
	log.Printf("Del(%s, %s)", req.Bucket, req.Key)
	return db.Backend.Del(req.Bucket, req.Key)
}

func (db *DatabaseService) Scan(req *DbScanRequest, res *DbScanResponse) error {
	defer db.counters.IncVar("scan", 1)
	iter, err := db.Backend.Scan(req.Bucket, req.StartKey, req.EndKey, req.Limit, req.KeysOnly)
	if err != nil {
		return err
	}
	defer iter.Close()

	results := []Datum{}
	for {
		key, data, err := iter.Next()
		if err != nil {
			break
		}
		results = append(results, Datum{Key: key, Data: data})
	}
	res.Results = results

	log.Printf("Scan(%s, %s, %s) -> %d results", req.Bucket, req.StartKey, req.EndKey, len(results))

	return nil
}

func RegisterService(db *DatabaseService) {
	setupDebugHandlers(db)
	rpc.RegisterName("Db", db)
	rpc.HandleHTTP()
}
