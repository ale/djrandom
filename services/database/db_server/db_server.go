package main

import (
	"flag"
	"log"

	"git.autistici.org/ale/djrandom/services/database"
	"git.autistici.org/ale/djrandom/services/database/backend_leveldb"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/daemon"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
	addr       = flag.String("addr", ":3007", "TCP address to listen on")
	dbPath     = config.String("db_path", "", "Database directory")
)

func main() {
	flag.Parse()
	config.MustParse(*configFile)

	daemon.Setup()

	backend := backend_leveldb.NewLevelDbBackend(
		config.MustString("db_path", *dbPath))
	dbsvc := database.NewDatabaseService(backend)
	database.RegisterService(dbsvc)

	log.Printf("Starting database service on %s", *addr)
	err := util.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
