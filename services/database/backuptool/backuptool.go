package main

import (
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"flag"
	"fmt"
	"io"
	"log"
	"os"
	"sync"

	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/util/config"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
	dump       = flag.Bool("dump", false, "Dump the database")
	restore    = flag.Bool("restore", false, "Restore a database dump")
	verbose    = flag.Bool("verbose", false, "Print statistics on the dump")
)

// List of database buckets that we should dump. Other data can be
// reconstructed with a Mapreduce.
var allBuckets = []string{
	db_client.SongBucket,
	db_client.AudioFileBucket,
	db_client.FingerprintBucket,
	db_client.AcousticDataBucket,
	db_client.AcousticCorrelationBucket,
	db_client.UserBucket,
	db_client.AuthKeyBucket,
	db_client.PlaylogBucket,
}

type Record struct {
	Bucket string
	Key    string
	Data   []byte
}

func ReadRecord(in io.Reader) (*Record, error) {
	var sz int64
	if err := binary.Read(in, binary.LittleEndian, &sz); err != nil {
		return nil, err
	}

	data := make([]byte, sz)
	n, err := io.ReadFull(in, data)
	if err != nil {
		return nil, err
	}
	if int64(n) != sz {
		return nil, fmt.Errorf("short read (got %d, want %d)", n, sz)
	}

	var record Record
	if err := gob.NewDecoder(bytes.NewBuffer(data)).Decode(&record); err != nil {
		return nil, err
	}

	return &record, nil
}

func WriteRecord(out io.Writer, record Record) error {
	var buf bytes.Buffer
	if err := gob.NewEncoder(&buf).Encode(&record); err != nil {
		return err
	}
	enc := buf.Bytes()
	sz := int64(len(enc))

	binary.Write(out, binary.LittleEndian, sz)
	_, err := out.Write(enc)
	return err
}

func scanner(db *db_client.DatabaseRpcClient, bucketCh chan string, outCh chan Record) {
	session, err := db.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	for bucket := range bucketCh {
		// Call Scan() in batches, to avoid overloading the server.
		total := 0
		batchSize := 1000
		startKey, endKey := "", "\xff"
		for startKey < endKey {
			iter, err := session.Scan(bucket, startKey, endKey, batchSize)
			if err != nil {
				log.Printf("Scan error for bucket %s: %s", bucket, err)
				break
			}

			// Iterate over the results and send them to outCh.
			n := 0
			for {
				var data []byte
				key, err := iter.Next(&data)
				if err != nil {
					break
				}

				outCh <- Record{
					Bucket: bucket,
					Key:    key,
					Data:   data,
				}
				n++
				total++
				startKey = key
			}
			// An empty result means we've reached the end
			// of the scan.
			if n == 0 {
				break
			}
			// Start the next scan from the next key.
			startKey = db_client.IncrementKey(startKey)
		}
		if *verbose {
			log.Printf("bucket %s: %d entries", bucket, total)
		}
	}
}

func doDump(db *db_client.DatabaseRpcClient) {
	nScanners := 3

	bucketCh := make(chan string, nScanners)
	recCh := make(chan Record, 1000)

	var wg sync.WaitGroup
	for i := 0; i < nScanners; i++ {
		wg.Add(1)
		go func() {
			scanner(db, bucketCh, recCh)
			wg.Done()
		}()
	}

	go func() {
		wg.Wait()
		close(recCh)
	}()

	go func() {
		for _, bucket := range allBuckets {
			bucketCh <- bucket
		}
		close(bucketCh)
	}()

	for rec := range recCh {
		WriteRecord(os.Stdout, rec)
	}
}

func dbwriter(db *db_client.DatabaseRpcClient, inCh chan Record) {
	session, err := db.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	for rec := range inCh {
		if err := session.Put(rec.Bucket, rec.Key, rec.Data); err != nil {
			log.Printf("Error writing record %s/%s: %s", rec.Bucket, rec.Key, err)
		}
	}
}

func doRestore(db *db_client.DatabaseRpcClient) {
	nWriters := 20

	recCh := make(chan Record, 1000)

	var wg sync.WaitGroup
	for i := 0; i < nWriters; i++ {
		wg.Add(1)
		go func() {
			dbwriter(db, recCh)
			wg.Done()
		}()
	}

	go func() {
		for {
			rec, err := ReadRecord(os.Stdin)
			if err != nil {
				log.Printf("read error: %s", err)
				return
			}
			recCh <- *rec
		}
		close(recCh)
	}()

	wg.Wait()
}

func main() {
	log.SetFlags(0)
	flag.Parse()
	config.Parse(*configFile)

	if !*dump && !*restore {
		log.Fatal("You need to specify either --dump or --restore")
	}

	db := db_client.NewRawDatabaseRpcClientFromConfig()

	if *dump {
		doDump(db)
	} else {
		doRestore(db)
	}
}
