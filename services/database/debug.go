package database

import (
	"bytes"
	"html/template"
	"net/http"

	"git.autistici.org/ale/djrandom/util/status"
)

const queryHtml = `<html>
<body>
<h1>Query</h1>
<form action="" method="post">
<table>
 <tr>
  <td>Bucket:</td>
  <td><input type="text" name="bucket" value="{{.Bucket}}"></td>
 </tr>
 <tr>
  <td>Object ID:</td>
  <td><input type="text" name="id" value="{{.Id}}"></td>
 </tr>
 <tr>
  <td></td>
  <td><button type="submit">Lookup</button></td>
 </tr>
</table>
{{if .Err}}
<hr>
<h4>Error: {{.Err}}</h4>
{{else if .Obj}}
<hr>
<pre>{{.Obj}}</pre>
{{end}}
</form>
</body>
</html>`

var (
	queryTmpl = template.Must(template.New("Query").Parse(queryHtml))
)

func setupDebugHandlers(db *DatabaseService) {
	http.HandleFunc("/debug/db", func(w http.ResponseWriter, r *http.Request) {
		var ctx struct {
			Bucket string
			Id     string
			Obj    string
			Err    error
		}
		ctx.Bucket = "song"

		if r.Method == "POST" {
			ctx.Id = r.FormValue("id")
			ctx.Bucket = r.FormValue("bucket")
			if ctx.Id != "" || ctx.Bucket != "" {
				data, err := db.Backend.Get(ctx.Bucket, ctx.Id)
				ctx.Obj = string(data)
				ctx.Err = err
			}
		}

		var buf bytes.Buffer
		if err := queryTmpl.Execute(&buf, ctx); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
		w.Write(buf.Bytes())
	})
}

func init() {
	status.RegisterDebugEndpoint("/debug/db")
}
