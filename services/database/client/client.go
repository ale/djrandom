package database

import (
	"encoding/json"
	"errors"

	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/services/database"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/rrpc"
)

var (
	EOF = errors.New("EOF")

	dbAddr = config.String("db_addr", "", "TCP address of the database server")
)

type ClientCodec interface {
	Marshal(interface{}) ([]byte, error)
	Unmarshal([]byte, interface{}) error
}

type JsonCodec struct{}

func (j JsonCodec) Marshal(obj interface{}) ([]byte, error) {
	return json.Marshal(obj)
}

func (j JsonCodec) Unmarshal(data []byte, obj interface{}) error {
	return json.Unmarshal(data, obj)
}

type RawCodec struct{}

func (r RawCodec) Marshal(obj interface{}) ([]byte, error) {
	return (obj).([]byte), nil
}

func (r RawCodec) Unmarshal(data []byte, obj interface{}) error {
	*((obj).(*[]byte)) = data
	return nil
}

type DatabaseRpcClient struct {
	codec  ClientCodec
	client *rrpc.Client
}

// In this implementation, the session only contains a pointer back to
// a DatabaseRpcClient object.
type DatabaseRpcClientSession struct {
	*DatabaseRpcClient
	RpcOptions *rrpc.Options
}

func NewDatabaseRpcClient(endpoint string, codec ClientCodec) *DatabaseRpcClient {
	return &DatabaseRpcClient{
		codec:  codec,
		client: rrpc.New(endpoint, nil),
	}
}

func NewDatabaseRpcClientFromConfig() *DatabaseRpcClient {
	return NewDatabaseRpcClient(
		config.MustString("db_addr", *dbAddr),
		JsonCodec{})
}

func NewRawDatabaseRpcClientFromConfig() *DatabaseRpcClient {
	return NewDatabaseRpcClient(
		config.MustString("db_addr", *dbAddr),
		RawCodec{})
}

func (c *DatabaseRpcClient) NewSession() (services.Session, error) {
	return &DatabaseRpcClientSession{DatabaseRpcClient: c}, nil
}

// Session methods.

func (s *DatabaseRpcClientSession) Get(bucket, key string, obj interface{}) error {
	req := &database.DbGetRequest{
		Bucket: bucket,
		Key:    key,
	}
	var resp database.DbGetResponse
	if err := s.client.Call("Db.Get", req, &resp, s.RpcOptions); err != nil {
		return err
	}
	return s.codec.Unmarshal(resp.Data, obj)
}

func (s *DatabaseRpcClientSession) Put(bucket, key string, obj interface{}) error {
	data, err := s.codec.Marshal(obj)
	if err != nil {
		return err
	}
	req := &database.DbPutRequest{
		Bucket: bucket,
		Key:    key,
		Data:   data,
	}
	var resp database.EmptyResponse
	return s.client.Call("Db.Put", req, &resp, s.RpcOptions)
}

func (s *DatabaseRpcClientSession) Del(bucket, key string) error {
	req := &database.DbDelRequest{
		Bucket: bucket,
		Key:    key,
	}
	var resp database.EmptyResponse
	return s.client.Call("Db.Del", req, &resp, s.RpcOptions)
}

type resultIterator struct {
	results []database.Datum
	codec   ClientCodec
	i       int
}

func (r *resultIterator) Next(obj interface{}) (string, error) {
	if r.i >= len(r.results) {
		return "", EOF
	}
	if obj != nil {
		if err := r.codec.Unmarshal(r.results[r.i].Data, obj); err != nil {
			return "", err
		}
	}
	key := r.results[r.i].Key
	r.i++
	return key, nil
}

func (s *DatabaseRpcClientSession) doScan(bucket, startKey, endKey string, limit int, keysOnly bool) (services.Iterator, error) {
	req := &database.DbScanRequest{
		Bucket:   bucket,
		StartKey: startKey,
		EndKey:   endKey,
		Limit:    limit,
		KeysOnly: keysOnly,
	}
	var resp database.DbScanResponse
	if err := s.client.Call("Db.Scan", req, &resp, s.RpcOptions); err != nil {
		return nil, err
	}

	iter := &resultIterator{
		results: resp.Results,
		codec:   s.codec,
	}
	return iter, nil
}

func (s *DatabaseRpcClientSession) Scan(bucket, startKey, endKey string, limit int) (services.Iterator, error) {
	return s.doScan(bucket, startKey, endKey, limit, false)
}

func (s *DatabaseRpcClientSession) ScanKeys(bucket, startKey, endKey string, limit int) (services.Iterator, error) {
	return s.doScan(bucket, startKey, endKey, limit, true)
}

func (s *DatabaseRpcClientSession) Close() {
	//s.client.Close()
}

// IncrementKey returns the key immediately following the given one
// (key + 1).
func IncrementKey(key string) string {
	b := []byte(key)
	for i := len(key) - 1; i >= 0; i-- {
		if b[i] != 0xff {
			b[i]++
			return string(b)
		} else {
			b[i] = 0
		}
	}
	return "\x01" + string(b)
}
