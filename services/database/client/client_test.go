package database

import (
	"testing"
)

func TestIncrementKey(t *testing.T) {
	testData := []struct {
		input, output string
	}{
		{"a", "b"},
		{"aa", "ab"},
		{"aaa", "aab"},
		{"a\xff", "b\x00"},
		{"\xff", "\x01\x00"},
	}

	for _, data := range testData {
		got := IncrementKey(data.input)
		if got != data.output {
			t.Errorf("IncrementKey(%q) -> got %q, want %q", data.input, got, data.output)
		}
	}
}
