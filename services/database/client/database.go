// The database service consists of a set of high-level operations
// built on top of a simple key/value store.
//
// The backend implementation is abstracted in the DatabaseClient
// interface, and is currently implemented as a single-instance RPC
// server using an SQLite backend.  The implementation can be replaced
// by any key/value store that supports buckets and range scans.
//
// The high-level API implements some simple relational behavior for
// the more complex objects (such as Song), and it hides the bucket
// names from the caller. Access is managed via Session objects, which
// are supposed to be short-lived and expose the low-level API. One
// day Sessions might acquire transactional semantics, but it
// currently has none.
//
package database

import (
	"fmt"
	"log"
	"math/rand"
	"strings"
	"sync"
	"time"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
)

const (
	// Key prefixes for various object types.
	SongBucket                = "song"
	AudioFileBucket           = "audio"
	FingerprintBucket         = "fp"
	AcousticDataBucket        = "ac_data"
	AcousticCorrelationBucket = "ac_corr"
	UserBucket                = "user"
	AuthKeyBucket             = "auth_key"
	PlaylogBucket             = "playlog"
	MarkovBucket              = "markov"
	StatsBucket               = "stats"

	// Default concurrency for parallel song fetches.
	defaultConcurrency = 20
)

type DatabaseImpl struct {
	client services.DatabaseClient
}

func NewDatabaseImpl(client services.DatabaseClient) *DatabaseImpl {
	return &DatabaseImpl{client: client}
}

func NewDatabaseImplFromConfig() *DatabaseImpl {
	return NewDatabaseImpl(NewDatabaseRpcClientFromConfig())
}

func (db *DatabaseImpl) NewSession() (services.Session, error) {
	return db.client.NewSession()
}

func (db *DatabaseImpl) uniqueToken(base string) string {
	return fmt.Sprintf("%s:%08x:%016x",
		base,
		time.Now().Unix(),
		rand.Int63())
}

func (db *DatabaseImpl) makeKey(parts ...string) string {
	return strings.Join(parts, "/")
}

func (db *DatabaseImpl) GetSong(s services.Session, id api.SongID) (*api.Song, bool) {
	if s == nil {
		var serr error
		if s, serr = db.client.NewSession(); serr != nil {
			return nil, false
		}
		defer s.Close()
	}

	var songIntl api.SongIntl
	err := s.Get(SongBucket, id.String(), &songIntl)
	if err != nil {
		log.Printf("GetSong(%s): error %s", id, err)
		return nil, false
	}

	song := api.Song{
		Id:    songIntl.Id,
		Meta:  songIntl.Meta,
		Info:  songIntl.Info,
		Files: make([]*api.AudioFile, len(songIntl.Files)),
	}

	var wg sync.WaitGroup

	for i, audioHash := range songIntl.Files {
		wg.Add(1)
		go func(audioHash string, idx int) {
			af, ok := db.GetAudioFile(s, audioHash)
			if !ok {
				log.Printf("GetSong(%s): AudioFile not found: %s", id, audioHash)
			} else {
				song.Files[idx] = af
			}
			wg.Done()
		}(audioHash, i)
	}

	wg.Wait()

	return &song, true
}

func (db *DatabaseImpl) GetSongWithoutDupes(s services.Session, id api.SongID) (*api.Song, bool) {
	if s == nil {
		var serr error
		if s, serr = db.client.NewSession(); serr != nil {
			return nil, false
		}
		defer s.Close()
	}

	for {
		song, ok := db.GetSong(s, id)
		if !ok || song.Info.State != "duplicate" {
			return song, ok
		}
		id = song.Info.DuplicateOf
	}
	return nil, false // unreached
}

func (db *DatabaseImpl) PutSong(s services.Session, song *api.Song) error {
	if s == nil {
		var serr error
		if s, serr = db.client.NewSession(); serr != nil {
			return serr
		}
		defer s.Close()
	}

	songIntl := api.SongIntl{
		Id:    song.Id,
		Meta:  song.Meta,
		Info:  song.Info,
		Files: make([]string, len(song.Files)),
	}

	var wg sync.WaitGroup

	for i, af := range song.Files {
		songIntl.Files[i] = af.MD5
		wg.Add(1)
		go func(af *api.AudioFile) {
			err := db.PutAudioFile(s, af)
			if err != nil {
				log.Printf("PutSong(%s): Error saving audioFile %s: %s", song.Id, af.MD5, err.Error())
			}
			wg.Done()
		}(af)
	}

	wg.Wait()

	return s.Put(SongBucket, song.Id.String(), songIntl)
}

func (db *DatabaseImpl) GetAudioFile(s services.Session, key string) (*api.AudioFile, bool) {
	var af api.AudioFile
	if err := db.Get(s, AudioFileBucket, key, &af); err != nil {
		return nil, false
	}
	return &af, true
}

func (db *DatabaseImpl) PutAudioFile(s services.Session, af *api.AudioFile) error {
	return db.Put(s, AudioFileBucket, af.MD5, af)
}

func (db *DatabaseImpl) GetUser(s services.Session, email string) (*api.User, bool) {
	var user api.User
	if err := db.Get(s, UserBucket, email, &user); err != nil {
		return nil, false
	}
	return &user, true
}

func (db *DatabaseImpl) PutUser(s services.Session, user *api.User) error {
	return db.Put(s, UserBucket, user.Email, user)
}

func (db *DatabaseImpl) GetAuthKey(s services.Session, keyId string) (*api.AuthKey, bool) {
	var authKey api.AuthKey
	if err := db.Get(s, AuthKeyBucket, keyId, &authKey); err != nil {
		return nil, false
	}
	return &authKey, true
}

func (db *DatabaseImpl) PutAuthKey(s services.Session, authKey *api.AuthKey) error {
	return db.Put(s, AuthKeyBucket, authKey.KeyId, authKey)
}

func (db *DatabaseImpl) GetSongFingerprint(s services.Session, id api.SongID) (*api.Fingerprint, error) {
	var fp api.Fingerprint
	err := db.Get(s, FingerprintBucket, id.String(), &fp)
	return &fp, err
}

func (db *DatabaseImpl) PutSongFingerprint(s services.Session, id api.SongID, fp *api.Fingerprint) error {
	return db.Put(s, FingerprintBucket, id.String(), fp)
}

func (db *DatabaseImpl) GetSongAcousticData(s services.Session, song *api.Song) (*api.AcousticData, error) {
	var ac api.AcousticData
	err := db.Get(s, AcousticDataBucket, song.Id.String(), &ac)
	return &ac, err
}

func (db *DatabaseImpl) PutSongAcousticData(s services.Session, song *api.Song, ac *api.AcousticData) error {
	return db.Put(s, AcousticDataBucket, song.Id.String(), ac)
}

func correlationKey(id1, id2 string) string {
	if id1 < id2 {
		return id1 + "/" + id2
	}
	return id2 + "/" + id1
}

func (db *DatabaseImpl) GetSongAcousticCorrelation(s services.Session, songId1, songId2 api.SongID) (float32, error) {
	var corr float32
	err := db.Get(s, AcousticCorrelationBucket, correlationKey(songId1.String(), songId2.String()), &corr)
	return corr, err
}

func (db *DatabaseImpl) GetSongAcousticCorrelations(s services.Session, id api.SongID) ([]api.AcousticCorrelation, error) {
	if s == nil {
		var serr error
		if s, serr = db.client.NewSession(); serr != nil {
			return nil, serr
		}
		defer s.Close()
	}

	result := make([]api.AcousticCorrelation, 0)
	iter, err := s.Scan(AcousticCorrelationBucket, id.String()+"/", id.String()+"/\xff", -1)
	if err != nil {
		return nil, err
	}
	for {
		var corr float32
		key, err := iter.Next(&corr)
		if err != nil {
			break
		}
		result = append(result, api.AcousticCorrelation{key, corr})
	}
	return result, nil
}

func (db *DatabaseImpl) PutSongAcousticCorrelation(s services.Session, songId1, songId2 api.SongID, corr float32) error {
	return db.Put(s, AcousticCorrelationBucket, correlationKey(songId1.String(), songId2.String()), &corr)
}

func (db *DatabaseImpl) GetPlayLog(s services.Session, key string) (*api.PlayLogEntry, bool) {
	var pl api.PlayLogEntry
	if err := db.Get(s, PlaylogBucket, key, &pl); err != nil {
		return nil, false
	}
	return &pl, true
}

func (db *DatabaseImpl) AppendPlayLog(s services.Session, entry *api.PlayLogEntry) error {
	key := fmt.Sprintf("%s/%s", entry.User, db.uniqueToken("pl"))
	return db.Put(s, PlaylogBucket, key, entry)
}

func (db *DatabaseImpl) GetMarkovMap(s services.Session, user string) (*api.MarkovMap, bool) {
	var m api.MarkovMap
	if err := db.Get(s, MarkovBucket, user, &m); err != nil {
		return nil, false
	}
	return &m, true
}

func (db *DatabaseImpl) PutMarkovMap(s services.Session, user string, m *api.MarkovMap) error {
	return db.Put(s, MarkovBucket, user, m)
}

func (db *DatabaseImpl) GetArtists(s services.Session, prefix string) ([]string, error) {
	if s == nil {
		var serr error
		if s, serr = db.client.NewSession(); serr != nil {
			return nil, serr
		}
		defer s.Close()
	}

	prefix = "artist/" + prefix
	iter, err := s.ScanKeys(StatsBucket, prefix, prefix+"\xff", -1)
	if err != nil {
		return nil, err
	}

	var out []string
	for {
		key, err := iter.Next(nil)
		if err != nil {
			if err != EOF {
				return nil, err
			}
			break
		}
		out = append(out, key[7:])
	}
	return out, nil
}

// Get exposes the generic 'get' method with optional session wrap.
func (db *DatabaseImpl) Get(s services.Session, bucket string, key string, obj interface{}) error {
	if s == nil {
		var serr error
		if s, serr = db.client.NewSession(); serr != nil {
			return serr
		}
		defer s.Close()
	}
	return s.Get(bucket, key, obj)
}

// Put exposes the generic 'put' method with optional session wrap.
func (db *DatabaseImpl) Put(s services.Session, bucket string, key string, obj interface{}) error {
	if s == nil {
		var serr error
		if s, serr = db.client.NewSession(); serr != nil {
			return serr
		}
		defer s.Close()
	}
	return s.Put(bucket, key, obj)
}

// Del exposes the generic 'del' method with optional session wrap.
func (db *DatabaseImpl) Del(s services.Session, bucket string, key string) error {
	if s == nil {
		var serr error
		if s, serr = db.client.NewSession(); serr != nil {
			return serr
		}
		defer s.Close()
	}
	return s.Del(bucket, key)
}

// Utility function to fetch a number of songs in parallel.
func ParallelFetchSongs(db services.Database, songIds []api.SongID) ([]*api.Song, error) {
	session, err := db.NewSession()
	if err != nil {
		return nil, err
	}
	defer session.Close()

	// Scatter/gather with limited concurrency (using a buffered
	// channel). Feed rch from a separate goroutine, as we need to
	// start reading from it before the "for" loop is complete.
	rch := make(chan *api.Song)
	go func() {
		var wg sync.WaitGroup
		ch := make(chan bool, defaultConcurrency)
		for _, id := range songIds {
			wg.Add(1)
			ch <- true
			go func(id api.SongID) {
				defer func() { <-ch }()
				defer wg.Done()
				if song, ok := db.GetSongWithoutDupes(session, id); ok {
					rch <- song
				}
			}(id)
		}

		// Final cleanup. At this point at most
		// defaultConcurrency goroutines are still running,
		// but we wait on wg to ensure that all results have
		// been properly written to rch.
		go func() {
			wg.Wait()
			close(ch)
			close(rch)
		}()
	}()

	// Read the results into a map, and then turn that into an
	// array so that we can preserve the order in 'songIds'.
	tempMap := make(map[api.SongID]*api.Song)
	for song := range rch {
		tempMap[song.Id] = song
	}
	result := make([]*api.Song, 0, len(tempMap))
	for _, id := range songIds {
		if song, ok := tempMap[id]; ok {
			result = append(result, song)
		}
	}
	return result, nil
}
