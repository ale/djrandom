package database

import (
	"encoding/json"
	"os"
	"testing"
	"time"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/services/database"
	"git.autistici.org/ale/djrandom/services/database/backend_leveldb"
)

var testSongId = [16]byte{0x58, 0xee, 0x09, 0x93, 0x7d, 0x51, 0xf3, 0x88, 0x6d, 0xcd, 0x0c, 0x0e, 0xa9, 0x1c, 0x05, 0x3e}

func createSong() *api.Song {
	af := api.AudioFile{
		MD5:        "aaabbbccc",
		Size:       1234,
		SampleRate: 44100,
		Channels:   2,
		Duration:   1.12,
		SongId:     testSongId,
	}
	return &api.Song{
		Id:    testSongId,
		Files: []*api.AudioFile{&af},
		Meta: api.Meta{
			Artist: "artist",
			Title:  "title",
			Album:  "an album",
			Year:   1970,
		},
		Info: api.SongInfo{
			State:     "ok",
			CreatedAt: time.Now().Unix(),
		},
	}
}

// Short-circuit the rpc client with a local Sqlite database.
// Implements the DatabaseClient interface.
type testLocalDatabaseClient struct {
	be *backend_leveldb.LevelDbBackend
}

// Session for the above.
type testLocalDatabaseSession struct {
	be *backend_leveldb.LevelDbBackend
}

func newTestLocalDatabaseClient() *testLocalDatabaseClient {
	return &testLocalDatabaseClient{
		backend_leveldb.NewLevelDbBackend(".testdb"),
	}
}

func (db *testLocalDatabaseClient) Close() {
	db.be.Close()
	os.RemoveAll(".testdb")
}

func (db *testLocalDatabaseClient) NewSession() (services.Session, error) {
	return &testLocalDatabaseSession{db.be}, nil
}

func (db *testLocalDatabaseSession) Close() {}

func (db *testLocalDatabaseSession) Get(bucket, key string, obj interface{}) error {
	data, err := db.be.Get(bucket, key)
	if err != nil {
		return err
	}
	return json.Unmarshal(data, obj)
}

func (db *testLocalDatabaseSession) Put(bucket, key string, obj interface{}) error {
	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}
	return db.be.Put(bucket, key, data)
}

func (db *testLocalDatabaseSession) Del(bucket, key string) error {
	return db.be.Del(bucket, key)
}

type testResultIterator struct {
	i database.BackendIterator
}

func (r testResultIterator) Next(obj interface{}) (string, error) {
	key, data, err := r.i.Next()
	if err != nil {
		return key, err
	}
	return key, json.Unmarshal(data, obj)
}

func (db *testLocalDatabaseSession) Scan(bucket, startKey, endKey string, limit int) (services.Iterator, error) {
	i, err := db.be.Scan(bucket, startKey, endKey, limit, false)
	if err != nil {
		return nil, err
	}
	return &testResultIterator{i}, nil
}

func (db *testLocalDatabaseSession) ScanKeys(bucket, startKey, endKey string, limit int) (services.Iterator, error) {
	return db.Scan(bucket, startKey, endKey, limit)
}

func TestDatabase_PutSong(t *testing.T) {
	client := newTestLocalDatabaseClient()
	defer client.Close()
	db := NewDatabaseImpl(client)

	song := createSong()
	err := db.PutSong(nil, song)
	if err != nil {
		t.Fatalf("PutSong() failed: %s", err.Error())
	}
}

func TestDatabase_ModifySong(t *testing.T) {
	client := newTestLocalDatabaseClient()
	defer client.Close()
	db := NewDatabaseImpl(client)

	song := createSong()
	err := db.PutSong(nil, song)
	if err != nil {
		t.Fatalf("PutSong() failed: %s", err.Error())
	}

	song, ok := db.GetSong(nil, testSongId)
	if !ok {
		t.Fatalf("GetSong() failed, not found")
	}

	// Add a couple of new AudioFiles to the song (it should be
	// autosaved to the db).
	newFiles := []*api.AudioFile{}
	newFiles = append(newFiles, &api.AudioFile{
		MD5:        "cccdddeee",
		Size:       2345,
		SampleRate: 44100,
		Channels:   2,
		Duration:   2.00,
		SongId:     testSongId,
	})
	newFiles = append(newFiles, &api.AudioFile{
		MD5:        "dddeeefff",
		Size:       3456,
		SampleRate: 44100,
		Channels:   2,
		Duration:   3.00,
		SongId:     testSongId,
	})

	song.Files = append(song.Files, newFiles...)

	err = db.PutSong(nil, song)
	if err != nil {
		t.Fatalf("PutSong() / 2 failed: %s", err.Error())
	}

	// Check that we can retrieve the new AudioFile.
	af2, ok := db.GetAudioFile(nil, "cccdddeee")
	if !ok {
		t.Fatalf("GetAudioFile() failed for new AudioFile")
	}
	if af2.Channels != 2 {
		t.Fatalf("Result of GetAudioFile() does not match")
	}
}
