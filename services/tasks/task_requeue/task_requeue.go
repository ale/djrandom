// Re-add orphaned tasks to the task_queue. The task_server should be
// restarted after this, to pick up the newly queued tasks.
package main

import (
	"flag"
	"log"

	"git.autistici.org/ale/djrandom/api/task_api"
	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/services/tasks"
	"git.autistici.org/ale/djrandom/util/config"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
)

const (
	tasksBucket = "tasks"
)

func main() {
	flag.Parse()
	config.Parse(*configFile)

	db := db_client.NewDatabaseImplFromConfig()
	tc := tasks.NewTaskClientFromConfig(db)
	session, err := db.NewSession()
	if err != nil {
		log.Fatal(err)
	}
	defer session.Close()

	iter, err := session.Scan(tasksBucket, "", "\xff", -1)
	if err != nil {
		log.Fatal(err)
	}

	count := 0
	for {
		var job task_api.Job
		_, err := iter.Next(&job)
		if err != nil {
			if err != db_client.EOF {
				log.Fatal(err)
			}
			break
		}

		if job.Final {
			continue
		}

		tc.Push(job.Id)
		count++
	}

	log.Printf("rescheduled %d tasks", count)
}
