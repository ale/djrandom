package main

import (
	"flag"
	"log"
	"time"

	db_client "git.autistici.org/ale/djrandom/services/database/client"
	"git.autistici.org/ale/djrandom/services/tasks"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/daemon"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
	addr       = flag.String("addr", ":3005", "TCP address to listen on.")
)

func main() {
	flag.Parse()
	config.MustParse(*configFile)

	daemon.Setup()

	db := db_client.NewDatabaseImplFromConfig()
	ts := tasks.NewTaskServer(db)
	ts.RegisterService()

	log.Printf("Starting task queue service on %s", *addr)
	err := util.ListenAndServeWithTimeout(*addr, nil, 1800*time.Second)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
