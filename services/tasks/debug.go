package tasks

import (
	"html/template"
	"log"
	"net/http"

	"git.autistici.org/ale/djrandom/api/task_api"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util/status"
)

const listHtml = `<html>
<body>
<h1>Tasks</h1>
<table>
 <tr>
  <th align="center">Job ID</th>
  <th align="center">Plan</th>
  <th align="center">Arg</th>
  <th align="center">State</th>
  <th></th>
 </tr>
{{range .}}
 <tr>
  <td><a href="/debug/tasks/show?job_id={{.Id}}">{{.Id}}</a></td>
  <td>{{.PlanName}}</td>
  <td>{{.Arg}}</td>
  <td>{{.State}}</td>
  <td>{{if .Final}}F{{end}} {{if .Error}}<span style="color:red">E</span>{{end}}</td>
 </tr>
{{end}}
</table>
</body>
</html>`

const taskHtml = `<html>
<body>
<h1>Task {{.Id}}</h1>
<p>
 Plan: {{.PlanName}}<br>
 Arg: {{.Arg}}<br>
 State: {{.State}}<br>
 Final: {{.Final}}<br>
 Error: {{.Error}}<br>
</p>
<h3>Log</h3>
<pre>{{range .Logs}}{{.Msg}}
{{end}}</pre>
</body>
</html>`

var (
	// Just keep a global Database instance for the debug console.
	globalDb services.Database

	// Templates
	taskTmpl = template.Must(template.New("Task").Parse(taskHtml))
	listTmpl = template.Must(template.New("Task list").Parse(listHtml))
)

func taskList(w http.ResponseWriter, r *http.Request) {
	session, err := globalDb.NewSession()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer session.Close()

	iter, err := session.Scan(tasksBucket, "", "\xff", 0)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	var jobs []*task_api.Job
	for {
		var job task_api.Job
		if _, err := iter.Next(&job); err != nil {
			break
		}
		jobs = append(jobs, &job)
	}

	err = listTmpl.Execute(w, jobs)
	if err != nil {
		log.Printf("template error: %s", err)
	}
}

func showTask(w http.ResponseWriter, r *http.Request) {
	session, err := globalDb.NewSession()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	defer session.Close()

	jobId := r.URL.Query().Get("job_id")

	var job task_api.Job
	if err := session.Get(tasksBucket, jobId, &job); err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	err = taskTmpl.Execute(w, &job)
	if err != nil {
		log.Printf("template error: %s", err)
	}
}

func setupDebugHandlers(db services.Database) {
	globalDb = db
	http.HandleFunc("/debug/tasks/show", showTask)
	http.HandleFunc("/debug/tasks", taskList)
}

func init() {
	status.RegisterDebugEndpoint("/debug/tasks")
}
