package tasks

import (
	"bytes"
	"fmt"
	"log"
	"sync"
	"time"

	"git.autistici.org/ale/djrandom/api/task_api"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util/instrumentation"
)

// StateMachine implements a state machine on top of a TaskClient
// interface (not directly the TaskClient class in this same package).
type StateMachine struct {
	Client  services.TaskClient
	Plans   map[string]*task_api.Plan
	Actions map[string]task_api.Action

	counters *instrumentation.Counter
}

func NewStateMachine(tc services.TaskClient, plans map[string]*task_api.Plan, actions map[string]task_api.Action) *StateMachine {
	return &StateMachine{
		Client:   tc,
		Plans:    plans,
		Actions:  actions,
		counters: instrumentation.NewCounter("state_machine", 1.0),
	}
}

func (sm *StateMachine) abortJob(j *task_api.Job, msg string, args ...interface{}) {
	errMsg := fmt.Sprintf(msg, args...)
	log.Printf("Job %s: %s", j.Id, errMsg)
	j.Log(errMsg)
	j.Error = true
	j.Final = true
	j.CompletedAt = time.Now().Unix()
}

func (sm *StateMachine) handleJob(j *task_api.Job) {
	// Don't fully trust the queue system...
	if j.Final {
		log.Printf("job %s is already final", j.Id)
		return
	}

	// Find and validate plan and action. If the data is not
	// consistent somehow, abort the job with an error.
	plan, ok := sm.Plans[j.PlanName]
	if !ok {
		// This should be unreachable by design (we won't be
		// listening for jobs on unknown plans).
		sm.abortJob(j, "Unknown plan '%s'", j.PlanName)
		sm.counters.IncVar("abort", 1)
		return
	}
	next, ok := plan.M[j.State]
	if !ok {
		sm.abortJob(j, "Undefined action '%s' in plan '%s'", j.State, j.PlanName)
		sm.counters.IncVar("abort", 1)
		return
	}
	handler, ok := sm.Actions[j.State]
	if !ok {
		sm.abortJob(j, "Unknown action '%s'", j.State)
		sm.counters.IncVar("abort", 1)
		return
	}

	// Run the action and move to the next state. Provide a logger
	// to collect debugging output for the job and save them to
	// the database.
	var buf bytes.Buffer
	var nextState string
	err := handler.Handle(j.Arg, &buf)
	if err != nil {
		sm.counters.IncVar("errors", 1)
		sm.counters.IncVar("state."+j.State+".errors", 1)
		j.Error = true
		j.Log(fmt.Sprintf("Error: %s", err))
		nextState = next.Err
	} else {
		sm.counters.IncVar("ok", 1)
		sm.counters.IncVar("state."+j.State+".ok", 1)
		j.Error = false
		nextState = next.Ok
	}
	if buf.Len() > 0 {
		j.Log(buf.String())
	}

	// Is the new state final?
	if nextState == "" {
		sm.counters.IncVar("done", 1)
		j.Final = true
		j.CompletedAt = time.Now().Unix()
	}

	// Add a log entry summarizing the transaction.
	log.Printf("%s: %s -> %s (%q %q)", j.Id, j.State, nextState, j.Error, j.Final)
	j.Log(fmt.Sprintf("%s -> %s", j.State, nextState))

	// Set the new job state.
	j.State = nextState
}

func (sm *StateMachine) Run(nWorkers int) {
	var wg sync.WaitGroup

	for i := 0; i < nWorkers; i++ {
		wg.Add(1)
		go sm.Client.Poll(func(job *task_api.Job) {
			sm.handleJob(job)
		})
	}

	// Wait forever.
	wg.Wait()
}
