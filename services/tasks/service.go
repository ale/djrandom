// A persistent task queue server.
//
// This is not a very good implementation, but it should be reliable
// (the state is kept in the database), and it should be able to
// handle a large number of queued tasks.
//
package tasks

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"sync"
	"time"

	"github.com/gorilla/mux"

	"git.autistici.org/ale/djrandom/services"
)

const (
	batchSize        = 100
	tasksQueueBucket = "tasks_queue"
)

var (
	idCounter     = 0
	idCounterLock sync.Mutex
)

func uniqueId() string {
	idCounterLock.Lock()
	idCounter++
	defer idCounterLock.Unlock()
	return fmt.Sprintf("%020d.%d", time.Now().Unix(), idCounter)
}

type msgPair struct {
	Key string
	Msg string
}

type TaskServer struct {
	db     services.Database
	fanout chan msgPair
	ready  chan bool
}

func NewTaskServer(db services.Database) *TaskServer {
	return &TaskServer{
		db:     db,
		fanout: make(chan msgPair),
		ready:  make(chan bool, 1),
	}
}

func (ts *TaskServer) setReady() {
	select {
	case ts.ready <- true:
	default:
	}
}

func (ts *TaskServer) scan() int {
	s, err := ts.db.NewSession()
	if err != nil {
		return 0
	}
	defer s.Close()
	iter, err := s.Scan(tasksQueueBucket, "", "\xff", batchSize)
	if err != nil {
		return 0
	}
	count := 0
	for {
		var msg string
		key, err := iter.Next(&msg)
		if err != nil {
			break
		}
		log.Printf("scan() -> {%s, %s}", key, msg)
		ts.fanout <- msgPair{key, msg}
		count++
	}
	log.Printf("scan() complete, %d results", count)
	return count
}

func (ts *TaskServer) scanLoop() {
	for {
		n := ts.scan()
		if n == 0 {
			// Wait until ready.
			<-ts.ready
		}
	}
}

func (ts *TaskServer) Push(w http.ResponseWriter, r *http.Request) {
	msg := r.URL.Query().Get("msg")
	if msg == "" {
		http.Error(w, "Bad Request", http.StatusBadRequest)
		return
	}

	log.Printf("Push(%s)", msg)

	key := uniqueId()
	err := ts.db.Put(nil, tasksQueueBucket, key, msg)
	if err != nil {
		log.Printf("Push(%s): Put() error: %s", msg, err)
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	ts.setReady()
	w.WriteHeader(200)
}

func (ts *TaskServer) Poll(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/plain")

	// We don't want to wait forever because if the client disconnects
	// we will queue up an unlimited amount of goroutines waiting on
	// ts.fanout, which will trigger requeues when data becomes
	// available. To mitigate this, force a timeout.
	timeout := time.NewTimer(600 * time.Second)
	defer timeout.Stop()
	select {
	case item := <-ts.fanout:
		if _, err := io.WriteString(w, item.Msg); err != nil {
			log.Printf("Error sending message to %s: %s", r.RemoteAddr, err)
			// Make sure that the scanner re-examines the queue.
			ts.setReady()
			return
		}

		// If we got no errors here, the client most likely did
		// get the message. Remove it from the queue.
		log.Printf("de-queuing %s", item.Msg)
		ts.db.Del(nil, tasksQueueBucket, item.Key)

	case <-timeout.C:
		// Send an empty response.
		w.Header().Set("Content-Length", "0")
		w.WriteHeader(200)
	}
}

func (ts *TaskServer) RegisterService() {
	go ts.scanLoop()

	setupDebugHandlers(ts.db)

	r := mux.NewRouter()
	r.HandleFunc("/tq/push", func(w http.ResponseWriter, r *http.Request) {
		ts.Push(w, r)
	})
	r.HandleFunc("/tq/poll", func(w http.ResponseWriter, r *http.Request) {
		ts.Poll(w, r)
	})
	http.Handle("/tq/", r)
}
