package tasks

import (
	"errors"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"git.autistici.org/ale/djrandom/api/task_api"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util/config"
)

var (
	RemoteError = errors.New("Remote error")

	taskServerAddr = config.String("task_server_addr", "", "Address of the task server")
)

const (
	tasksBucket = "tasks"
)

type TaskClient struct {
	addr   string
	db     services.Database
	client http.Client
}

func NewTaskClient(addr string, db services.Database) *TaskClient {
	return &TaskClient{
		addr: addr,
		db:   db,
	}
}

func NewTaskClientFromConfig(db services.Database) *TaskClient {
	return NewTaskClient(config.MustString("task_server_addr", *taskServerAddr), db)
}

// Run 'fn' within a job transaction.
func (t *TaskClient) runWithJob(fn func(*task_api.Job), jobId string) {
	job, err := t.GetJob(jobId)
	if err != nil {
		log.Printf("runWithJob: Get(%s) error: %s", jobId, err)
		return
	}

	fn(job)

	if err := t.PutJob(job); err != nil {
		log.Printf("runWithJob: Put(%s) error: %s", jobId, err)
	}
	if !job.Final {
		// Queue job again.
		go t.Push(jobId)
	}
}

// Poll loops forever, waiting until an event is available, and
// calling 'fn' with it.
func (t *TaskClient) Poll(fn func(*task_api.Job)) {
	retryTime := 5 * time.Second
	pollUrl := fmt.Sprintf("%s/tq/poll", t.addr)
	for {
		log.Printf("connecting to %s", pollUrl)
		res, err := t.client.Get(pollUrl)
		if err != nil {
			log.Printf("temporary error, retrying... (%s)", err)
			time.Sleep(retryTime)
			continue
		}

		buf, err := ioutil.ReadAll(res.Body)
		res.Body.Close()
		if err != nil {
			log.Printf("error reading response (%s)", err)
			time.Sleep(retryTime)
			continue
		}
		payload := string(buf)
		if payload == "" {
			time.Sleep(retryTime)
			continue
		}

		log.Printf("Poll() -> %s", payload)
		t.runWithJob(fn, payload)
	}
}

// Push enqueues an event.
func (t *TaskClient) Push(payload string) error {
	log.Printf("Push(%s)", payload)

	pushUrl := fmt.Sprintf("%s/tq/push?msg=%s", t.addr, payload)
	resp, err := t.client.Get(pushUrl)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		return RemoteError
	}
	return nil
}

// PutJob saves a job to persistent storage.
func (t *TaskClient) PutJob(job *task_api.Job) error {
	return t.db.Put(nil, tasksBucket, job.Id, job)
}

// GetJob retrieves a job from persistent storage.
func (t *TaskClient) GetJob(jobId string) (*task_api.Job, error) {
	var job task_api.Job
	if err := t.db.Get(nil, tasksBucket, jobId, &job); err != nil {
		return nil, err
	}
	return &job, nil
}

// CreateJob creates a new job and puts it in the queue.
func (t *TaskClient) CreateJob(plan *task_api.Plan, arg string) error {
	job := task_api.NewJob(plan, arg)

	if err := t.PutJob(job); err != nil {
		return err
	}
	if err := t.Push(job.Id); err != nil {
		return err
	}

	log.Printf("created job %s (plan %s)", job.Id, plan.Name)
	return nil
}
