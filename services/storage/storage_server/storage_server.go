// Server that implements the Storage API.

package main

import (
	"flag"
	"log"
	"net/rpc"
	"time"

	"git.autistici.org/ale/djrandom/partition"
	"git.autistici.org/ale/djrandom/services/storage"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/daemon"
)

var (
	configFile  = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
	addr        = flag.String("addr", ":3003", "TCP address to listen on.")
	storageRoot = config.String("storage_root", "", "Root directory for on-disk storage")
	storageSelf = config.String("storage_self", "", "Public address of this node (host:port)")
)

func main() {
	flag.Parse()
	config.MustParse(*configFile)

	daemon.Setup()

	storagePmap := storage.NewPartitionMapFromConfig()
	storageAuthKey := storage.NewAuthKeyFromConfig()

	// Storage service.
	svc := storage.NewStorageService(
		config.MustString("storage_root", *storageRoot),
		storageAuthKey,
		storagePmap,
		config.MustString("storage_self", *storageSelf),
	)

	// Start background rebalancing worker.
	partition.RegisterStatsService(svc)
	go partition.RunRebalancer(svc, time.Second*900)

	storage.RegisterService(svc)

	// Start the RPC HTTP handler for debug functions.
	rpc.HandleHTTP()

	log.Printf("Starting storage server on %s", *addr)
	err := util.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
