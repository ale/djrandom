package storage

import (
	"fmt"
	"io"
	"net/http"
	"net/url"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/partition"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
)

var (
	storagePmap      = config.String("storage_pmap", "/etc/djrandom/part-storage.json", "Storage partition map file")
	storageKeyId     = config.String("storage_key_id", "", "API Key ID for storage authentication")
	storageKeySecret = config.String("storage_key_secret", "", "API Key secret for storage authentication")
)

type authHttpClient struct {
	http.Client
	authKey *api.AuthKey
}

func newAuthHttpClient(authKey *api.AuthKey) *authHttpClient {
	c := &authHttpClient{
		http.Client{
			Transport: &http.Transport{
				DisableCompression: true,
			},
		},
		authKey,
	}
	return c
}

func (c *authHttpClient) SignRequest(r *http.Request) {
	util.SignHttpRequest(c.authKey, r)
}

type DistributedStorageClient struct {
	// The partition map.
	P *partition.PartitionMap

	// HTTP client.
	client *authHttpClient
}

func NewPartitionMapFromConfig() *partition.PartitionMap {
	pmap := partition.MustLoadPartitionMap(
		config.MustString("storage_pmap", *storagePmap))
	partition.Register(pmap, "storage")
	return pmap
}

func NewAuthKeyFromConfig() *api.AuthKey {
	return &api.AuthKey{
		KeyId:  config.MustString("storage_key_id", *storageKeyId),
		Secret: config.MustString("storage_key_secret", *storageKeySecret),
	}
}

func NewDistributedStorageClient(pmap *partition.PartitionMap, authKey *api.AuthKey) *DistributedStorageClient {
	return &DistributedStorageClient{
		P:      pmap,
		client: newAuthHttpClient(authKey),
	}
}

func NewDistributedStorageClientFromConfig() *DistributedStorageClient {
	return NewDistributedStorageClient(
		NewPartitionMapFromConfig(),
		NewAuthKeyFromConfig())
}

// GetUrl returns the URL where a blob can be accessed.
func (c *DistributedStorageClient) GetUrl(key, mode string) string {
	return fmt.Sprintf("%s/storage/%s/%s", c.P.GetTarget(key), mode, key)
}

// Write saves a blob of data to the distributed storage.
func (c *DistributedStorageClient) Write(key string, r io.Reader) error {
	req, err := http.NewRequest("PUT", c.GetUrl(key, "w"), r)
	if err != nil {
		return err
	}

	c.client.SignRequest(req)
	resp, err := c.client.Do(req)
	if err != nil {
		return err
	}

	// Ignore the response body but dispose of it properly.
	defer resp.Body.Close()

	return nil
}

// Open returns a ReadCloser object that yields blob data.
func (c *DistributedStorageClient) Open(key string) (io.ReadCloser, error) {
	req, err := http.NewRequest("GET", c.GetUrl(key, "r"), nil)
	if err != nil {
		return nil, err
	}

	c.client.SignRequest(req)
	resp, err := c.client.Do(req)
	if err != nil {
		return nil, err
	}

	return resp.Body, nil
}

// ReverseProxyDirector gives you a function to be used as the
// director of an httputil.ReverseProxy (providing authenticated
// access to the read-only tree).
func (c *DistributedStorageClient) ReverseProxyDirector() func(string, *http.Request) {
	return func(key string, req *http.Request) {
		// Remove incoming authorization headers.
		req.Header.Del("Authorization")

		req.URL, _ = url.Parse(c.GetUrl(key, "r"))
		c.client.SignRequest(req)
	}
}
