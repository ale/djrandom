package storage

import (
	"fmt"
	"io"
	"os"
	"path/filepath"
)

// The filesystem-based storage backend, implemented using directory
// hashing of the key.
type FileSystemStorage struct {
	Root      string
	HashDepth int
}

func NewFileSystemStorage(root string, hashDepth int) *FileSystemStorage {
	return &FileSystemStorage{
		Root:      root,
		HashDepth: hashDepth,
	}
}

func (fs *FileSystemStorage) getPath(key string) string {
	parts := []string{fs.Root}
	for i := 0; i < fs.HashDepth; i++ {
		parts = append(parts, fmt.Sprintf("%c", key[i]))
	}
	base := filepath.Join(parts...)

	// Ensure that the containing directory exists.
	stat, err := os.Stat(base)
	if err != nil || !stat.IsDir() {
		// Silently ignore errors here (will get them anyway
		// when we're actually trying to access the file).
		os.MkdirAll(base, 0700)
	}

	return filepath.Join(base, key)
}

// Write copies data into the filesystem.
func (fs *FileSystemStorage) Write(key string, r io.Reader) error {
	file, err := os.Create(fs.getPath(key))
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = io.Copy(file, r)
	return err
}

// Open returns a ReadCloser pointing at the file contents.
func (fs *FileSystemStorage) Open(key string) (*os.File, error) {
	return os.Open(fs.getPath(key))
}

// Delete removes a file from the local filesystem.
func (fs *FileSystemStorage) Delete(key string) {
	os.Remove(fs.getPath(key))
}

// Scan the local filesystem for files and write keys to a channel.
func (fs *FileSystemStorage) Scan(out chan string) error {
	return filepath.Walk(fs.Root, func(path string, info os.FileInfo, err error) error {
		if err == nil && !info.IsDir() {
			out <- info.Name()
		}
		return nil
	})
}
