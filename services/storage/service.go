// The storage package implements a simple distributed blob store.
//
// It's a very simple blob store that expects hex-encoded keys and uses
// the filesystem as the backend. It implements the PartitionedService
// interface.
//
// It offers a simple HTTP API (with HMAC authentication using a
// shared secret), which supports PUT and GET operations on keys. Read
// operations actually support the full feature set of Go's
// http.FileServer, including caching headers and ranged
// requests. These are not exposed in the DistributedStorageClient
// API, but are rather meant for directly proxying the blob store to
// clients.
//
package storage

import (
	"errors"
	"io"
	"log"
	"net/http"
	"os"
	"path"

	"github.com/gorilla/mux"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/partition"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/instrumentation"
)

type StorageService struct {
	partition.PartitionedServiceBase

	// Authentication credentials.
	AuthKey *api.AuthKey

	// The backend filesystem-based storage.
	Fs *FileSystemStorage

	// Client interface for rebalancing.
	Client *DistributedStorageClient

	counters *instrumentation.Counter
}

func NewStorageService(fsRoot string, authKey *api.AuthKey, pmap *partition.PartitionMap, selfTarget string) *StorageService {
	return &StorageService{
		partition.PartitionedServiceBase{
			Pmap:       pmap,
			SelfTarget: selfTarget,
		},
		authKey,
		NewFileSystemStorage(fsRoot, 2),
		NewDistributedStorageClient(pmap, authKey),
		instrumentation.NewCounter("rpc", 1),
	}
}

func (ss *StorageService) Scan() <-chan string {
	ch := make(chan string)

	go func() {
		// Walk the local filesystem and yield all keys
		if err := ss.Fs.Scan(ch); err != nil {
			log.Printf("Error scanning local filesystem: %s", err)
		}
		close(ch)
	}()

	return ch
}

func (ss *StorageService) Move(key string) error {
	file, err := ss.Fs.Open(key)
	if err != nil {
		return err
	}
	defer file.Close()
	if err := ss.Client.Write(key, file); err != nil {
		return err
	}
	ss.Fs.Delete(key)
	return nil
}

// This type implements the http.File interface.
type plainFile struct {
	*os.File
}

func (p plainFile) Readdir(count int) ([]os.FileInfo, error) {
	return nil, errors.New("Not a directory")
}

// Open implements the http.FileSystem interface.
func (ss *StorageService) Open(name string) (http.File, error) {
	key := path.Base(name)
	log.Printf("Open(%s)", key)
	file, err := ss.Fs.Open(key)
	if err != nil {
		return nil, err
	}
	return plainFile{file}, nil
}

func (ss *StorageService) HandlerFunc(f func(*StorageService, http.ResponseWriter, *http.Request)) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		f(ss, w, r)
	})
}

func auth(ss *StorageService, h http.Handler) http.Handler {
	// Compare the authentication key against the shared one.
	authFn := func(key string) (*api.AuthKey, bool) {
		if key == ss.AuthKey.KeyId {
			return ss.AuthKey, true
		}
		return nil, false
	}

	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if _, err := util.AuthenticateHttpRequest(authFn, r); err != nil {
			switch err {
			case util.Unauthorized:
				http.Error(w, err.Error(), 401)
			case util.AuthFailed:
				http.Error(w, err.Error(), 403)
			default:
				http.Error(w, err.Error(), 400)
			}
			return
		}
		h.ServeHTTP(w, r)
	})
}

func rpcStats(ss *StorageService, op string, h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.ServeHTTP(w, r)
		ss.counters.IncVar(op, 1)
	})
}

func Write(ss *StorageService, w http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	key := vars["key"]
	log.Printf("Write(%s)", key)
	err := ss.Fs.Write(key, req.Body)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	io.WriteString(w, "ok")
}

func RegisterService(ss *StorageService) {
	r := mux.NewRouter()
	r.Handle("/storage/w/{key}", rpcStats(ss, "write", ss.HandlerFunc(Write)))
	r.Handle("/storage/r/{key}",
		rpcStats(ss, "read",
			http.StripPrefix("/storage/r/",
				http.FileServer(ss))))
	http.Handle("/storage/", auth(ss, r))
}
