package index

import (
	"testing"
)

func stqCompareSlices(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i := 0; i < len(a); i++ {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func testQuery(t *testing.T, query, expected string) {
	ast := StringToQuery(query)
	if ast == nil {
		t.Fatalf("nil ast for: '%s'", query)
	}

	s := ast.String()
	if s != expected {
		t.Fatalf("query: '%s', result: '%s', expected: '%s'", query, s, expected)
	}
}

func TestStringToQuery_Default(t *testing.T) {
	testQuery(t, "chet", "(artist:\"chet\") OR (album:\"chet\") OR (title:\"chet\")")
}

func TestStringToQuery_Default_TwoWords(t *testing.T) {
	testQuery(t, "chet baker", "((artist:\"chet\") OR (album:\"chet\") OR (title:\"chet\")) AND ((artist:\"baker\") OR (album:\"baker\") OR (title:\"baker\"))")
}

func TestStringToQuery_Keyword(t *testing.T) {
	testQuery(t, "artist:\"chet baker\"", "(artist:\"chet\") AND (artist:\"baker\")")
}

func TestStringToQuery_Mixed(t *testing.T) {
	testQuery(t, "tokyo artist:\"chet baker\"", "((artist:\"tokyo\") OR (album:\"tokyo\") OR (title:\"tokyo\")) AND (artist:\"chet\") AND (artist:\"baker\")")
}

func TestStringToQuery_Tokenizer(t *testing.T) {
	testQuery(t, "artist:\"F. de André\"", "(artist:\"de\") AND (artist:\"andre\")")
	testQuery(t, "artist:andrè", "artist:\"andre\"")
}
