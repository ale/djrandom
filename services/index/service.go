package index

import (
	"log"
	"net/rpc"
	"time"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/partition"
	"git.autistici.org/ale/djrandom/util/instrumentation"
)

type IndexService struct {
	partition.PartitionedServiceBase

	// The in-memory multi-index object.
	Index *MultiIndex

	// Client for rebalancing.
	Client *IndexClient

	// Quit channel for the background updater.
	quit chan bool
}

// Once in a while, dump the index state to disk.
func periodicSaveWorker(idx *MultiIndex, stateFile string, quit chan bool) {
	t := time.NewTicker(time.Second * 300)
	defer t.Stop()

	save := func() {
		if err := idx.Save(stateFile); err != nil {
			log.Printf("Error saving index: %s", err)
		}
	}

	for {
		select {
		case <-t.C:
			save()
		case <-quit:
			save()
			return
		}
	}
}

func NewIndexService(stateFile string, pmap *partition.PartitionMap, selfTarget string) *IndexService {
	idx, err := LoadMultiIndex(stateFile)
	if err != nil {
		log.Printf("Warning: could not read index from %s (%s), creating empty index", stateFile, err)
		idx = NewMultiIndex()
	} else {
		log.Printf("Loaded %d documents from %s", len(idx.D), stateFile)
	}

	quit := make(chan bool, 1)
	go periodicSaveWorker(idx, stateFile, quit)

	return &IndexService{
		partition.PartitionedServiceBase{
			Pmap:       pmap,
			SelfTarget: selfTarget,
		},
		idx,
		NewIndexClient(pmap),
		quit,
	}
}

func (is *IndexService) Stop() {
	is.quit <- true
}

func (is *IndexService) Scan() <-chan string {
	ch := make(chan string)
	idCh := make(chan api.SongID)

	go func() {
		for id := range idCh {
			ch <- id.String()
		}
		close(ch)
	}()

	go func() {
		is.Index.Scan(idCh)
		close(idCh)
	}()

	return ch
}

func (is *IndexService) Move(key string) error {
	id, err := api.ParseSongID(key)
	if err != nil {
		return err
	}

	doc, ok := is.Index.D[id]
	if !ok {
		return nil
	}

	if err = is.Client.AddDoc(id, doc); err != nil {
		return err
	}

	is.Index.DeleteDoc(id)

	return nil
}

// RPC Interface

// Just a wrapper that only exposes RPC methods.
type IndexServiceRPC struct {
	is       *IndexService
	counters *instrumentation.Counter
}

func newIndexServiceRPC(is *IndexService) *IndexServiceRPC {
	return &IndexServiceRPC{
		is:       is,
		counters: instrumentation.NewCounter("rpc", 1),
	}
}

type AddDocRequest struct {
	DocId api.SongID
	Doc   api.Document
}

type DeleteDocRequest struct {
	DocId api.SongID
}

type DeleteDocResponse struct{}

type SearchRequest struct {
	Query string
}

type SearchResponse struct {
	Results api.SongSet
}

type ScanRequest struct{}

type ScanResponse struct {
	DocIds []api.SongID
}

func (isrpc *IndexServiceRPC) AddDoc(req *AddDocRequest, resp *bool) error {
	defer isrpc.counters.IncVar("add", 1)
	*resp = true
	return isrpc.is.Index.AddDoc(req.DocId, req.Doc)
}

func (isrpc *IndexServiceRPC) DeleteDoc(req *DeleteDocRequest, resp *DeleteDocResponse) error {
	defer isrpc.counters.IncVar("delete", 1)
	isrpc.is.Index.DeleteDoc(req.DocId)
	return nil
}

func (isrpc *IndexServiceRPC) Search(req *SearchRequest, resp *SearchResponse) error {
	defer isrpc.counters.IncVar("search", 1)
	resp.Results = isrpc.is.Index.Search(req.Query)
	return nil
}

func (isrpc *IndexServiceRPC) Scan(req *ScanRequest, resp *ScanResponse) error {
	defer isrpc.counters.IncVar("scan", 1)
	resp.DocIds = isrpc.is.Index.GetDocIds()
	return nil
}

func RegisterService(is *IndexService) {
	rpc.RegisterName("IndexService", newIndexServiceRPC(is))
	rpc.HandleHTTP()
}
