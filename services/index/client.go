package index

import (
	"log"
	"sync"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/partition"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/rrpc"
)

var (
	IndexPmapFile = config.String("index_pmap", "/etc/djrandom/part-index.json", "Index partition map file")
)

// IndexClient is the distributed index client stub.
type IndexClient struct {
	P           *partition.PartitionMap
	clientCache map[string]*rrpc.Client
	cacheLock   sync.Mutex
}

func NewPartitionMapFromConfig() *partition.PartitionMap {
	pmap := partition.MustLoadPartitionMap(
		config.MustString("index_pmap", *IndexPmapFile))
	partition.Register(pmap, "index")
	return pmap
}

func NewIndexClient(pmap *partition.PartitionMap) *IndexClient {
	return &IndexClient{
		P:           pmap,
		clientCache: make(map[string]*rrpc.Client),
	}
}

func NewIndexClientFromConfig() *IndexClient {
	return NewIndexClient(NewPartitionMapFromConfig())
}

func (ic *IndexClient) connect(target string) *rrpc.Client {
	ic.cacheLock.Lock()
	defer ic.cacheLock.Unlock()
	if client, ok := ic.clientCache[target]; ok {
		return client
	}
	client := rrpc.New(target, nil)
	ic.clientCache[target] = client
	return client
}

// AddDoc adds a Document to the index.
func (ic *IndexClient) AddDoc(id api.SongID, doc api.Document) error {
	client := ic.connect(ic.P.GetTarget(id.String()))
	defer client.Close()

	ar := &AddDocRequest{
		DocId: id,
		Doc:   doc,
	}
	var resp bool
	return client.Call("IndexService.AddDoc", ar, &resp, nil)
}

// DeleteDoc removes a document from the index, given its ID.
func (ic *IndexClient) DeleteDoc(id api.SongID) error {
	client := ic.connect(ic.P.GetTarget(id.String()))
	defer client.Close()

	req := &DeleteDocRequest{DocId: id}
	var resp DeleteDocResponse
	return client.Call("IndexService.DeleteDoc", req, &resp, nil)
}

// Search runs a query against the index and returns a set of document IDs.
func (ic *IndexClient) Search(query string) api.SongSet {
	result := make(api.SongSet)
	ch := make(chan api.SongSet, 5)
	var wg sync.WaitGroup

	for _, t := range ic.P.AllTargets() {
		wg.Add(1)
		go func(target string) {
			defer wg.Done()

			client := ic.connect(target)
			defer client.Close()

			req := &SearchRequest{Query: query}
			var resp SearchResponse
			if err := client.Call("IndexService.Search", req, &resp, nil); err != nil {
				log.Printf("Remote error in distributed search on target %s: %s", target, err)
				return
			}
			ch <- resp.Results
		}(t)
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	for partial := range ch {
		result.Update(partial)
	}
	return result
}

// Scan returns a list of all the document IDs contained in the index
// (warning: potentially a very large list!).
func (ic *IndexClient) Scan() []api.SongID {
	var ids []api.SongID
	ch := make(chan []api.SongID, 5)
	var wg sync.WaitGroup

	for _, t := range ic.P.AllTargets() {
		wg.Add(1)
		go func(target string) {
			defer wg.Done()
			client := ic.connect(target)
			defer client.Close()

			req := &ScanRequest{}
			var resp ScanResponse
			if err := client.Call("IndexService.Scan", req, &resp, nil); err == nil {
				ch <- resp.DocIds
			}
		}(t)
	}

	go func() {
		wg.Wait()
		close(ch)
	}()

	for partial := range ch {
		ids = append(ids, partial...)
	}
	return ids
}
