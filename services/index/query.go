package index

import (
	"fmt"
	"log"
	"regexp"
	"strings"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/util/tokenizer"
)

// Interface from the index implementation to the query engine.
type QueryIndex interface {
	Lookup(key, value string) api.SongSet
}

// Query AST.
type QueryAstNode interface {
	Eval(QueryIndex) api.SongSet
	String() string
}

// AST node for an index search (key=value).
type querySearchNode struct {
	Key   string
	Value string
}

func (node *querySearchNode) Eval(index QueryIndex) api.SongSet {
	return index.Lookup(node.Key, node.Value)
}

func (node *querySearchNode) String() string {
	return fmt.Sprintf("%s:\"%s\"", node.Key, node.Value)
}

func NewQuerySearchNode(key string, value string) QueryAstNode {
	return &querySearchNode{key, value}
}

func NewQuerySearchNodeOnManyKeys(keys []string, value string) QueryAstNode {
	var args []QueryAstNode
	for _, key := range keys {
		args = append(args, NewQuerySearchNode(key, value))
	}
	return NewQueryOrNode(args)
}

// AST node base type for a boolean operator.
type queryOpNode struct {
	Args []QueryAstNode
}

func boolToString(oper string, args []QueryAstNode) string {
	sep := fmt.Sprintf(") %s (", oper)
	var strs []string
	for _, a := range args {
		if a == nil {
			strs = append(strs, "<nil>")
		} else {
			strs = append(strs, a.String())
		}
	}
	return fmt.Sprintf("(%s)", strings.Join(strs, sep))
}

// AST node for the AND operator.
type queryAndNode queryOpNode

func (node *queryAndNode) Eval(index QueryIndex) api.SongSet {
	result := node.Args[0].Eval(index)
	for _, arg := range node.Args[1:] {
		result = result.Intersect(arg.Eval(index))
	}
	return result
}

func (node *queryAndNode) String() string {
	return boolToString("AND", node.Args)
}

func NewQueryAndNode(args []QueryAstNode) QueryAstNode {
	return &queryAndNode{args}
}

// AST node for the OR operator.
type queryOrNode queryOpNode

func (node *queryOrNode) Eval(index QueryIndex) api.SongSet {
	result := node.Args[0].Eval(index)
	for _, arg := range node.Args[1:] {
		result.Update(arg.Eval(index))
	}
	return result
}

func (node *queryOrNode) String() string {
	return boolToString("OR", node.Args)
}

func NewQueryOrNode(args []QueryAstNode) QueryAstNode {
	return &queryOrNode{args}
}

var queryPrefixRegexp = regexp.MustCompile("([a-z0-9]+:)?(\"[^\"]*\"|[\\pL\\pN]*)")
var queryDefaultIndexes = []string{"artist", "album", "title"}

// StringToQuery parses a query string and builds a search query.
func StringToQuery(s string) QueryAstNode {
	var qnodes []QueryAstNode

	matches := queryPrefixRegexp.FindAllStringSubmatch(s, -1)
	for _, match := range matches {
		field := match[1]
		token := strings.Trim(match[2], "\"")

		// If the token does not start with '=', we need to
		// split it further and process it with the standard
		// indexing tokenizer.
		var tokens []string
		if strings.HasPrefix(token, "=") {
			tokens = []string{token}
		} else {
			tokens = tokenizer.Tokenize(token)
		}

		for _, t := range tokens {
			var qnode QueryAstNode
			if field == "" {
				qnode = NewQuerySearchNodeOnManyKeys(queryDefaultIndexes, t)
			} else {
				qnode = NewQuerySearchNode(strings.TrimRight(field, ":"), t)
			}
			qnodes = append(qnodes, qnode)
		}
	}

	if len(qnodes) == 0 {
		log.Printf("StringToQuery(%s): nil", s)
		return nil
	} else if len(qnodes) == 1 {
		return qnodes[0]
	}
	ast := NewQueryAndNode(qnodes)
	log.Printf("StringToQuery(%s): %s", s, ast.String())
	return ast
}
