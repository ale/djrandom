package main

import (
	"flag"
	"log"
	"time"

	"git.autistici.org/ale/djrandom/partition"
	"git.autistici.org/ale/djrandom/services/index"
	"git.autistici.org/ale/djrandom/util"
	"git.autistici.org/ale/djrandom/util/config"
	"git.autistici.org/ale/djrandom/util/daemon"
)

var (
	configFile = flag.String("config", "/etc/djrandom/server.conf", "Config file location")
	addr       = flag.String("addr", ":3002", "TCP address to listen on")
	indexFile  = config.String("index_file", "", "Location of the index state file")
	indexSelf  = config.String("index_self", "", "Public address of this node (host:port)")
)

func main() {
	flag.Parse()
	config.MustParse(*configFile)

	daemon.Setup()

	indexPmap := index.NewPartitionMapFromConfig()
	svc := index.NewIndexService(
		config.MustString("index_file", *indexFile),
		indexPmap,
		config.MustString("index_self", *indexSelf),
	)

	// Start rebalance worker and stats collector.
	partition.RegisterStatsService(svc)
	go partition.RunRebalancer(svc, time.Second*900)

	index.RegisterService(svc)

	log.Printf("Starting index server on %s", *addr)
	err := util.ListenAndServe(*addr, nil)
	if err != nil {
		log.Fatal("ListenAndServe: ", err)
	}
}
