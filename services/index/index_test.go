package index

import (
	"fmt"
	"os"
	"testing"

	"git.autistici.org/ale/djrandom/api"
)

var (
	testSongId  = [16]byte{0x58, 0xee, 0x09, 0x93, 0x7d, 0x51, 0xf3, 0x88, 0x6d, 0xcd, 0x0c, 0x0e, 0xa9, 0x1c, 0x05, 0x3e}
	testSongId2 = [16]byte{0x59, 0xef, 0x10, 0x94, 0x7e, 0x52, 0xf4, 0x89, 0x6e, 0xce, 0x0d, 0x0f, 0xb0, 0x1d, 0x06, 0x3f}
	testSongId3 = [16]byte{0x60, 0xf0, 0x11, 0x95, 0x7f, 0x53, 0xf5, 0x90, 0x6f, 0xcf, 0x0e, 0x10, 0xb1, 0x1e, 0x07, 0x40}
)

func TestIndex_Add(t *testing.T) {
	i := NewIndex("test")
	i.Add("key1", testSongId)
	i.Add("key1", testSongId2)
	i.Add("key2", testSongId2)

	result, ok := i.Get("key1")
	if !ok {
		t.Fatalf("Get(key1) not found")
	}
	if len(result) != 2 {
		t.Fatalf("Unexpected number of results for Get(key1): %d", len(result))
	}
}

func TestMultiIndex_AddDoc(t *testing.T) {
	srv := NewMultiIndex()
	doc := make(api.Document)
	doc.Add("index1", "v1")
	doc.Add("index2", "v2")
	srv.AddDoc(testSongId, doc)
}

func BenchmarkMultiIndex_AddDoc(b *testing.B) {
	srv := NewMultiIndex()
	for i := 0; i < b.N; i++ {
		value := fmt.Sprintf("%d", i)
		docId, _ := api.ParseSongID(fmt.Sprintf("%032X", i))
		doc := make(api.Document)
		doc.Add("index1", "all")
		doc.Add("index2", value)
		srv.AddDoc(docId, doc)
	}
}

func TestMultiIndex_Delete(t *testing.T) {
	srv := NewMultiIndex()
	doc1 := make(api.Document)
	doc1.Add("index1", "v1")
	srv.AddDoc(testSongId, doc1)

	doc2 := make(api.Document)
	doc2.Add("index1", "v1")
	srv.AddDoc(testSongId2, doc2)

	srv.DeleteDoc(testSongId2)

	result := srv.Search("index1:v1")
	if len(result) != 1 {
		t.Fatalf("Unexpected result length: %d", len(result))
	}
}

func TestMultiIndex_Search(t *testing.T) {
	srv := NewMultiIndex()
	doc1 := make(api.Document)
	doc1.Add("index1", "v1")
	srv.AddDoc(testSongId, doc1)

	doc2 := make(api.Document)
	doc2.Add("index1", "v1")
	srv.AddDoc(testSongId2, doc2)

	result := srv.Search("index1:v1")
	if len(result) != 2 {
		t.Fatalf("Wrong number of results: %d", len(result))
	}

	result = srv.Search("index1:novalue")
	if len(result) != 0 {
		t.Fatalf("Error querying for index with no value: found=%d", len(result))
	}

	result = srv.Search("noindex:v1")
	if len(result) != 0 {
		t.Fatalf("Error querying for missing index: found=%d", len(result))
	}
}

func TestMultiIndex_SearchMultiValuedDoc(t *testing.T) {
	srv := NewMultiIndex()
	doc1 := make(api.Document)
	doc1.Add("index1", "v1")
	doc1.Add("index1", "v2")
	srv.AddDoc(testSongId, doc1)

	result := srv.Search("index1:v1")
	if len(result) != 1 {
		t.Fatalf("Wrong number of results for v1: %d", len(result))
	}

	result = srv.Search("index1:v2")
	if len(result) != 1 {
		t.Fatalf("Wrong number of results for v2: %d", len(result))
	}
}

func TestMultiIndex_SearchMultipleIndexes(t *testing.T) {
	srv := NewMultiIndex()

	doc := make(api.Document)
	doc.Add("index1", "v2")
	doc.Add("index2", "v1")
	doc.Add("index3", "v3")
	srv.AddDoc(testSongId, doc)

	doc = make(api.Document)
	doc.Add("index1", "v2")
	doc.Add("index2", "v1")
	doc.Add("index3", "v4")
	srv.AddDoc(testSongId2, doc)

	doc = make(api.Document)
	doc.Add("index1", "v2")
	doc.Add("index2", "v3")
	doc.Add("index3", "v5")
	srv.AddDoc(testSongId3, doc)

	// Intersection of two result sets.
	result := srv.Search("index1:v2 index2:v1")
	if len(result) != 2 {
		t.Fatalf("Wrong number of results for 2-way AND merge: %d", len(result))
	}

	// Intersection of three result sets.
	result = srv.Search("index1:v2 index2:v1 index3:v4")
	if len(result) != 1 {
		t.Fatalf("Wrong number of results for 3-way AND merge: %d", len(result))
	}

	// Union of two result sets.
	// q := mapToSearchQuery(map[string]string{"index2": "v1", "index3": "v4"})
	// q.Op = api.SearchOpOR
	// result = srv.Search(q)
	// if len(result) != 2 {
	// 	t.Fatalf("Wrong number of results for 2-way OR merge: %d", len(result))
	// }
}

func TestMultiIndex_SaveAndRestore(t *testing.T) {
	srv := NewMultiIndex()
	doc1 := make(api.Document)
	doc1.Add("index1", "v1")
	doc1.Add("index1", "v2")
	srv.AddDoc(testSongId, doc1)

	tmpf := ".indexsavetest"
	defer os.Remove(tmpf)

	if err := srv.Save(tmpf); err != nil {
		t.Fatalf("Save(): %s", err)
	}

	srv2, err := LoadMultiIndex(tmpf)
	if err != nil {
		t.Fatalf("Load(): %s", err)
	}

	result := srv2.Search("index1:v1")
	if len(result) != 1 {
		t.Fatalf("Index did not reload properly: %v", result)
	}
}
