// The index package implements a distributed search index.
//
// It is basically a simple reverse document index, implemented as a
// PartitionedService, which keeps the entire database in-memory (and
// occasionally checkpoints to disk).
//
// The query engine is not particularly clever or efficient, and the
// query expression language is rudimental, but the implementation is
// very simple and straightforward. And, of course, the entire service
// can be replaced by anything that implements the Index interface.
//
package index

import (
	"encoding/gob"
	"os"
	"sync"

	"git.autistici.org/ale/djrandom/api"
)

// Index is an index for a single attribute.  Each key can have
// multiple values.
type Index struct {
	Name string
	M    map[string]api.SongSet
}

func NewIndex(name string) *Index {
	return &Index{
		Name: name,
		M:    make(map[string]api.SongSet),
	}
}

// Add a key/value pair to the index.
func (i *Index) Add(key string, id api.SongID) {
	set, ok := i.M[key]
	if !ok {
		set = make(api.SongSet)
		i.M[key] = set
	}
	set.Add(id)
}

// Discard a key from the index.
func (i *Index) Discard(key string, id api.SongID) {
	if set, ok := i.M[key]; ok {
		set.Discard(id)
	}
}

// Get returns all values for key.
func (i *Index) Get(key string) (api.SongSet, bool) {
	s, ok := i.M[key]
	return s, ok
}

type MultiIndex struct {
	// The various indexes we built.
	Indexes map[string]*Index

	// The reverse docId -> values map which we're going to use to
	// remove documents from the index.
	D map[api.SongID]api.Document

	// Read-write lock to synchronize access to the index.
	lock sync.RWMutex
}

func NewMultiIndex() *MultiIndex {
	return &MultiIndex{
		Indexes: make(map[string]*Index),
		D:       make(map[api.SongID]api.Document),
	}
}

func LoadMultiIndex(path string) (*MultiIndex, error) {
	file, err := os.Open(path)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	var is MultiIndex
	if err = gob.NewDecoder(file).Decode(&is); err != nil {
		return nil, err
	}
	return &is, nil
}

func (is *MultiIndex) Save(path string) error {
	is.lock.Lock()
	defer is.lock.Unlock()

	tmpf := path + ".tmp"
	file, err := os.Create(tmpf)
	if err != nil {
		return err
	}
	defer file.Close()
	defer os.Remove(tmpf)
	if err := gob.NewEncoder(file).Encode(is); err != nil {
		return err
	}
	return os.Rename(tmpf, path)
}

// AddDoc adds a document to the index.
func (is *MultiIndex) AddDoc(id api.SongID, doc api.Document) error {
	// Are we re-indexing this document? If so, clear the existing
	// values before proceeding.
	is.lock.Lock()
	defer is.lock.Unlock()

	if _, ok := is.D[id]; ok {
		is.deleteDoc(id)
	}

	for indexName, values := range doc {
		idx, ok := is.Indexes[indexName]
		if !ok {
			idx = NewIndex(indexName)
			is.Indexes[indexName] = idx
		}
		for _, v := range values {
			idx.Add(v, id)
		}
	}

	is.D[id] = doc

	// This implementation always succeeds.
	return nil
}

func (is *MultiIndex) deleteDoc(id api.SongID) {
	doc, ok := is.D[id]
	if !ok {
		return
	}

	// Remove the values associated with this document from
	// all the indexes we maintain.
	for indexName, values := range doc {
		idx, ok := is.Indexes[indexName]
		if !ok {
			continue
		}
		for _, v := range values {
			idx.Discard(v, id)
		}
	}
	delete(is.D, id)
}

// DeleteDoc removes a document from the index.
func (is *MultiIndex) DeleteDoc(id api.SongID) {
	is.lock.Lock()
	defer is.lock.Unlock()

	is.deleteDoc(id)
}

// Lookup performs a point lookup in the index, returns a list of
// document IDs.
func (is *MultiIndex) Lookup(key string, value string) api.SongSet {
	is.lock.RLock()
	defer is.lock.RUnlock()

	if idx, ok := is.Indexes[key]; ok {
		if result, ok := idx.Get(value); ok {
			return result
		}
	}
	return make(api.SongSet)
}

// Search parses and executes a query. It returns the list of matching
// document IDs.
func (is *MultiIndex) Search(query string) api.SongSet {
	// Parse the query into an AST.
	pquery := StringToQuery(query)
	if pquery == nil {
		return nil
	}

	// Execute the parsed query.
	return pquery.Eval(is)
}

// GetDocIds returns the list of unique document IDs in the index.
func (is *MultiIndex) GetDocIds() []api.SongID {
	is.lock.RLock()
	defer is.lock.RUnlock()

	ids := make([]api.SongID, 0, len(is.D))
	for docId, _ := range is.D {
		ids = append(ids, docId)
	}
	return ids
}

// Scan outputs all document IDs to the given channel.
func (is *MultiIndex) Scan(out chan api.SongID) {
	is.lock.RLock()
	defer is.lock.RUnlock()

	for docId, _ := range is.D {
		out <- docId
	}
}
