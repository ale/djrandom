// This package contains the public interfaces of the available services.

package services

import (
	"io"
	"net/http"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/api/task_api"
)

// Storage is the client interface to the storage service.
type Storage interface {
	GetUrl(key, mode string) string
	Write(key string, r io.Reader) error
	Open(key string) (io.ReadCloser, error)
	ReverseProxyDirector() func(string, *http.Request)
}

// Index is the client interface to the indexing service.
type Index interface {
	AddDoc(id api.SongID, doc api.Document) error
	DeleteDoc(id api.SongID) error
	Search(query string) api.SongSet
	Scan() []api.SongID
}

// Generic interface for a database client iterator.
type Iterator interface {
	Next(interface{}) (string, error)
}

// Session represents a database session. Whether this can be
// associated with a transaction or not is something that depends on
// the implementation.
type Session interface {
	Get(bucket, key string, obj interface{}) error
	Put(bucket, key string, obj interface{}) error
	Del(bucket, key string) error
	Scan(bucket, startKey, endKey string, limit int) (Iterator, error)
	ScanKeys(bucket, startKey, endKey string, limit int) (Iterator, error)
	Close()
}

// DatabaseClient is the low-level interface to a database client.
type DatabaseClient interface {
	NewSession() (Session, error)
}

// Database is the high-level interface to the database layer.
type Database interface {
	NewSession() (Session, error)

	Get(Session, string, string, interface{}) error
	Put(Session, string, string, interface{}) error
	Del(Session, string, string) error

	GetSong(Session, api.SongID) (*api.Song, bool)
	GetSongWithoutDupes(Session, api.SongID) (*api.Song, bool)
	PutSong(Session, *api.Song) error

	GetSongFingerprint(Session, api.SongID) (*api.Fingerprint, error)
	PutSongFingerprint(Session, api.SongID, *api.Fingerprint) error

	GetSongAcousticData(Session, *api.Song) (*api.AcousticData, error)
	PutSongAcousticData(Session, *api.Song, *api.AcousticData) error
	GetSongAcousticCorrelation(Session, api.SongID, api.SongID) (float32, error)
	GetSongAcousticCorrelations(Session, api.SongID) ([]api.AcousticCorrelation, error)
	PutSongAcousticCorrelation(Session, api.SongID, api.SongID, float32) error

	GetAudioFile(Session, string) (*api.AudioFile, bool)
	PutAudioFile(Session, *api.AudioFile) error

	GetUser(Session, string) (*api.User, bool)
	PutUser(Session, *api.User) error
	GetAuthKey(Session, string) (*api.AuthKey, bool)
	PutAuthKey(Session, *api.AuthKey) error

	AppendPlayLog(Session, *api.PlayLogEntry) error
	GetPlayLog(Session, string) (*api.PlayLogEntry, bool)

	GetMarkovMap(Session, string) (*api.MarkovMap, bool)
	PutMarkovMap(Session, string, *api.MarkovMap) error

	GetArtists(Session, string) ([]string, error)
}

// TaskClient exports the services of the global task queue.
type TaskClient interface {
	Poll(fn func(*task_api.Job))
	Push(string) error
	PutJob(*task_api.Job) error
	GetJob(string) (*task_api.Job, error)
	CreateJob(*task_api.Plan, string) error
}
