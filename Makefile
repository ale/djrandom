
UI_VERSION = new
JS_SOURCES = \
	ui/js-$(UI_VERSION)/_namespace.js \
	$(wildcard ui/js-$(UI_VERSION)/[a-z]*.js)
JS_TARGET = \
	ui/static/js/djrandom.js

# 'prefix' is only used for auxiliary scripts, real binaries are
# installed in $GOPATH/bin.
prefix = /usr/local
sysconfdir = /etc/djrandom

all: version.go $(JS_TARGET)

install-client:
	go install ./client/...

install: all
	(for dir in server services mapreduce ; do \
	 go install ./$${dir}/... ; done)
	rsync -ar ./ui/static/ $(sysconfdir)/static/
	rsync -ar ./ui/templates/ $(sysconfdir)/templates/
	install -m 755 -o root -g root djrandom.init /etc/init.d/djrandom
	install -m 755 -o root -g root djrandom-cron $(prefix)/bin/djrandom-cron

.PHONY: version.go
version.go:
	./mkversion.sh

$(JS_TARGET): $(JS_SOURCES)
	cat $^ > $@

.PHONY: dist-client
dist-client:
	./mkclientdist.sh
