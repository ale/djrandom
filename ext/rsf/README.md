rsf
===

The rsf (Royal Straight fLaC) package provides support for reading [flac][]
(Free Lossless Audio Codec) files.

[flac]: http://flac.sourceforge.net/

documentation
-------------

Documentation provided by GoPkgDoc:

   - [rsf][]
       - [frame][]
       - [meta][]

[rsf]: http://go.pkgdoc.org/github.com/mewkiz/rsf
[frame]: http://go.pkgdoc.org/github.com/mewkiz/rsf/frame
[meta]: http://go.pkgdoc.org/github.com/mewkiz/rsf/meta

public domain
-------------

This code is hereby release this code into the *[public domain][]*.

[public domain]: https://creativecommons.org/publicdomain/zero/1.0/
