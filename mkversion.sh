#!/bin/bash

version=$(cat VERSION)
version_ref=$(git rev-parse --short=7 HEAD)
date=$(date +%Y%m%d%H%M)
host=$(hostname -s)
arch=$(uname -m)

cat >version.go <<EOF
package djrandom
var (
	MajorVersion = "${version}"
	Version = "${version}~${version_ref}"
	BuildTag = "${date}@${host}/${arch}"
)
EOF
