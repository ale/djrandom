package config

import (
	"encoding/json"
	"flag"
	"io/ioutil"
	"log"
	"os"
	"strconv"
)

type configContainer struct {
	values map[string]string
}

func newConfigContainer() *configContainer {
	return &configContainer{make(map[string]string)}
}

func (c *configContainer) Read(path string) error {
	file, err := os.Open(path)
	if err != nil {
		return err
	}
	defer file.Close()
	return json.NewDecoder(file).Decode(&(c.values))
}

// Perform file-based and environment-based substitutions.
func expand(value string) string {
	if len(value) > 1 && value[0] == '<' {
		path := value[1:]
		data, err := ioutil.ReadFile(path)
		if err != nil {
			log.Fatalf("Fatal error: could not read data from '%s': %s", path, err)
		}
		return string(data)
	}
	return os.ExpandEnv(value)
}

type configFlag interface {
	Name() string
	IsSet() bool
	Copy(*configContainer)
}

type strFlag struct {
	name         string
	ptr          *string
	defaultValue string
}

func (s *strFlag) Name() string { return s.name }

func (s *strFlag) IsSet() bool {
	return *s.ptr != ""
}

func (s *strFlag) Copy(c *configContainer) {
	if value, ok := c.values[s.name]; ok {
		*s.ptr = expand(value)
	} else {
		*s.ptr = s.defaultValue
	}
}

type intFlag struct {
	name         string
	ptr          *int
	defaultValue int
}

func (i *intFlag) Name() string { return i.name }

func (i *intFlag) IsSet() bool {
	return *i.ptr != -1
}

func (i *intFlag) Copy(c *configContainer) {
	if value, ok := c.values[i.name]; ok {
		*i.ptr, _ = strconv.Atoi(value)
	} else {
		*i.ptr = i.defaultValue
	}
}

var (
	flagRegistry []configFlag
	globalConfig *configContainer
)

func init() {
	flagRegistry = make([]configFlag, 0)
	globalConfig = newConfigContainer()
}

func registerFlag(cf configFlag) {
	flagRegistry = append(flagRegistry, cf)
}

func String(name, defaultValue, help string) *string {
	ptr := flag.String(name, "", help)
	registerFlag(&strFlag{name, ptr, defaultValue})
	return ptr
}

func Int(name string, defaultValue int, help string) *int {
	ptr := flag.Int(name, -1, help)
	registerFlag(&intFlag{name, ptr, defaultValue})
	return ptr
}

func Parse(configFile string) error {
	if err := globalConfig.Read(configFile); err != nil {
		return err
	}
	for _, f := range flagRegistry {
		if !f.IsSet() {
			f.Copy(globalConfig)
		} else {
			log.Printf("flag %s set from command-line", f.Name())
		}
	}
	return nil
}

func MustParse(configFile string) {
	if err := Parse(configFile); err != nil {
		log.Fatalf("Error: could not read configuration file: %s", err)
	}
}

// Convenience function to abort if s is unset.
func MustString(optName, s string) string {
	if s == "" {
		log.Fatalf("Must set --%s", optName)
	}
	return s
}

func MustInt(optName string, i int) int {
	if i == -1 {
		log.Fatalf("Must set --%s", optName)
	}
	return i
}
