package util

import (
	"time"
)

type RestartableTimer struct {
	C     chan time.Time
	timer *time.Timer
}

func NewRestartableTimer() *RestartableTimer {
	rt := new(RestartableTimer)
	rt.C = make(chan time.Time)
	return rt
}

func (rt *RestartableTimer) Reset(d time.Duration) {
	if rt.timer != nil {
		rt.timer.Stop()
	}
	rt.timer = time.AfterFunc(d, func() {
		rt.C <- time.Now()
	})
}
