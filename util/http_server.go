package util

import (
	"net/http"
	"time"
)

func ListenAndServeWithTimeout(addr string, handler http.Handler, timeout time.Duration) error {
	server := &http.Server{
		Addr:         addr,
		Handler:      handler,
		ReadTimeout:  timeout,
		WriteTimeout: timeout,
	}
	return server.ListenAndServe()
}

func ListenAndServe(addr string, handler http.Handler) error {
	return ListenAndServeWithTimeout(addr, handler, 600*time.Second)
}
