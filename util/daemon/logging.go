package daemon

import (
	"flag"
	"fmt"
	"log"
	"log/syslog"
	"os"
	"path/filepath"
)

var (
	enableSyslog = flag.Bool("syslog", false, "Log to syslog")
	logFile      = flag.String("log-file", "", "Log to this file")
)

func setupLogging() {
	if *enableSyslog {
		l, err := syslog.New(syslog.LOG_NOTICE, "")
		if err == nil {
			log.SetOutput(l)
			log.SetFlags(log.Lshortfile)
			return
		} else {
			log.Printf("Error opening syslog: %s", err)
		}
	} else if *logFile != "" {
		logfd, err := os.OpenFile(*logFile, os.O_WRONLY|os.O_APPEND|os.O_CREATE, 0640)
		if err == nil {
			log.SetOutput(logfd)
			log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
			return
		} else {
			log.Printf("Error opening log file '%s': %s", *logFile, err)
		}
	}

	// Default logging goes to stderr.
	log.SetPrefix(fmt.Sprintf("%s[%d] ", filepath.Base(os.Args[0]), os.Getpid()))
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
}
