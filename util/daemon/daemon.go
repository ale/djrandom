package daemon

import (
	"git.autistici.org/ale/djrandom/util/status"
)

var EnableStatusPage = true

func Setup() {
	setupLogging()
	createPidFile()
	if EnableStatusPage {
		status.SetupStatusPage()
	}
}
