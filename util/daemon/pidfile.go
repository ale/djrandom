package daemon

import (
	"flag"
	"fmt"
	"log"
	"os"
)

var (
	pidfilePath = flag.String("pid-file", "", "Write PID file")
)

func createPidFile() {
	if *pidfilePath != "" {
		file, err := os.Create(*pidfilePath)
		if err != nil {
			log.Printf("Could not create pid file: %s", err)
		}
		defer file.Close()
		fmt.Fprintf(file, "%d\n", os.Getpid())
	}
}
