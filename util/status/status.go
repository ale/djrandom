package status

import (
	"html/template"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	_ "net/http/pprof"

	"git.autistici.org/ale/djrandom"
)

type statusContext struct {
	Program       string
	Cmdline       string
	Hostname      string
	ShortHostname string
	Version       string
	BuildTag      string
	StartTime     time.Time
	Endpoints     []string
}

var statusCtx *statusContext

var debugEndpoints = []string{
	"/debug/rpc",
	"/debug/pprof/",
}

func RegisterDebugEndpoint(url string) {
	debugEndpoints = append(debugEndpoints, url)
}

var statusTmpl = template.Must(template.New("status").Parse(`<!DOCTYPE html>
<html>
<head>
  <title>{{.Program}}</title>
</head>
<body>
<h1>{{.Program}} <small>{{.Version}} @{{.ShortHostname}}</small> </h1>
<p>
  Running on {{.Hostname}} since {{.StartTime}}.<br>
  Build tag: <code>{{.BuildTag}}</code><br>
  Command line: <code>{{.Cmdline}}</code>
</p>
<p>
  Debug:<br>
  {{range .Endpoints}}
    <code><a href="{{.}}">{{.}}</a></code><br>
  {{end}}
</p>
</body>
</html>`))

func statusPage(w http.ResponseWriter, r *http.Request) {
	if r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	if err := statusTmpl.Execute(w, statusCtx); err != nil {
		http.Error(w, "Internal error", http.StatusInternalServerError)
	}
}

func SetupStatusPage() {
	prog := filepath.Base(os.Args[0])
	hostname, _ := os.Hostname()
	shortHostname := strings.SplitN(hostname, ".", 2)[0]
	statusCtx = &statusContext{
		Program:       prog,
		Cmdline:       strings.Join(os.Args, " "),
		Version:       djrandom.Version,
		BuildTag:      djrandom.BuildTag,
		Hostname:      hostname,
		ShortHostname: shortHostname,
		StartTime:     time.Now(),
		Endpoints:     debugEndpoints,
	}

	http.Handle("/", http.HandlerFunc(statusPage))
}
