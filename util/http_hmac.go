package util

import (
	"crypto/hmac"
	"crypto/sha1"
	"encoding/base64"
	"encoding/hex"
	"errors"
	"fmt"
	"io"
	"log"
	"math"
	"net/http"
	"strings"
	"time"

	"git.autistici.org/ale/djrandom/api"
)

var (
	// Some incoming HTTP requests might take a long time to reach
	// the backend if we have a buffering proxy on the frontend
	// (for instance, NGINX). Relax the allowed delay.
	MaxTimeSkew = 12 * 3600.0

	// Authorization errors.
	BadAuth      = errors.New("Bad authenticated request")
	Unauthorized = errors.New("Unauthorized")
	AuthFailed   = errors.New("Authentication failed")
)

// GetHttpRequestSignature returns the signature of a HTTP request.
func GetHttpRequestSignature(secret string, r *http.Request) string {
	h := hmac.New(sha1.New, []byte(secret))

	items := []string{
		r.Method,
		r.Header.Get("Content-MD5"),
		r.Header.Get("Content-Type"),
		r.Header.Get("Date"),
		r.URL.Path,
	}

	io.WriteString(h, strings.Join(items, "\n"))
	return hex.EncodeToString(h.Sum(nil))
}

// SignHttpRequests adds a HMAC Authorization header with a signature
// to a HTTP request.
func SignHttpRequest(key *api.AuthKey, r *http.Request) {
	// Add a 'Date' header if not present.
	if r.Header.Get("Date") == "" {
		r.Header.Add("Date", time.Now().UTC().Format(http.TimeFormat))
	}

	sig := GetHttpRequestSignature(key.Secret, r)
	encSig := base64.StdEncoding.EncodeToString([]byte(sig))
	r.Header.Add("Authorization", fmt.Sprintf("HMAC %s:%s", key.KeyId, encSig))
}

// Authenticate checks the Authorization header in the request, and
// verifies its validity. It accepts a function to retrieve
// authentication keys from a database or other source.
func AuthenticateHttpRequest(getAuth func(string) (*api.AuthKey, bool), r *http.Request) (*api.AuthKey, error) {
	authHdr := r.Header.Get("Authorization")
	if authHdr == "" {
		return nil, Unauthorized
	}

	// Decode the Authorization header, and check auth type.
	authParts := strings.SplitN(authHdr, " ", 2)
	if len(authParts) != 2 || authParts[0] != "HMAC" {
		log.Printf("AuthenticateHTTPRequest(): bad auth type")
		return nil, BadAuth
	}

	// Decode auth key ID and encoded signature.
	sigParts := strings.SplitN(authParts[1], ":", 2)
	if len(sigParts) != 2 {
		log.Printf("AuthenticateHTTPRequest(): bad auth format")
		return nil, BadAuth
	}

	// Retrieve the auth key object from the database.
	authKey, ok := getAuth(sigParts[0])
	if !ok {
		log.Printf("AuthenticateHTTPRequest(): bad auth key")
		return nil, AuthFailed
	}

	sig, err := base64.StdEncoding.DecodeString(sigParts[1])
	if err != nil {
		log.Printf("AuthenticateHTTPRequest(): bad base64 encoding")
		return nil, BadAuth
	}

	// Compute the expected signature, and validate the request.
	expectedSig := GetHttpRequestSignature(authKey.Secret, r)
	if expectedSig != string(sig) {
		log.Printf("AuthenticateHTTPRequest(): bad signature")
		return nil, AuthFailed
	}

	// If the request is too old, discard it (but allow for some
	// amount of time desynchronization on the client).
	stamp, err := time.Parse(http.TimeFormat, r.Header.Get("Date"))
	if err != nil {
		log.Printf("AuthenticateHTTPRequest(): unparseable Date header")
		return nil, AuthFailed
	} else {
		off := math.Abs(time.Since(stamp).Seconds())
		if off > MaxTimeSkew {
			log.Printf("AuthenticateHTTPRequest(): timestamp error (offset=%f)", off)
			return nil, AuthFailed
		}
	}

	return authKey, nil
}
