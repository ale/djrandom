package metadata_extraction

import (
	"testing"

	"git.autistici.org/ale/djrandom/api"
)

func TestFLAC_Metadata(t *testing.T) {
	testFile := "../../testdata/chet.flac"
	flacex := &FLAC{}
	audioMeta, meta, err := flacex.GetMetadata(testFile)
	if err != nil {
		t.Fatal(err)
	}

	if meta.Artist != "Chet Baker" || meta.Title != "Stella By Starlight" || meta.Album != "Memories" {
		t.Errorf("Metadata error: %v", meta)
	}
	if audioMeta.MimeType != api.MIMETYPE_FLAC {
		t.Errorf("Bad mime type: %v", audioMeta)
	}
	if audioMeta.SampleRate != 44100 {
		t.Errorf("Bad sample rate: %v", audioMeta)
	}
	if audioMeta.Channels != 2 {
		t.Errorf("Bad number of channels: %v", audioMeta)
	}
	if audioMeta.MD5 != "104f31597265f611f73ab2fd95c515be" {
		t.Errorf("Bad checksum: %v", audioMeta)
	}
}
