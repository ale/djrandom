package metadata_extraction

import (
	"os"
	"strconv"

	"git.autistici.org/ale/djrandom/api"
	id3 "github.com/dustin/go-id3"
)

// Parse ID3 metadata if present, leave file at the beginning of contained data.
func parseId3Metadata(file *os.File, m *api.Meta) {
	id3meta := id3.Read(file)
	if id3meta != nil && id3meta.Header.Size > 0 {
		m.Title = id3meta.Name
		m.Artist = id3meta.Artist
		m.Album = id3meta.Album
		m.Genre = id3meta.Genre
		if id3meta.Year != "" {
			m.Year, _ = strconv.Atoi(id3meta.Year)
		}
		if id3meta.Track != "" {
			m.TrackNum, _ = strconv.Atoi(id3meta.Track)
		}
		// Skip the ID3 header.
		file.Seek(int64(10+id3meta.Header.Size), os.SEEK_SET)
	} else {
		file.Seek(0, os.SEEK_SET)
	}
}
