package main

import (
	"encoding/hex"
	"flag"
	"fmt"
	"log"

	"git.autistici.org/ale/djrandom/ext/rsf"
	"git.autistici.org/ale/djrandom/ext/rsf/meta"
)

func main() {
	flag.Parse()

	stream, err := rsf.Open(flag.Arg(0))
	if err != nil {
		log.Fatalf("Open error: %s", err.Error())
	}

	for _, block := range stream.MetaBlocks {
		if block.Header.BlockType == meta.TypeStreamInfo {
			si := block.Body.(*meta.StreamInfo)
			fmt.Printf("StreamInfo: sample_rate=%d, chans=%d, bits=%d, samples=%d, md5=%s\n",
				si.SampleRate,
				si.ChannelCount,
				si.BitsPerSample,
				si.SampleCount,
				hex.EncodeToString(si.MD5sum[:16]))
		} else if block.Header.BlockType == meta.TypeCueSheet {
			cs := block.Body.(*meta.CueSheet)
			fmt.Printf("CueSheet: MCN=%s, tracks=%d\n",
				cs.MCN, cs.TrackCount)
		} else if block.Header.BlockType == meta.TypePicture {
			fmt.Printf("Picture:\n")
		} else if block.Header.BlockType == meta.TypeVorbisComment {
			v := block.Body.(*meta.VorbisComment)
			fmt.Printf("Vorbis comment: vendor=%s\n", v.Vendor)
			for _, entry := range v.Entries {
				fmt.Printf("    %s = %s\n", entry.Name, entry.Value)
			}
		}
	}
}
