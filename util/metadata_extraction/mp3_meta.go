package metadata_extraction

import (
	"crypto/md5"
	"encoding/hex"
	"errors"
	"io"
	"log"
	"os"

	"git.autistici.org/ale/djrandom/api"
)

const (
	bytesToFingerprint = 1024 * 64
)

func init() {
	extractors[api.MIMETYPE_MP3] = &MP3{}
}

type MP3 struct{}

// Decode the first few KBs from the MPEG stream, and return the first
// frame header (to extract audio params) and the MD5 fingerprint of
// the contents of the data stream. If no data could be read, the
// empty string is returned.
func decodeMpegFrames(r io.Reader) (MpegFrameHeader, string) {
	var firstHdr MpegFrameHeader
	h := md5.New()
	var tot int
	for tot = 0; tot < bytesToFingerprint; {
		hdr, data := ReadMpegFrame(r)
		if hdr == nil {
			break
		}
		if firstHdr == nil {
			firstHdr = make(MpegFrameHeader, 3)
			copy(firstHdr, hdr)
		}
		if data != nil {
			l := len(data)
			nn := bytesToFingerprint - tot
			if l > nn {
				l = nn
			}
			h.Write(data[:l])
			tot += l
		}
	}
	if tot > 0 {
		return firstHdr, hex.EncodeToString(h.Sum(nil))
	}
	return firstHdr, ""
}

func (m MP3) GetMetadata(path string) (*api.AudioFile, *api.Meta, error) {
	// Stat the file to get its size.
	stat, err := os.Stat(path)
	if err != nil {
		log.Printf("MP3.GetMetadata(%s): Stat failed: %s", path, err)
		return nil, nil, err
	}

	file, err := os.Open(path)
	if err != nil {
		return nil, nil, err
	}
	defer file.Close()

	// Decode ID3 metadata.
	am := &api.Meta{}
	parseId3Metadata(file, am)

	// Decode encoder parameters. A failure means that the file is
	// probably invalid. Note that we're assuming that the ID3 data
	// comes first.
	hdr, fp := decodeMpegFrames(file)
	if hdr == nil || fp == "" {
		return nil, nil, errors.New("No MPEG frames could be decoded")
	}
	af := &api.AudioFile{
		MD5:           fp,
		MimeType:      api.MIMETYPE_MP3,
		SampleRate:    hdr.SampleRate(),
		BitRate:       hdr.Bitrate(),
		Channels:      hdr.Channels(),
		BitsPerSample: 16,
		Size:          stat.Size(),
	}

	return af, am, nil
}
