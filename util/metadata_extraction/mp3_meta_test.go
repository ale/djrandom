package metadata_extraction

import (
	"testing"

	"git.autistici.org/ale/djrandom/api"
)

func TestMP3_Metadata(t *testing.T) {
	testFile := "../../testdata/chet.mp3"
	mp3ex := &MP3{}
	audioMeta, meta, err := mp3ex.GetMetadata(testFile)
	if err != nil {
		t.Fatal(err)
	}

	if meta.Artist != "Chet Baker" || meta.Title != "Stella By Starlight" || meta.Album != "Memories" {
		t.Errorf("Metadata error: %v", meta)
	}
	if audioMeta.MimeType != api.MIMETYPE_MP3 {
		t.Errorf("Bad mime type: %v", audioMeta)
	}
	if audioMeta.SampleRate != 44100 {
		t.Errorf("Bad sample rate: %v", audioMeta)
	}
	if audioMeta.BitRate != 320 {
		t.Errorf("Bad bitrate: %v", audioMeta)
	}
	if audioMeta.Channels != 2 {
		t.Errorf("Bad number of channels: %v", audioMeta)
	}
	if audioMeta.MD5 != "9622e3cdcd8ce80f1d8db41f12f3bc9d" {
		t.Errorf("Bad checksum: %v", audioMeta)
	}
}
