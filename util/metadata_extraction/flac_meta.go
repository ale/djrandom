package metadata_extraction

import (
	"encoding/hex"
	"log"
	"os"
	"strconv"
	"strings"

	"git.autistici.org/ale/djrandom/api"

	"git.autistici.org/ale/djrandom/ext/rsf"
	"git.autistici.org/ale/djrandom/ext/rsf/meta"
)

func init() {
	extractors[api.MIMETYPE_FLAC] = &FLAC{}
}

type FLAC struct{}

// ExtractMetadata reads a (local) FLAC file and returns audio
// encoding parameters and song metadata as two separate objects.
func (f FLAC) GetMetadata(path string) (*api.AudioFile, *api.Meta, error) {
	var af api.AudioFile
	var m api.Meta

	// Set some placeholders for missing values.
	m.TrackNum = -1
	m.Year = -1

	// Stat the file for later.
	stat, err := os.Stat(path)
	if err != nil {
		log.Printf("FLAC.GetMetadata(%s): Stat failed: %s", path, err)
		return nil, nil, err
	}

	// Open the FLAC file and Scan through all the blocks. Try to read
	// ID3 metadata first, if present.
	file, err := os.Open(path)
	if err != nil {
		log.Printf("FLAC.GetMetadata(%s): Could not open: %s", path, err)
		return nil, nil, err
	}
	defer file.Close()

	parseId3Metadata(file, &m)

	filePos, _ := file.Seek(0, os.SEEK_CUR)
	log.Printf("file at position %d", filePos)
	stream, err := rsf.NewStream(file)
	if err != nil {
		log.Printf("FLAC.GetMetadata(%s): %s", path, err)
		return nil, nil, err
	}

	hasAudio := false
	for _, block := range stream.MetaBlocks {
		if block.Header.BlockType == meta.TypeStreamInfo {
			si := block.Body.(*meta.StreamInfo)

			// Found the audio file parameters.
			hasAudio = true
			af.MimeType = api.MIMETYPE_FLAC
			af.SampleRate = int(si.SampleRate)
			af.Channels = int(si.ChannelCount)
			af.BitsPerSample = int(si.BitsPerSample)
			af.Duration = float64(si.SampleCount) / float64(si.SampleRate)
			af.Size = stat.Size()
			af.MD5 = hex.EncodeToString(si.MD5sum[:16])

			// fmt.Printf("StreamInfo: sample_rate=%d, chans=%d, bits=%d, duration=%d, md5=%s\n",
			//	af.SampleRate, af.Channels, af.BitsPerSample, int(af.Duration), af.MD5)
			// } else if block.Header.BlockType == meta.TypePicture {
			//	fmt.Printf("Picture:\n")
		} else if block.Header.BlockType == meta.TypeVorbisComment {
			v := block.Body.(*meta.VorbisComment)

			// Scan for song metadata.
			// fmt.Printf("Vorbis comment: vendor=%s\n", v.Vendor)
			for _, entry := range v.Entries {
				// fmt.Printf("    %s = %s\n", entry.Name, entry.Value)
				switch strings.ToLower(entry.Name) {
				case "artist":
					m.Artist = entry.Value
				case "album":
					m.Album = entry.Value
				case "title":
					m.Title = entry.Value
				case "genre":
					m.Genre = entry.Value
				case "tracknumber":
					p := strings.Split(entry.Value, " ")
					m.TrackNum, _ = strconv.Atoi(p[0])
				case "date":
					p := strings.Split(entry.Value, " ")
					m.Year, _ = strconv.Atoi(p[0])
				}
			}
		}
	}

	// Verify that the required sections are present.
	if !hasAudio {
		log.Printf("FLAC.GetMetadata(%s): No audio info", path)
		return nil, nil, NoMetadata
	}

	return &af, &m, nil
}
