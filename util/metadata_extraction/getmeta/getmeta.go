package main

import (
	"flag"
	"fmt"

	"git.autistici.org/ale/djrandom/util/metadata_extraction"
)

var (
	mime = flag.String("mime", "audio/mpeg", "MIME type for decoder")
)

func main() {
	flag.Parse()

	for _, path := range flag.Args() {
		af, meta, err := metadata_extraction.GetMetadata(path, *mime)
		if err != nil {
			fmt.Printf("%s: %s\n", path, err)
		} else {
			fmt.Printf("%s: %v %v\n", path, af, meta)
		}
	}
}
