package metadata_extraction

import (
	"errors"

	"git.autistici.org/ale/djrandom/api"
)

var (
	NoMetadata      = errors.New("No audio metadata")
	UnknownMimeType = errors.New("Unknown MIME type")

	extractors = make(map[string]MetadataExtractor)
)

type MetadataExtractor interface {
	GetMetadata(path string) (*api.AudioFile, *api.Meta, error)
}

func GetMetadata(path, mimeType string) (*api.AudioFile, *api.Meta, error) {
	ex, ok := extractors[mimeType]
	if !ok {
		return nil, nil, UnknownMimeType
	}
	return ex.GetMetadata(path)
}

func GuessMetadata(path string) (*api.AudioFile, *api.Meta, error) {
	for _, ex := range extractors {
		af, am, err := ex.GetMetadata(path)
		if err == nil {
			return af, am, nil
		}
	}
	return nil, nil, UnknownMimeType
}
