package metadata_extraction

import (
	"bufio"
	"io"
)

const (
	MPEG_V25 = 0
	MPEG_V2  = 2
	MPEG_V1  = 3

	LAYER_I   = 3
	LAYER_II  = 2
	LAYER_III = 1
)

var (
	mpegBitRates = [][]int{
		{0, 0, 0, 0, 0},
		{32, 32, 32, 32, 8},
		{64, 48, 40, 48, 16},
		{96, 56, 48, 56, 24},
		{128, 64, 56, 64, 32},
		{160, 80, 64, 80, 40},
		{192, 96, 80, 96, 48},
		{224, 112, 96, 112, 56},
		{256, 128, 112, 128, 64},
		{288, 160, 128, 144, 80},
		{320, 192, 160, 160, 96},
		{352, 224, 192, 176, 112},
		{384, 256, 225, 192, 128},
		{416, 320, 256, 224, 144},
		{448, 384, 320, 256, 160},
		{0, 0, 0, 0, 0},
	}

	mpegSampleRates = [][]int{
		{44100, 22050, 11025},
		{48000, 24000, 12000},
		{32000, 16000, 8000},
		{0, 0, 0},
	}
)

type MpegFrameHeader []byte

func NewMpegFrameHeader() MpegFrameHeader {
	return make(MpegFrameHeader, 3)
}

func (hdr MpegFrameHeader) MpegVersion() int {
	return (int(hdr[0]) & 0x18) >> 3
}

func (hdr MpegFrameHeader) MpegLayer() int {
	return (int(hdr[0]) & 0x06) >> 1
}

func (hdr MpegFrameHeader) Bitrate() int {
	mpegVersion := hdr.MpegVersion()
	mpegLayer := hdr.MpegLayer()
	bits := (int(hdr[1]) & 0xf0) >> 4
	var idx int
	if mpegVersion == MPEG_V1 {
		switch mpegLayer {
		case LAYER_I:
			idx = 0
		case LAYER_II:
			idx = 1
		case LAYER_III:
			idx = 2
		}
	} else if mpegLayer == LAYER_I {
		idx = 3
	} else {
		idx = 4
	}
	return mpegBitRates[bits][idx]
}

func (hdr MpegFrameHeader) SampleRate() int {
	bits := (int(hdr[1]) & 0x0c) >> 2
	var idx int
	switch hdr.MpegVersion() {
	case MPEG_V1:
		idx = 0
	case MPEG_V2:
		idx = 1
	case MPEG_V25:
		idx = 2
	}
	return mpegSampleRates[bits][idx]
}

func (hdr MpegFrameHeader) Channels() int {
	bits := (int(hdr[2]) & 0xc0) >> 6
	if bits == 3 {
		return 1
	}
	return 2
}

func (hdr MpegFrameHeader) Padding() int {
	return (int(hdr[2]) & 0x02) >> 1
}

func (hdr MpegFrameHeader) HasCRC() bool {
	return int(hdr[1]&1) == 1
}

func (hdr MpegFrameHeader) FrameSize() (flen int) {
	if hdr.MpegLayer() == LAYER_I {
		flen = (12000*hdr.Bitrate()/hdr.SampleRate() + hdr.Padding()) * 4
	} else {
		flen = 144000*hdr.Bitrate()/hdr.SampleRate() + hdr.Padding()
	}
	return
}

func (hdr MpegFrameHeader) IsValid() bool {
	return (int(hdr[0])&0xe0 == 0xe0 && hdr.SampleRate() != 0 && hdr.Bitrate() != 0)
}

// ReadMpegFrame returns the next frame header and data in the input
// stream.  Note that this function makes no attempt at decoding the
// actual MPEG bitstream.
func ReadMpegFrame(r io.Reader) (MpegFrameHeader, []byte) {
	buf := bufio.NewReader(r)
	hdr := NewMpegFrameHeader()
	data := make([]byte, 4096)
	crc := make([]byte, 2)
	var err error
	for err != io.EOF {
		// Skip until next 0xff.
		if _, err = buf.ReadBytes(0xff); err == nil {
			var n int
			// Read frame header and validate it.
			n, err = buf.Read(hdr)
			if err == nil && n == 3 && hdr.IsValid() {
				// Skip over the frame CRC if present.
				if hdr.HasCRC() {
					buf.Read(crc)
				}
				// Read the frame data.
				n, err = buf.Read(data[:hdr.FrameSize()])
				if err == nil {
					return hdr, data[:n]
				} else {
					return hdr, nil
				}
			}
		}
	}
	return nil, nil
}
