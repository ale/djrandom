package webdav

import (
	"encoding/xml"
	"html/template"
	"time"
)

var s = `<?xml version="1.0" encoding="UTF-8" ?>
 <propfind xmlns="DAV:">
      <prop>
        <supported-live-property-set/>
        <supported-method-set/>
      </prop>
    </propfind>`

type PropSet struct {
	XMLName xml.Name
}

type Prop struct {
	Props []PropSet `xml:",any"`
}

type PropfindRequest struct {
	XMLName xml.Name `xml:"DAV: propfind"`
	Prop    Prop     `xml:"DAV: prop"`
	AllProp xml.Name `xml:"DAV: allprop"`
}

type PropValue struct {
	CreationDate  string `xml:"DAV: creationdate,omitempty"`
	LastModified  string `xml:"DAV: getlastmodified,omitempty"`
	ContentLength int64  `xml:"DAV: getcontentlength,omitempty"`
	ResourceType  struct {
		Collection *struct{} `xml:"DAV: collection,omitempty"`
	} `xml:"DAV: resourcetype"`
}

type PropStat struct {
	Prop   PropValue `xml:"DAV: prop"`
	Status string    `xml:"DAV: status"`
}

type StatusResponse struct {
	Href  string   `xml:"DAV: href"`
	Stats PropStat `xml:"DAV: propstat"`
}

type MultiStatusResponse struct {
	XMLName   xml.Name         `xml:"DAV: multistatus"`
	Responses []StatusResponse `xml:"DAV: response"`
}

func epochToXMLTime(sec int64) string {
	return template.HTMLEscapeString(time.Unix(sec, 0).UTC().Format(time.RFC3339))
}
