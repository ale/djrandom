package webdav

import (
	"encoding/xml"
	"errors"
	"log"
	"net/http"
	"net/url"
	"strings"
)

var defaultProperties = []string{
	"creationdate",
	"resourcetype",
	"getcontentlength",
	"getlastmodified",
}

type Attrs struct {
	Name  string
	IsDir bool
	Size  int64
	Ctime int64
	Mtime int64
}

type Fs interface {
	Get(w http.ResponseWriter, r *http.Request)
	Stat(path string) (Attrs, error)
	Ls(path string) ([]Attrs, error)
}

type WebDAV struct {
	prefix string
	fs     Fs
}

func NewWebDAV(prefix string, fs Fs) *WebDAV {
	return &WebDAV{prefix, fs}
}

func (wd *WebDAV) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		wd.get(w, r)
	case "PROPFIND":
		wd.propfind(w, r)
	case "OPTIONS":
		w.Header().Set("DAV", "1")
	default:
		w.WriteHeader(http.StatusBadRequest)
	}
}

func (wd *WebDAV) get(w http.ResponseWriter, r *http.Request) {
	r.URL.Path = r.URL.Path[len(wd.prefix):]
	wd.fs.Get(w, r)
}

// Parse a PROPFIND request body and return the properties that were requested.
func propsFromRequest(r *http.Request) ([]string, error) {
	var req PropfindRequest
	if err := xml.NewDecoder(r.Body).Decode(&req); err != nil {
		return nil, err
	}
	log.Printf("PROPFIND: %v", req)

	var propsToFind []string
	if req.Prop.Props != nil {
		propsToFind = []string{}
		for _, prop := range req.Prop.Props {
			switch prop.XMLName.Local {
			case "supported-live-property-set", "supported-report-set":
				propsToFind = defaultProperties
			default:
				propsToFind = append(propsToFind, prop.XMLName.Local)
			}
		}
	} else if req.AllProp.Local != "" {
		propsToFind = defaultProperties
	} else {
		return nil, errors.New("No properties requested")
	}
	return propsToFind, nil
}

func (wd *WebDAV) propfind(w http.ResponseWriter, r *http.Request) {
	defer r.Body.Close()
	propsToFind, err := propsFromRequest(r)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	msr := MultiStatusResponse{
		xml.Name{"DAV:", "multistatusresponse"},
		[]StatusResponse{},
	}

	// Depending on the 'depth' setting, perform either a Stat or a Ls
	// call to the underlying Fs implementation.
	var fserr error
	path := strings.Trim(r.URL.Path[len(wd.prefix):], "/")
	files := make([]Attrs, 0, 1)
	depth := r.Header.Get("Depth")
	switch depth {
	case "0":
		attrs, fserr := wd.fs.Stat(path)
		if fserr == nil {
			files = append(files, attrs)
		}
	case "1":
		attrlist, fserr := wd.fs.Ls(path)
		if fserr == nil {
			// Always fake a directory entry
			files = append(files, Attrs{IsDir: true})
			files = append(files, attrlist...)
		}
		// Make sure that the request URL ends in a slash.
		if !strings.HasSuffix(r.URL.Path, "/") {
			r.URL.Path = r.URL.Path + "/"
		}
	case "", "infinity":
		http.Error(w, "Unsupported depth of infinity", http.StatusForbidden)
		return
	default:
		http.Error(w, "Invalid depth", http.StatusBadRequest)
		return
	}
	if fserr != nil {
		msr.Responses = append(msr.Responses, StatusResponse{
			Href: r.URL.String(),
			Stats: PropStat{
				Status: "HTTP/1.1 404 Not Found",
			},
		})
		goto end
	}

	// Assemble the response.
	for _, file := range files {
		var pval PropValue
		for _, p := range propsToFind {
			switch p {
			case "creationdate":
				pval.CreationDate = epochToXMLTime(file.Ctime)
			case "getlastmodified":
				pval.LastModified = epochToXMLTime(file.Mtime)
			case "getcontentlength":
				pval.ContentLength = file.Size
			case "resourcetype":
				if file.IsDir {
					pval.ResourceType.Collection = new(struct{})
				}
			}
		}

		href := url.URL{Path: r.URL.Path + file.Name}
		msr.Responses = append(msr.Responses, StatusResponse{
			Href: href.String(),
			Stats: PropStat{
				Prop:   pval,
				Status: "HTTP/1.1 200 Ok",
			},
		})
	}

end:
	log.Printf("Reply: %v", msr)
	w.WriteHeader(207)
	xml.NewEncoder(w).Encode(msr)
}
