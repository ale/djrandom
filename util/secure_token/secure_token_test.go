package secure_token

import (
	"testing"
	"log"
)

func TestSecureToken(t *testing.T) {
	*secretKey = "test secret key"

	s := NewSecureToken("verb", "arg", 3600)
	enc := s.Encode()
	log.Printf("encoded token: %s", enc)

	s2, err := DecodeSecureToken(enc)
	if err != nil {
		t.Fatal(err)
	}
	if s2.Verb != s.Verb || s2.Arg != s.Arg {
		t.Errorf("decoded object does not match: %v / %v", s, s2)
	}

	exp := NewSecureToken("verb", "arg", -3600)
	enc = exp.Encode()
	_, err = DecodeSecureToken(enc)
	if err == nil {
		t.Fatal("expired token validated successfully")
	}
}
