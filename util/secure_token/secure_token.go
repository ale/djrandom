package secure_token

import (
	"bytes"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/gob"
	"errors"
	"fmt"
	"strings"
	"time"

	"git.autistici.org/ale/djrandom/util/config"
)

var (
	secretKey = config.String("secret_key", "", "Secret key for token signing")
)

type SecureToken struct {
	Verb    string
	Arg     string
	Expires time.Time
}

func (s *SecureToken) Encode() string {
	var enc bytes.Buffer
	if err := gob.NewEncoder(&enc).Encode(s); err != nil {
		return ""
	}

	mac := hmac.New(sha256.New, []byte(*secretKey))
	mac.Write(enc.Bytes())
	senc := base64.URLEncoding.EncodeToString(mac.Sum(nil))
	benc := base64.URLEncoding.EncodeToString(enc.Bytes())

	return fmt.Sprintf("%s.%s", benc, senc)
}

func NewSecureToken(verb, arg string, ttl int64) *SecureToken {
	return &SecureToken{verb, arg, time.Now().Add(time.Second * time.Duration(ttl))}
}

func DecodeSecureToken(in string) (*SecureToken, error) {
	inparts := strings.Split(in, ".")
	if len(inparts) != 2 {
		return nil, errors.New("token format error")
	}

	// Decode the base64 elements.
	benc, err := base64.URLEncoding.DecodeString(inparts[0])
	if err != nil {
		return nil, err
	}
	senc, err := base64.URLEncoding.DecodeString(inparts[1])
	if err != nil {
		return nil, err
	}

	// Verify signature.
	mac := hmac.New(sha256.New, []byte(*secretKey))
	mac.Write(benc)
	if !hmac.Equal(mac.Sum(nil), senc) {
		return nil, errors.New("bad signature")
	}

	// Decode the token itself.
	var s SecureToken
	if err := gob.NewDecoder(bytes.NewBuffer(benc)).Decode(&s); err != nil {
		return nil, err
	}

	if time.Now().After(s.Expires) {
		return nil, errors.New("token expired")
	}

	return &s, nil
}

func Validate(in, verb string) (string, error) {
	st, err := DecodeSecureToken(in)
	if err != nil {
		return "", err
	}
	if st.Verb != verb {
		return "", errors.New("bad verb")
	}
	return st.Arg, nil
}
