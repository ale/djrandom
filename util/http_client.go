package util

import (
	"bytes"
	"crypto/tls"
	"crypto/x509"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"git.autistici.org/ale/djrandom/api"
)

type HttpClient struct {
	baseUrl    string
	client     *http.Client
	bulkClient *http.Client
	authKey    *api.AuthKey
}

func NewHttpClient(baseUrl string, authKey *api.AuthKey, caPool *x509.CertPool) *HttpClient {
	hc := &HttpClient{
		baseUrl: baseUrl,
		authKey: authKey,
	}

	// Create a client with compression enabled, and one without,
	// which we will use for bulk file transfers.
	tr := &http.Transport{
		TLSClientConfig:    &tls.Config{RootCAs: caPool},
		DisableCompression: true,
	}
	hc.bulkClient = &http.Client{Transport: tr}

	tr = &http.Transport{
		TLSClientConfig: &tls.Config{RootCAs: caPool},
	}
	hc.client = &http.Client{Transport: tr}

	return hc
}

func (h *HttpClient) DoJSON(c *http.Client, r *http.Request, resp interface{}) error {
	if c == nil {
		c = h.client
	}
	SignHttpRequest(h.authKey, r)
	httpResp, err := c.Do(r)
	if err != nil {
		log.Printf("doRequest(): %s", err)
		return err
	}

	defer httpResp.Body.Close()

	if httpResp.StatusCode != 200 {
		log.Printf("HTTP request failed (%s): %v", httpResp.Status, r)
		return fmt.Errorf("Bad HTTP response (%s)", httpResp.Status)
	}

	return json.NewDecoder(httpResp.Body).Decode(resp)
}

// PostJSON performs a POST request with a JSON-encoded payload.
func (h *HttpClient) Post(url string, req interface{}, resp interface{}) error {
	encData, err := json.Marshal(req)
	if err != nil {
		log.Printf("Post(%s): JSON encoding error: %s", url, err)
		return err
	}
	httpReq, err := h.NewRequest("POST", url, bytes.NewBuffer(encData))
	if err != nil {
		log.Printf("Post(%s): NewRequest error: %s", url, err)
		return err
	}
	httpReq.Header.Set("Content-Type", "application/json")

	return h.DoJSON(h.client, httpReq, resp)
}

func (h *HttpClient) Put(url string, in io.Reader, size int64, resp interface{}) error {
	httpReq, err := h.NewRequest("PUT", url, in)
	if err != nil {
		log.Printf("Put(%s): NewRequest error: %s", url, err)
		return err
	}

	// Set some standard headers.
	httpReq.Header.Set("Content-Type", "application/octet-stream")
	httpReq.ContentLength = size

	return h.DoJSON(h.bulkClient, httpReq, resp)
}

func (h *HttpClient) Get(url string, resp interface{}) error {
	httpReq, err := h.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("Get(%s): NewRequest error: %s", url, err)
		return err
	}

	return h.DoJSON(h.client, httpReq, resp)
}

func (h *HttpClient) GetRaw(url string) (*http.Response, error) {
	req, err := h.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("GetRaw(%s): NewRequest error: %s", url, err)
		return nil, err
	}

	SignHttpRequest(h.authKey, req)

	return h.bulkClient.Do(req)
}

func (h *HttpClient) GetRawRange(url string, beg, end int64) (*http.Response, error) {
	req, err := h.NewRequest("GET", url, nil)
	if err != nil {
		log.Printf("GetRaw(%s): NewRequest error: %s", url, err)
		return nil, err
	}
	req.Header.Add("Range", fmt.Sprintf("bytes=%d-%d", beg, end-1))

	SignHttpRequest(h.authKey, req)

	return h.bulkClient.Do(req)
}

func (h *HttpClient) NewRequest(method, relUrl string, data io.Reader) (*http.Request, error) {
	fullUrl := fmt.Sprintf("%s%s", h.baseUrl, relUrl)
	return http.NewRequest(method, fullUrl, data)
}
