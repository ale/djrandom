// Compute audio fingerprints of songs using the Echonest analyzers.
//
package fingerprinting

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"os/exec"
	"path/filepath"
	"strconv"

	"git.autistici.org/ale/djrandom/api"
	"git.autistici.org/ale/djrandom/services"
	"git.autistici.org/ale/djrandom/util/ffmpeg"
)

var (
	// Flags
	codegenBinary = flag.String("codegen-binary", "echoprint-codegen", "Codegen binary")

	// Errors
	NoAudioFiles = errors.New("No audio files")

	// Fingerprint sampling parameters.
	fingerprintSampleOffset = 20
	fingerprintSampleTime   = 60
)

func RunCodegen(tempName string) (string, string, error) {
	cmd := exec.Command(
		*codegenBinary, tempName, "0",
		strconv.Itoa(fingerprintSampleTime))
	log.Printf("Exec: %v", cmd)
	stdout, err := cmd.StdoutPipe()
	if err != nil {
		return "", "", err
	}
	defer stdout.Close()
	if err := cmd.Start(); err != nil {
		return "", "", err
	}
	data, err := ioutil.ReadAll(stdout)
	if err != nil {
		return "", "", err
	}
	if err := cmd.Wait(); err != nil {
		return "", "", err
	}

	// Now that we have the data, we can pass it as is to the
	// Echonest HTTP client. But we'd like to take a peek at it,
	// to check if there have been any errors: the codegen tool
	// does not report failure with its exit status, but returns a
	// JSON containing the error...
	var codegenResponse []struct {
		Metadata struct {
			Version float64
			// And a lot of other fields
		}
		Error string
		Code  string
	}
	if err := json.Unmarshal(data, &codegenResponse); err != nil {
		return "", "", err
	}
	if len(codegenResponse) != 1 {
		return "", "", errors.New("Wrong # of responses from codegen")
	}
	resp := codegenResponse[0]
	if resp.Error != "" {
		return "", "", errors.New(resp.Error)
	}
	if resp.Code == "" {
		return "", "", errors.New("Empty fingerprint")
	}

	// Default version is ENMFP (3.15).
	version := "3.15"
	if resp.Metadata.Version > 0 {
		version = fmt.Sprintf("%g", resp.Metadata.Version)
	}

	return resp.Code, version, nil
}

func GetSongFingerprint(song *api.Song, db services.Database, storage services.Storage) (string, string, error) {

	// Make sure we actually have an audio file to process...
	if len(song.Files) < 1 {
		return "", "", NoAudioFiles
	}

	// Request the file from distributed storage.
	af := song.Files[0]
	audioFileId := af.MD5
	file, err := storage.Open(audioFileId)
	if err != nil {
		log.Printf("GetSongFingerprint(): %s", err)
		return "", "", err
	}
	defer file.Close()

	// Pipe it to ffmpeg to extract a WAV sample of the audio.
	// This is just so that hopefully we don't have to download
	// the entire file, as ffmpeg should close stdin as soon as it
	// has decoded the time slice it needs.
	tmpDir, err := ioutil.TempDir("", "djrandom-fingerprint-")
	if err != nil {
		return "", "", err
	}
	defer os.RemoveAll(tmpDir)

	tmpName := filepath.Join(tmpDir, "fp.wav")
	ffmpegArgs := []string{
		"-f", api.GetFfmpegFormatForMimeType(af.MimeType),
		"-i", "-",
		"-t", strconv.Itoa(fingerprintSampleTime),
		"-ss", strconv.Itoa(fingerprintSampleOffset),
		tmpName,
	}
	output, err := ffmpeg.Run(file, ffmpegArgs)
	if err != nil {
		log.Printf("GetSongFingerprint(): ffmpeg: %s\n%s", err, output)
		return "", "", err
	}

	// Now we have a WAV sample, run the Echonest code generator on it.
	fpCode, fpVersion, err := RunCodegen(tmpName)
	if err != nil {
		log.Printf("GetSongFingerprint(): codegen: %s", err)
		return "", "", err
	}

	return fpCode, fpVersion, nil
}
