package fingerprinting

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"
	"time"

	"git.autistici.org/ale/djrandom/api"
)

var (
	echonestApiUrl = "http://developer.echonest.com/api/v4/song/identify"
	errRetry       = errors.New("retry")
)

const maxRetries = 5

type Echonest struct {
	endpoint string
}

func NewEchonest(apiKey string) *Echonest {
	return &Echonest{
		endpoint: fmt.Sprintf("%s?api_key=%s", echonestApiUrl, apiKey),
	}
}

// Identify performs a query to the Echonest public servers to
// identify a song given a fingerprint. The fingerprint is actually
// some JSON-encoded data coming directly from 'echoprint-codegen'.
func (e *Echonest) Identify(fingerprint, version string) (*api.Meta, error) {
	delay := 10 * time.Second
	for i := 0; i < maxRetries; i++ {
		m, err := e.identify(fingerprint, version)
		if err != errRetry {
			return m, err
		}
		time.Sleep(delay)
		delay *= 2
	}
	return nil, errors.New("too many errors")
}

func (e *Echonest) identify(fingerprint, version string) (*api.Meta, error) {
	resp, err := http.Get(fmt.Sprintf("%s&code=%s&version=%s", e.endpoint, fingerprint, version))
	if err != nil {
		log.Printf("Identify(): echonest query returned error: %s", err)
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode == 429 {
		log.Printf("Identify(): echonest above request limits, retrying...")
		return nil, errRetry
	} else if resp.StatusCode != 200 {
		log.Printf("Identify(): echonest returned status %d: %v", resp.StatusCode, resp)
		return nil, fmt.Errorf("http error %s", resp.Status)
	}

	var echonestResponse struct {
		Response struct {
			Status struct {
				Version string
				Message string
				Code    int
			}
			Songs []struct {
				ArtistName string `json:"artist_name"`
				Title      string `json:"title"`
				ArtistId   string `json:"artist_id"`
				Id         string `json:"id"`
				Message    string `json:"message"`
				Score      int    `json:"score"`
			}
		}
	}
	if err = json.NewDecoder(resp.Body).Decode(&echonestResponse); err != nil {
		log.Printf("Identify(): echonest returned bad response: %s", err)
		return nil, err
	}
	log.Printf("Identify(): response = %v", echonestResponse.Response)

	// Status.Code == 0 means ok, everything else is an error.
	if echonestResponse.Response.Status.Code != 0 {
		log.Printf("Identify(): response status code %d",
			echonestResponse.Response.Status.Code)
		return nil, errors.New(echonestResponse.Response.Status.Message)
	}

	if len(echonestResponse.Response.Songs) > 0 {
		log.Printf("Identify(): found %v", echonestResponse.Response.Songs)
		meta := echonestResponse.Response.Songs[0]
		return &api.Meta{
			Artist: meta.ArtistName,
			Title:  meta.Title,
		}, nil
	}

	return nil, nil
}
