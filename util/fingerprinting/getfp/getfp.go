package main

import (
	"flag"
	"fmt"
	"os"

	"git.autistici.org/ale/djrandom/util/ffmpeg"
	fp "git.autistici.org/ale/djrandom/util/fingerprinting"
)

var (
	apiKey = flag.String("api-key", "", "Echonest API key")
)

func main() {
	flag.Parse()

	e := fp.NewEchonest(*apiKey)

	for _, filename := range flag.Args() {
		file, err := os.Open(filename)
		if err != nil {
			fmt.Printf("error opening %s: %s\n", filename, err)
			continue
		}

		os.Remove("temp.wav")

		ffmpegArgs := []string{
			"-f", "flac",
			"-i", "-",
			"-t", "30", "-ss", "30",
			"temp.wav",
		}
		output, err := ffmpeg.Run(file, ffmpegArgs)
		file.Close()
		if err != nil {
			fmt.Printf("error running ffmpeg: %s\n%s", err, output)
			continue
		}

		code, version, err := fp.RunCodegen("temp.wav")
		if err != nil {
			fmt.Printf("error running codegen: %s\n", err)
			continue
		}

		fmt.Printf("%s: %s %s\n", filename, code, version)

		meta, err := e.Identify(code, version)
		if err != nil {
			fmt.Printf("error identifying %s: %s\n", filename, err)
			continue
		}
		fmt.Printf("%v\n", meta)
	}
}
