package http_auth

import (
	"encoding/base64"
	"errors"
	"net/http"
	"strings"
)

type BasicAuth struct {
	Username string
	Password string
}

func NewBasicAuthFromRequest(r *http.Request) (*BasicAuth, error) {
	h, ok := r.Header["Authorization"]
	if !ok || len(h) == 0 {
		return nil, errors.New("No Authorization header")
	}

	parts := strings.SplitN(h[0], " ", 2)
	if len(parts) != 2 {
		return nil, errors.New("Authorization header malformed")
	}

	scheme := parts[0]
	if scheme != "Basic" {
		return nil, errors.New("Unsupported authentication type")
	}
	credentials, err := base64.StdEncoding.DecodeString(parts[1])
	if err != nil {
		return nil, err
	}

	cparts := strings.Split(string(credentials), ":")
	if len(cparts) != 2 {
		return nil, errors.New("Authorization header invalid")
	}

	return &BasicAuth{
		Username: cparts[0],
		Password: cparts[1],
	}, nil
}
