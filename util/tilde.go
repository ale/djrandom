package util

import (
	"os"
	"path/filepath"
	"strings"
)

func ExpandTilde(path string) string {
	if strings.HasPrefix(path, "~/") {
		return filepath.Join(os.Getenv("HOME"), path[2:])
	}
	return path
}
