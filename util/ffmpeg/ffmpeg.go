package ffmpeg

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"os/exec"
	"strings"
)

var ffmpegBinary = flag.String("ffmpeg-binary", "avconv", "Ffmpeg/avconv binary")

// Run invokes ffmpeg with arguments and data on stdin. Returns output
// and exit status.
func Run(input io.Reader, args []string) (string, error) {
	var b bytes.Buffer
	cmd := exec.Command(*ffmpegBinary, args...)

	cmd.Stdout = &b
	cmd.Stderr = &b
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return "", err
	}

	if err = cmd.Start(); err != nil {
		stdin.Close()
		return "", err
	}
	io.Copy(stdin, input)
	stdin.Close()

	err = cmd.Wait()
	return b.String(), err
}

// RunPipe connects ffmpeg's output with another command, and returns
// its standard output and standard error separately.
func RunPipe(input io.Reader, args, pipeArgs []string) (string, string, error) {
	var stdOutBuf bytes.Buffer
	var stdErrBuf bytes.Buffer

	cmd := exec.Command("/bin/sh", "-c", fmt.Sprintf("%s %s | %s", *ffmpegBinary, strings.Join(args, " "), strings.Join(pipeArgs, " ")))

	cmd.Stdout = &stdOutBuf
	cmd.Stderr = &stdErrBuf
	stdin, err := cmd.StdinPipe()
	if err != nil {
		return "", "", err
	}

	if err = cmd.Start(); err != nil {
		stdin.Close()
		return "", "", err
	}

	io.Copy(stdin, input)
	stdin.Close()

	err = cmd.Wait()

	return stdOutBuf.String(), stdErrBuf.String(), err
}
