package ffmpeg

import (
	"bytes"
	"os"
	"fmt"
	"io/ioutil"
	"testing"
)

func createTestFfmpegScript(exitStatus int) string {
	tmpf, _ := ioutil.TempFile("", "ffmpeg_test_")
	filename := tmpf.Name()
	fmt.Fprintf(tmpf, "#!/bin/sh\ntest \"$1\" = a || exit 127\necho stderr 1>&2\ncat\nexit %d\n", exitStatus)
	tmpf.Close()
	os.Chmod(filename, 0755)
	return filename
}

func TestFfmpeg_Run(t *testing.T) {
	*ffmpegBinary = createTestFfmpegScript(0)
	defer os.Remove(*ffmpegBinary)

	input := bytes.NewBufferString("stdin\n")
	output, err := Run(input, []string{"a", "b", "c"})
	if err != nil {
		t.Fatal(err)
	}
	if output != "stderr\nstdin\n" {
		t.Fatalf("Output: '%s'", output)
	}
}

func TestFfmpeg_RunFailure(t *testing.T) {
	*ffmpegBinary = createTestFfmpegScript(1)
	defer os.Remove(*ffmpegBinary)

	input := bytes.NewBufferString("stdin\n")
	output, err := Run(input, []string{"a", "b", "c"})
	if err == nil {
		t.Fatal("err is nil")
	}
	if output != "stderr\nstdin\n" {
		t.Fatalf("Output: '%s'", output)
	}
}

func TestFfmpeg_RunPipe(t *testing.T) {
	*ffmpegBinary = createTestFfmpegScript(0)
	defer os.Remove(*ffmpegBinary)

	input := bytes.NewBufferString("stdin\n")
	stdout, stderr, err := RunPipe(
		input,
		[]string{"a", "b", "c"},
		[]string{"sed", "-e", "s/std/STD/"})
	if err != nil {
		t.Fatal(err)
	}
	if stderr != "stderr\n" {
		t.Errorf("StdErr: '%s'", stderr)
	}
	if stdout != "STDin\n" {
		t.Errorf("StdOut: '%s'", stdout)
	}
}
