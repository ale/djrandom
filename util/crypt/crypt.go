package crypt

import (
	"crypto/rand"
	"encoding/base64"
	"fmt"
	"strings"

	"golang.org/x/crypto/scrypt"
)

type scryptParams struct {
	N, r, p int
}

var (
	scryptSaltSize   = 48
	scryptOutputSize = 96
	scryptParamsMap  = map[string]scryptParams{
		"S0": scryptParams{16384, 8, 1},
	}
)

func GenRandomSalt() string {
	salt := make([]byte, scryptSaltSize)
	n, err := rand.Read(salt)
	if n != scryptSaltSize || err != nil {
		return ""
	}

	return fmt.Sprintf("$S0$%s$", base64.StdEncoding.EncodeToString(salt))
}

func sCrypt(password, salt string) string {
	saltParts := strings.Split(salt, "$")
	if len(saltParts) < 3 || saltParts[0] != "" {
		return ""
	}
	params, ok := scryptParamsMap[saltParts[1]]
	if !ok {
		return ""
	}
	saltData, err := base64.StdEncoding.DecodeString(saltParts[2])
	if err != nil {
		return ""
	}

	dk, err := scrypt.Key([]byte(password), saltData, params.N, params.r, params.p, scryptOutputSize)
	if err != nil {
		return ""
	}
	encPass := base64.StdEncoding.EncodeToString(dk)

	return strings.Join(
		[]string{"", saltParts[1], saltParts[2], encPass}, "$")
}

func Crypt(password, salt string) string {
	if strings.HasPrefix(salt, "$S0$") {
		return sCrypt(password, salt)
	}
	return systemCrypt(password, salt)
}
