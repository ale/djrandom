package crypt

import (
	"testing"
)

func TestCrypt(t *testing.T) {
	salt := GenRandomSalt()
	pw := "testpass"

	encPw := Crypt(pw, salt)
	result := Crypt(pw, encPw)
	if encPw != result {
		t.Fatalf("Crypt() failure: %s != %s", encPw, result)
	}

	result = Crypt("bad pass", encPw)
	if encPw == result {
		t.Fatalf("Crypt() failure: %s == %s", encPw, result)
	}
}
