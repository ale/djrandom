package crypt

import (
	"sync"
)

// #cgo linux CFLAGS: -D_XOPEN_SOURCE=1
// #cgo linux LDFLAGS: -lcrypt
// #include <unistd.h>
//
import "C"

var cryptMutex sync.Mutex

func systemCrypt(key, salt string) string {
	cryptMutex.Lock()
	defer cryptMutex.Unlock()
	return C.GoString(C.crypt(C.CString(key), C.CString(salt)))
}
