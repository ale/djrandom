package toolcli

import (
	"flag"
	"fmt"
	"os"
)

type Command struct {
	Name     string
	UsageStr string
	Help     string
	Fn       func([]string)
	MinArgs  int
	MaxArgs  int
}

func (c Command) PrintHelp() {
	fmt.Fprintf(os.Stderr, "%s %s\n  %s\n\n", c.Name, c.UsageStr, c.Help)
}

func (c Command) Run(args []string) {
	c.Fn(args)
}

type CommandList []Command

func (cl CommandList) PrintHelp() {
	fmt.Fprintf(os.Stderr, "Usage: %s <COMMAND> [<ARGS>...]\n", "prog")
	fmt.Fprintf(os.Stderr, "Known commands:\n\n")
	for _, cmd := range cl {
		cmd.PrintHelp()
	}
}

func Run(cl CommandList) {
	if !flag.Parsed() {
		flag.Parse()
	}

	args := flag.Args()
	if len(args) < 1 {
		cl.PrintHelp()
		return
	}

	cmdArg := args[0]
	args = args[1:]
	for _, cmd := range cl {
		if cmd.Name == cmdArg {
			if (cmd.MinArgs > 0 && len(args) < cmd.MinArgs) || (cmd.MaxArgs >= 0 && cmd.MaxArgs >= cmd.MinArgs && len(args) > cmd.MaxArgs) {
				fmt.Fprintf(os.Stderr, "Wrong number of arguments!\n")
				cmd.PrintHelp()
			} else {
				cmd.Run(args)
			}
			return
		}
	}
	fmt.Fprintf(os.Stderr, "Unknown command '%s'", cmdArg)
}
