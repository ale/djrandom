package tokenizer

import (
	"strings"
	"regexp"
)

var (
	// Unicode-safe RE2 alphanumeric token match.
	wordRegexp = regexp.MustCompile(`[\pL\pN]+`)

	// Regexp for track numbers.
	trackNumRegexp = regexp.MustCompile(`^[01][0-9]$`)
)

// WhitespaceTokenizer splits a string into alphanumeric tokens.
func WhitespaceTokenizer(s string) []string {
	return wordRegexp.FindAllString(s, -1)
}

// LowercaseFilter turns a token into lower case.
func LowercaseFilter(s string) (string, bool) {
	return strings.ToLower(s), true
}

// MinLengthFilter returns a FilterFunc which selects tokens that meet
// a minimum length.
func MinLengthFilter(minLength int) FilterFunc {
	return func(s string) (string, bool) {
		// Drop one-character words.
		if len(s) < minLength {
			return "", false
		}
		return s, true
	}
}

// TrackNumFilter drops tokens that look like track numbers (small
// two-digit zero-padded values), which frequently end up in song
// metadata.
func TrackNumFilter(s string) (string, bool) {
	if trackNumRegexp.MatchString(s) {
		return "", false
	}
	return s, true
}

// RemoveDiacriticsFilter drops diacritics from the token.
func RemoveDiacriticsFilter(s string) (string, bool) {
	return RemoveDiacritics(s), true
}
