package tokenizer

type TokenizerFunc func(string) []string
type FilterFunc func(string) (string, bool)

type Tokenizer struct {
	T TokenizerFunc
	Filters []FilterFunc
}

func NewTokenizer(tokenizer TokenizerFunc, filters... FilterFunc) *Tokenizer {
	return &Tokenizer{tokenizer, filters}
}

var DefaultTokenizer = NewTokenizer(
	WhitespaceTokenizer,
	MinLengthFilter(2),
	TrackNumFilter,
	LowercaseFilter,
	RemoveDiacriticsFilter)


func Tokenize(s string) []string {
	return DefaultTokenizer.Tokenize(s)
}

// Tokenize applies the analysis chain to the provided string.
func (t *Tokenizer) Tokenize(s string) []string {
	tokens := t.T(s)
	out := make([]string, 0, len(tokens))
	for _, token := range tokens {
		var ok bool
		for _, f := range t.Filters {
			token, ok = f(token)
			if !ok {
				break
			}
		}
		if ok {
			out = append(out, token)
		}
	}
	return out
}
