package tokenizer

import (
	"testing"
)

func compareSlices(a, b []string) bool {
	if len(a) != len(b) {
		return false
	}
	for i, _ := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

func testTokenize(t *testing.T, s string, expected []string) {
	result := Tokenize(s)
	if !compareSlices(result, expected) {
		t.Errorf("Error for '%s': result=%v, expected=%v", s, result, expected)
	}
}

func TestTokenizer_Basic(t *testing.T) {
	testTokenize(t,
		"the quick brown fox",
		[]string{"the", "quick", "brown", "fox"})

	testTokenize(t,
		"Hello, 世界",
		[]string{"hello", "世界"})

	testTokenize(t,
		"    hello     \r\n      world ",
		[]string{"hello", "world"})
}

func TestTokenizer_MinLengthFilter(t *testing.T) {
	testTokenize(t,
		"Hello, a!",
		[]string{"hello"})
}

func TestTokenizer_RemoveDiacritics(t *testing.T) {
	testTokenize(t,
		"Fabrizio de André",
		[]string{"fabrizio", "de", "andre"})
}

