package tokenizer

import (
	"unicode"

	"golang.org/x/text/unicode/norm"
)

var lat = []*unicode.RangeTable{unicode.Letter, unicode.Number}
var nop = []*unicode.RangeTable{unicode.Mark, unicode.Sk, unicode.Lm}

// RemoveDiacritics drops diacritic marks from the input string, using
// a Unicode NFKD normalization.
func RemoveDiacritics(s string) string {
	buf := make([]rune, 0, len(s))
	spc := false
	for _, r := range norm.NFKD.String(s) {
		switch {
		case unicode.IsOneOf(lat, r):
			buf = append(buf, unicode.ToLower(r))
			spc = true
		case unicode.IsOneOf(nop, r):
			// skip
		case spc:
			buf = append(buf, ' ')
			spc = false
		}
	}
	if i := len(buf) - 1; i >= 0 && buf[i] == ' ' {
		buf = buf[:i]
	}
	return string(buf)
}
