package instrumentation

import (
	"runtime"
	"time"
)

var (
	// How often to update memory statistics.
	updatePeriod = 60 * time.Second

	// Memory gauge variables.
	memGauge = NewGauge("go.memory", 1)
	gcGauge  = NewGauge("go.gc", 1)
)

func updateMemoryStats() {
	var m runtime.MemStats
	runtime.ReadMemStats(&m)

	memGauge.SetVar("alloc", int64(m.Alloc))
	memGauge.SetVar("sys", int64(m.Sys))
	memGauge.SetVar("heap_alloc", int64(m.HeapAlloc))
	memGauge.SetVar("heap_inuse", int64(m.HeapInuse))
	memGauge.SetVar("heap_objects", int64(m.HeapObjects))

	gcGauge.SetVar("pause_total_ns", int64(m.PauseTotalNs))
	gcGauge.SetVar("num_gc", int64(m.NumGC))
}

func init() {
	go func() {
		for _ = range time.NewTicker(updatePeriod).C {
			updateMemoryStats()
		}
	}()
}
