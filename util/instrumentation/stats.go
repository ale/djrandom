package instrumentation

import (
	"log"
	"os"
	"path/filepath"
	"sync"

	"git.autistici.org/ale/djrandom/util/config"
	"github.com/cactus/go-statsd-client/statsd"
)

var (
	statsdServer = config.String("statsd-server", "localhost:8125", "Location of the statsd server (host:port)")

	clientInitialized = false
	initLock          sync.Mutex
	statsdClient      statsd.Statter
)

// Lazy initialization of the statsd Client (with reduced contention).
func getClient() statsd.Statter {
	if !clientInitialized {
		initLock.Lock()
		if !clientInitialized {
			if *statsdServer != "" {
				prefix := "djrandom." + filepath.Base(os.Args[0])

				var err error
				statsdClient, err = statsd.New(*statsdServer, prefix)
				if err != nil {
					log.Printf("Error initializing statsd connection: %s", err)
					statsdClient = nil
				}
			}
			clientInitialized = true
		}
		initLock.Unlock()
	}
	return statsdClient
}

// Stats objects can represent an individual variable or a
// parametrized one, settable using the *Var() methods.
type Counter struct {
	Name string
	Rate float32
}

func (c *Counter) Inc(value int64) {
	if client := getClient(); client != nil {
		client.Inc(c.Name, value, c.Rate)
	}
}

func (c *Counter) IncVar(sfx string, value int64) {
	if client := getClient(); client != nil {
		client.Inc(c.Name+"."+sfx, value, c.Rate)
	}
}

func NewCounter(name string, rate float32) *Counter {
	return &Counter{name, rate}
}

type Gauge struct {
	Name string
	Rate float32
}

func (g *Gauge) Set(value int64) {
	if client := getClient(); client != nil {
		client.Gauge(g.Name, value, g.Rate)
	}
}

func (g *Gauge) SetVar(sfx string, value int64) {
	if client := getClient(); client != nil {
		client.Gauge(g.Name+"."+sfx, value, g.Rate)
	}
}

func NewGauge(name string, rate float32) *Gauge {
	return &Gauge{name, rate}
}

type Timing struct {
	Name string
	Rate float32
}

func (t *Timing) Add(value int64) {
	if client := getClient(); client != nil {
		client.Timing(t.Name, value, t.Rate)
	}
}

func (t *Timing) AddVar(sfx string, value int64) {
	if client := getClient(); client != nil {
		client.Timing(t.Name+"."+sfx, value, t.Rate)
	}
}

func NewTiming(name string, rate float32) *Timing {
	return &Timing{name, rate}
}
